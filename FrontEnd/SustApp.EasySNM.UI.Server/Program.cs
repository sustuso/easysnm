//using Microsoft.AspNetCore.Components.Web;
//using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
//using SustApp.EasySNM.UI.Server;
//
//var builder = WebAssemblyHostBuilder.CreateDefault(args);
//builder.RootComponents.Add<App>("#app");
//builder.RootComponents.Add<HeadOutlet>("head::after");
//
//builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
//
//await builder.Build().RunAsync();

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.UI.ServiceAgent;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace SustApp.EasySNM.UI.Server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            ServiceAgentLoggerProvider serviceAgentLoggerProvider = new ServiceAgentLoggerProvider();
            var host = Host.CreateDefaultBuilder(args)
                .ConfigureLogging((c, b) =>
                {
                    b.AddProvider(serviceAgentLoggerProvider);
                    b.AddFilter<ServiceAgentLoggerProvider>("Microsoft", LogLevel.Warning);
                    b.AddFilter<ServiceAgentLoggerProvider>("System.Net", LogLevel.Warning);
                })
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .Build();
            serviceAgentLoggerProvider.Services = host.Services;

            host.Run();
        }

    }
}

