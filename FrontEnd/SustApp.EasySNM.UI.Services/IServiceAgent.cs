﻿using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Services
{
    public interface IServiceAgent<T, in TKey>
        where T : class
    {
        Uri BaseAdress { get; }

        Task<T> AddAsync(T entity, CancellationToken token);

        Task<T> AddRangeAsync(List<T> entities, CancellationToken token);

        Task<T> UpdateAsync(T entity, CancellationToken token);

        Task<T> UpdateRangeAsync(List<T> entities, CancellationToken token);

        Task DeleteAsync(T entity, CancellationToken token);

        Task<T> GetAsync(TKey id, CancellationToken token);

        async Task<IEnumerable<T>> GetAsync(CancellationToken token)
        {
            CountResult<T> countResult = await GetAsync<T>(new GetRequest<T>(), token);
            return countResult.Items;
        }

        Task<CountResult<T>> GetAsync(GetRequest<T, T> query, CancellationToken token) =>
            GetAsync<T>(query, token);

        Task<CountResult<TResult>> GetAsync<TResult>(GetRequest<T, TResult> query,
            CancellationToken token)
            where TResult : class;
    }
}
