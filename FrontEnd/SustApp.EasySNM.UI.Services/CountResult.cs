﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM.UI.Services
{
    public class CountResult<T>
    {

        public static CountResult<T> Empty { get; } = new CountResult<T>();

        public CountResult(IReadOnlyList<T> items, int count)
        {
            Items = items;
            Count = count;
        }

        private CountResult()
        {
            Items = Array.Empty<T>();
        }

        public int Count { get; }

        public IReadOnlyList<T> Items { get; }
    }

}
