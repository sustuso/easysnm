﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace SustApp.EasySNM.UI.Services
{
    public class GetRequest<T, TResult>
    {
        public Func<IQueryable<T>, IQueryable<TResult>> QueryFactory { get; set; }

        public bool WithCount { get; set; }

        public List<Expression<Func<T, object>>> ExpandExpressions { get; } = new List<Expression<Func<T, object>>>();

    }

    public class GetRequest<T> : GetRequest<T, T>
    {
        public GetRequest()
        {
            QueryFactory = q => q;
        }

        public static GetRequest<T, TResult> Create<TResult>(Func<IQueryable<T>, IQueryable<TResult>> queryFactory)
        {
            return Create<TResult>(queryFactory, false);
        }

        public static GetRequest<T, TResult> Create<TResult>(Func<IQueryable<T>, IQueryable<TResult>> queryFactory, bool withCount)
        {
            return new GetRequest<T, TResult>
            {
                QueryFactory = queryFactory,
                WithCount = withCount
            };
        }
    }
}