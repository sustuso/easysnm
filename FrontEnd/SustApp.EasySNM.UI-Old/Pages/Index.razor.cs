﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using SustApp.EasySNM.UI.Forms;
using System.Security.Claims;
//using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SustApp.EasySNM.UI.Pages
{
    public partial class Index
    {
        [CascadingParameter(Name = "SelProPageContext")] PageContext SelProPageContext { get; set; }
        [CascadingParameter]private Task<AuthenticationState> authenticationStateTask { get; set; }
        [Inject] public NavigationManager NavManager { get; set; }

        AuthenticationState authenticationState;
        public List<Icon> ListIcon { get; set; } = new List<Icon>();

        protected override async Task OnInitializedAsync()
        {
            SelProPageContext.ModuleName = "";
            SelProPageContext.PageName = "";

            await base.OnInitializedAsync();
            authenticationState = await authenticationStateTask;

            GetIconAccessPolicies();

            //await ErrorService.RunAsync(async t =>
            //{
            //    //if (!CacheAppState.Contests.Any())
            //    //{
            //    //    //Valorize CacheAppState.Contests:
            //    //    var contests = await ContestServiceAgent.GetContestCacheAsync(t);
            //    //    foreach (var contest in contests)
            //    //    {
            //    //        CacheAppState.Contests.Add(contest.Id, contest.Description);
            //    //    }
            //    //}
            //
            //    //Valorize CacheAppState.Roles:
            //    var userIdentity = (ClaimsIdentity)authenticationState.User.Identity;
            //    //CacheAppState.Roles = userIdentity.Roles();
            //
            //}, Token);
        }



        private void GetIconAccessPolicies()
        {
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSmistamentoId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSmistamentoSottufficialiId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSmistamentoUfficialiId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSmistamentoVFPId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSmistamentoLocaleId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Smistamento", IconPath = "assets/selpro_icons/ModuleIcons/CalendarMarshalling.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/Marshalling/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleCheckInId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Check-In", IconPath = "assets/selpro_icons/ModuleIcons/CheckIn.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/CheckIn/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleProveFisicheId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Prove Fisiche", IconPath = "assets/selpro_icons/ModuleIcons/PhysicalTests.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/PhysicalTests/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleResponsabileAulaId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Prove psico attitudinali", IconPath = "assets/selpro_icons/ModuleIcons/PsychometricsTests.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/PsychometricsTests/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleWarteggId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Punteggio Wartegg", IconPath = "assets/selpro_icons/ModuleIcons/WarteggScoring.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/WarteggScoring/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePsicologoId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleResponsabilePsicologiId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Colloqui Psico-Att", IconPath = "assets/selpro_icons/ModuleIcons/PsychologicalEvaluation.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/PsychologicalEvaluation/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMedicoId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleInfermieriId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMedicinaGeneraleId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleCardiologoId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleOculistaId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleOtorinoloaringoiatraId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Prove mediche", IconPath = "assets/selpro_icons/ModuleIcons/MedicalEvaluation.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/MedicalExams/Contests/0/" });
            //}
            //
            ////---------- COMMISSIONI ------------
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMembroCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePresidenteCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSegretarioCommissioneId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Commissione Unica", IconPath = "assets/selpro_icons/ModuleIcons/ExaminationBoard.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/Commission/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMembroCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePresidenteCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSegretarioCommissioneId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Commissione Generale", IconPath = "assets/selpro_icons/ModuleIcons/ExaminationBoard.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/GeneralCommission/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMembroCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePresidenteCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSegretarioCommissioneId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Commissione Prove Fisiche", IconPath = "assets/selpro_icons/ModuleIcons/ExaminationBoard.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/PhysicalTestsCommission/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMembroCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePresidenteCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSegretarioCommissioneId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Commissione Psicofisica", IconPath = "assets/selpro_icons/ModuleIcons/ExaminationBoard.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/PsychophysicalCommission/Contests/0/" });
            //}
            //
            //if (authenticationState.User.IsInRole(ConstantCommon.UserRoleAdminId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleMembroCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRolePresidenteCommissioneId) || authenticationState.User.IsInRole(ConstantCommon.UserRoleSegretarioCommissioneId))
            //{
            //    ListIcon.Add(new Icon() { ModuleName = "Commissione Attitudinale", IconPath = "assets/selpro_icons/ModuleIcons/ExaminationBoard.png", IconStyle = "width: 105px; height: 105px;", LinkPath = "/AptitudeCommission/Contests/0/" });
            //}
        }

        private void GoUrl(string url)
        {
            NavManager.NavigateTo(url);
        }

        public class Icon
        {
            public string ModuleName { get; set; }
            public string IconPath { get; set; }
            public string LinkPath { get; set; }
            public string IconStyle { get; set; }
        }
    }
}