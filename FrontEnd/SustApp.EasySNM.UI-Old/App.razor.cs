﻿using Blazored.Modal;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.UI.Forms;
using SustApp.EasySNM.UI.Pages;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;

namespace SustApp.EasySNM.UI
{
    public partial class App
    {
#if SERVER
        private bool authorize = false;
#else
        private bool authorize = true;
#endif
    }
}
