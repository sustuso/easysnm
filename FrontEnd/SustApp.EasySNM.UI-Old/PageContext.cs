﻿using SustApp.EasySNM.DomainModel;
using System.Collections.Generic;
using System;
using System.Linq;

namespace SustApp.EasySNM.UI.Forms
{
    public class PageContext
    {
        public string ModuleName { get; set; }
        public string PageName { get; set; } = "SelPro";
        //public Site SiteSelected { get; set; } //Collegamento tra sito selezionato in overview e caricamento dati nel modulo corrispondente
    }
}

