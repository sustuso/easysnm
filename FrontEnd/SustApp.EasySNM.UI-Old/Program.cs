//var builder = WebApplication.CreateBuilder(args);
//
//// Add services to the container.
//builder.Services.AddRazorPages();
//
//var app = builder.Build();
//
//// Configure the HTTP request pipeline.
//if (!app.Environment.IsDevelopment())
//{
//    app.UseExceptionHandler("/Error");
//    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
//    app.UseHsts();
//}
//
//app.UseHttpsRedirection();
//app.UseStaticFiles();
//
//app.UseRouting();
//
//app.UseAuthorization();
//
//app.MapRazorPages();
//
//app.Run();

//#define REMOTE
using System;
using System.Net.Http;
using SustApp.EasySNM.UI.Forms.Services;
using Microsoft.Extensions.Options;
using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.UI.ServiceAgent;
using Microsoft.Extensions.Logging;
using SustApp.EasySNM.UI.Contexts;
#if !DESKTOP
using System.Threading.Tasks;
using SustApp.EasySNM.UI.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.EasySNM.UI
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            //TODO Da verificare
            //builder.RootComponents.Add<App>("app");

            var serviceAgentLoggerProvider = new ServiceAgentLoggerProvider();
            builder.Logging.AddProvider(serviceAgentLoggerProvider);
            builder.Logging.AddFilter<ServiceAgentLoggerProvider>("Microsoft", LogLevel.Warning);
            builder.Logging.AddFilter<ServiceAgentLoggerProvider>("System.Net", LogLevel.Warning);

            builder.Services.AddHttpClient(Options.DefaultName,
#if REMOTE
                client => client.BaseAddress = new Uri("https://staging.ei-selpro.it/"))
#elif DEBUG
                client => client.BaseAddress = new Uri("https://localhost:44334/"))
#else
            client => client.BaseAddress = new Uri(builder.HostEnvironment.BaseAddress))
#endif

#if DEBUG && !REMOTE
                .AddHttpMessageHandler(p =>
                {
                    var handler = p.GetRequiredService<AuthorizationMessageHandler>();
                    handler.ConfigureHandler(new[] { "https://localhost:44334", "https://localhost:44361/" });

                    return handler;
                })
#else
                .AddHttpMessageHandler<BaseAddressAuthorizationMessageHandler>()
#endif
                .AddHttpMessageHandler<DiagnosticsHandler>();

            builder.Services.AddTransient<DiagnosticsHandler>();

            builder.Services.AddTransient<HttpClient>(p =>
                p.GetRequiredService<IHttpClientFactory>().CreateClient(Options.DefaultName));

            builder.Services.AddOidcAuthentication(options =>
            {
                options.ProviderOptions.Authority = builder.Configuration["Authority"];
                options.ProviderOptions.ClientId = "EI.SelPro.UI";
                options.ProviderOptions.DefaultScopes.Add("EI.SelPro.Api");
                options.ProviderOptions.ResponseType = "code";
                options.UserOptions.RoleClaim = "role";
                options.UserOptions.NameClaim = "sub";
            });

            builder.Services.AddScoped<AccountClaimsPrincipalFactory<RemoteUserAccount>, EnricherAccountClaimsPrincipalFactory>();

            ConfigureServices(builder.Services);

            var host = builder.Build();
            serviceAgentLoggerProvider.Services = host.Services;
            await host.RunAsync();
        }

        public static void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<UserContext>();

            //services.AddTransient<TempFileService>();

            // Registro come scoped (singola istanza lato server e lato client)
            services.AddScoped<ErrorService>();
            // Anche per l'interfaccia uso la stessa istanza registrata nella riga precedente
            services.AddTransient<IErrorService>(p => p.GetRequiredService<ErrorService>());

            services.AddServiceAgent();

            services.AddSingleton<PushReceiverService>();
            services.AddSingleton<IPushReceiverService>(p => p.GetRequiredService<PushReceiverService>());

            services.AddTransient<IDialogService, DialogService>();
            services.AddScoped<IDownloadFileService, DownloadFileService>();

            services.AddSelProForms();

            //services.AddScoped<InteropService>();
            //services.AddScoped<CommissionAppState>();
            //services.AddScoped<CandidateMedicalConditionAppState>();
            //services.AddScoped<MarshallingAppState>();
            services.AddScoped<DataGridAppState>();
            //services.AddScoped<OverviewGroupAppState>();
            //services.AddScoped<PhisicalAppState>();
            services.AddScoped<CacheAppState>();//

            //services.AddDevExpressBlazor();

            //services.AddAuthorizationCore(o => o.AddPolicy("medico", b => b.RequireRole("medico")));
        }
    }
}
#endif
