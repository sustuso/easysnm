﻿namespace SustApp.EasySNM.UI.Services
{
    public class JsDownloadFileScript
    {
        /// <summary>
        /// The download script of the file
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="bytesBase64"></param>
        /// <returns></returns>
        public static string DownloadFileJavascriptScript(string fileName, string bytesBase64)
        {
            string js = $@"
                if (navigator.msSaveBlob) 
                {{
                    //Download document in Edge browser
                    var data = window.atob(""{bytesBase64}"");
                    var bytes = new Uint8Array(data.length);
                    for (var i = 0; i < data.length; i++) {{
                        bytes[i] = data.charCodeAt(i);
                    }}
                    var blob = new Blob([bytes.buffer], {{ type: ""application/octet-stream"" }});
                    navigator.msSaveBlob(blob, ""{fileName}"");
                }}
                else 
                {{
                    var link = document.createElement('a');
                    link.download = ""{fileName}"";
                    link.style.display = ""none"";
                    link.href = ""data:application/octet-stream;base64,"" + ""{bytesBase64}"";
                    document.body.appendChild(link); // Needed for Firefox
                    link.click();
                    document.body.removeChild(link);
                }}";

            return js;
        }
    }
}
