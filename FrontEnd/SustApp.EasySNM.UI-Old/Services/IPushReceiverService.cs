﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.DomainModel;

namespace SustApp.EasySNM.UI.Services
{
    public interface IPushReceiverService
    {
        event EventHandler<Message> OnMessage;
    }
}
