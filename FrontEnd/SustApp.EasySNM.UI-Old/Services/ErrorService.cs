﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using SustApp.EasySNM.UI.Forms;
using SustApp.EasySNM.UI.Forms.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace SustApp.EasySNM.UI.Services
{
    public class ErrorService : IErrorService
    {
        private readonly IDialogService _dialogService;
        private readonly ILogger<ErrorService> _logger;
        private readonly NavigationManager _navigationManger;
        private readonly IOptions<RemoteAuthenticationOptions<OidcProviderOptions>> _authenticationOptions;

        public bool HasError { get; set; }

        //Call from "@inject IErrorService ErrorService" in Index.razor
        public ErrorService(IDialogService dialogService, ILogger<ErrorService> logger, NavigationManager navigationManger, IOptions<RemoteAuthenticationOptions<OidcProviderOptions>> authenticationOptions)
        {
            _dialogService = dialogService;
            _logger = logger;
            _navigationManger = navigationManger;
            _authenticationOptions = authenticationOptions;
        }

        public async Task RunAsync(Func<CancellationToken, Task> task, SpinnerContext spinnerContext, CancellationToken token)
        {
            try
            {
                try
                {
                    HasError = false;

                    //Spinner start:
                    spinnerContext?.IncrementActive();

                    //Block and wait end task on delegate call:
                    await task(token);
                    token.ThrowIfCancellationRequested();
                }
                finally
                {
                    //Spinner end:
                    spinnerContext?.DecrementActive();
                }
            }
            catch (OperationCanceledException)
            {

            }
#if !SERVER
            catch (HttpRequestException ex)
            {
                _logger.LogError(ex, "HTTP error occurred");

                HasError = true;

                // Redirect to login
                if (ex.Message.Contains("401"))
                {
                    _navigationManger.NavigateTo(_authenticationOptions.Value.AuthenticationPaths.LogInPath);
                }
                else if (ex.Message.Contains("409"))
                {
                    await _dialogService.ShowErrorAsync("Errore", "Si è verificato un conflitto, ricaricare i dati");
                }
                else if (ex.Message.Contains("400"))
                {
                    await _dialogService.ShowErrorAsync("Errore",
                        "Si è verificato un errore nella richiesta perché i dati non sono corretti");
                }
                else
                {
                    await _dialogService.ShowErrorAsync("Errore", "Si è verificato un errore durante la richiesta");
                }                
            }
            catch (AccessTokenNotAvailableException)
            {
                _navigationManger.NavigateTo(_authenticationOptions.Value.AuthenticationPaths.LogInPath);
            }
#endif
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error while executing task");
                HasError = true;
                if (token.IsCancellationRequested) return;

#if DEBUG
                await _dialogService.ShowErrorAsync("Errore", ex.Message);
#else
                // TODO: mostrare errori diversi
                await _dialogService.ShowErrorAsync("Errore", "Si è verificato un errore non previsto");
#endif                
            }
        }
    }
}
