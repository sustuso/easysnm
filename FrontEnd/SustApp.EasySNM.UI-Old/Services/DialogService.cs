﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Blazored.Modal;
using Blazored.Modal.Services;
using Blazored.Toast.Services;
using SustApp.EasySNM.UI.Forms.Services;
//using SustApp.EasySNM.UI.Shared;
using Microsoft.AspNetCore.Components;

namespace SustApp.EasySNM.UI.Services
{
    public class DialogService : IDialogService
    {
        private readonly IModalService _modalService;
        private readonly IToastService _toastService;

        public DialogService(IModalService modalService, IToastService toastService)
        {
            _modalService = modalService;
            _toastService = toastService;
        }

        public async Task<TResult> Show<TComponent, TResult>(string title, object parameter, string cssClass = "")
            where TComponent : ComponentBase
        {
            var options = new ModalOptions()
            {
                Class = "blazored-modal " + cssClass,
                HideCloseButton = false,
                HideHeader = false,
                DisableBackgroundCancel = true
            };

            var parameters = new ModalParameters();
            if (parameter != null)
            {
                parameters.Add("Parameter", parameter);
            }

            ModalReference modalReference = (ModalReference)_modalService.Show<TComponent>(title, parameters, options);

            ModalResult modalResult = await modalReference.Result;

            if (modalResult.Data == null)
                return default(TResult);
            else
                return (TResult)modalResult.Data;
        }

        public Task ShowErrorAsync(string title, string message)
        {
            var options = new ModalOptions()
            {
                Class = "modal-dialog",
                HideCloseButton = true,
                HideHeader = true
            };

            var parameters = new ModalParameters();
            parameters.Add(nameof(DialogMessage), new DialogMessage(title, message));
            //ModalReference modalReference = (ModalReference)_modalService.Show<Error>(title, parameters, options);
            //return modalReference.Result;
            return null;
        }

        /// <summary>
        /// Show message rendering HTML unescaped
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public Task ShowUnescapedMessageAsync(string title, MarkupString message)
        {
            var options = new ModalOptions()
            {
                Class = "modal-dialog",
                HideCloseButton = true,
                HideHeader = true
            };

            var parameters = new ModalParameters();
            parameters.Add(nameof(DialogHtmlMessage), new DialogHtmlMessage(title, message));
            //ModalReference modalReference = _modalService.Show<HtmlMessage>(title, parameters, options);
            //
            //return modalReference.Result;
            return null;
        }

        public async Task<bool> ConfirmAsync(string title, string message)
        {
            var options = new ModalOptions()
            {
                Class = "modal-dialog",
                HideCloseButton = true,
                HideHeader = true
            };

            var parameters = new ModalParameters();
            parameters.Add(nameof(DialogMessage), new DialogMessage(title, message));
            //ModalReference modalReference = (ModalReference)_modalService.Show<Confirm>(title, parameters, options);
            //
            //ModalResult modalResult = await modalReference.Result;
            //return !modalResult.Cancelled;
            return false;
        }

        public Task ToastAsync(string title, string message, ToastLevel toastLevel)
        {
            _toastService.ShowToast(toastLevel, message, title);
            return Task.CompletedTask;
        }
    }
}
