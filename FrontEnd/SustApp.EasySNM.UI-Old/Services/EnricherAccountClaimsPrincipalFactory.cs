﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.UI.Forms.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.EasySNM.UI.Services
{
    public class EnricherAccountClaimsPrincipalFactory : AccountClaimsPrincipalFactory<RemoteUserAccount>
    {
        private readonly IServiceProvider _serviceProvider;

        public EnricherAccountClaimsPrincipalFactory(IAccessTokenProviderAccessor accessor, IServiceProvider serviceProvider) : base(accessor)
        {
            _serviceProvider = serviceProvider;
        }

        public override async ValueTask<ClaimsPrincipal> CreateUserAsync(RemoteUserAccount account, RemoteAuthenticationUserOptions options)
        {
            ClaimsPrincipal result = await base.CreateUserAsync(account, options);
            if (result.Identity.IsAuthenticated)
            {
                var identity = (ClaimsIdentity) result.Identity;

                // Read user roles
                var request = new GetRequest<UserRole>();
                request.QueryFactory = q => q.Where(u => u.Username == identity.Name);

                IServiceAgent<UserRole, Guid> userRoleServiceAgent = _serviceProvider.GetRequiredService<IServiceAgent<UserRole, Guid>>();
                var errorService = _serviceProvider.GetRequiredService<IErrorService>();

                await errorService.RunAsync(async t =>
                {
                    CountResult<UserRole> rolesResult = await userRoleServiceAgent.GetAsync(request, t);

                    // Add roles as claims
                    foreach (UserRole userRole in rolesResult.Items)
                    {
                        identity.AddClaim(new Claim(identity.RoleClaimType, userRole.RoleId));
                    }
                });
            }

            return result;
        }
    }
}
