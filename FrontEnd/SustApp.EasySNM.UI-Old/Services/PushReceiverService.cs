﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.DomainModel;

namespace SustApp.EasySNM.UI.Services
{
    public class PushReceiverService : IPushReceiverService
    {
        public event EventHandler<Message> OnMessage;

        public async Task ProcessMessageAsync(Message message)
        {
            OnMessage?.Invoke(this, message);
        }
    }
}
