﻿using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;

namespace SustApp.EasySNM.UI.Forms.Services
{
    public class DialogMessage
    {
        public string Title { get; }

        public string Message { get; }        

        public DialogMessage(string title, string message)
        {
            Title = title;
            Message = message;
        }
    }

    public class DialogHtmlMessage
    {
        public string Title { get; }

        public MarkupString Message { get; }

        public DialogHtmlMessage(string title, MarkupString message)
        {
            Title = title;
            Message = message;
        }
    }
}
