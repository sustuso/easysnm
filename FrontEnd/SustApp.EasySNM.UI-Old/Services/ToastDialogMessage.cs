﻿using System;
using Blazored.Toast.Services;

namespace SustApp.EasySNM.UI.Forms.Services
{
    public class ToastDialogMessage : DialogMessage
    {

        public ToastDialogMessage(string title, string message, ToastLevel toastLevel) : base(title, message)
        {
            ToastLevel = toastLevel;
        }

        public ToastLevel ToastLevel { get; set; }

    }
}
