﻿using Microsoft.AspNetCore.Components.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Contexts
{
    public class UserContext
    {
        public UserContext(/*AuthenticationStateProvider provider*/)
        {
            // provider.AuthenticationStateChanged += Provider_AuthenticationStateChanged;
        }

        //private async void Provider_AuthenticationStateChanged(Task<AuthenticationState> task)
        //{
        //    Username = (await task).User?.Identity.Name;
        //}

        public string Username { get; set; } = String.Empty;
    }
}
