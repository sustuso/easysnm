﻿var markup_left_air_masked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAeBJREFUeJzt3T1OVFEchvH3f8fcikQSQmIloVFXQEFwB7oBF2FCYViBJkAxpexDayqw8KN37JxailHqeyyYkNhNI/cJ8/zqU7zJk3NPeStLj5+9eVGtO0yyl2QjugvXST63Gqbz76cfk2SSJDtPjt5W6n2S3ST9iAPXTZ9kt1KvNrcO+sXV5Xktb8aHsZcpaTW87JafKQFU6w673LwZYtjr4gNOstGNvUD/MgiMQWAMAvNg1YM/Z8f1P4fcdztPj9oq57whMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPA1Kr/+dbd8IbAGATGIDAGgemS/Bl7hG797pJ8GXuFbn3tWg3TsVfoRqthOln8+vRjc+ugT+X52IPWWsu7+ezkbJIki6vL84fb+98q9SjJdpJ+3HVr4zrJRavh9Xx2cpYkfwF0cTwcAXDVkgAAAABJRU5ErkJggg==';
var markup_left_air_unmasked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAABQJJREFUeJzt3M9OG0ccwPHv2BI9JFKMcCRO/LnUeQAEFTJvkLxAXyDqIQRUReQFWgWI5BQ1InmOtFJRopxwpDbJvc4JOGclfO3B2wNeiii2d3bnz2/W85W4wO7OaD/aNfauVzFs4d6T+yqtbQOrw1/9lapB5+zv/d+IGWvSfq4DLH6785NCHQLLwMzwZ1mhvm8027f7Sfedj8lXLLXY2tlXqF+5aT/PtWf6Sfe9Goq9Gbsl1MFJ79kWkDqYeBVTS62nL1LSzXELpWrwoN6Y28iOjHGtzTY3Zs+T4yNjU5yecmEAKNR8jf/OZWNLSTcXWjuHgCo5wWkqN8aw1RoapyEFDyNK7nQxAAY14KPWKPBwqfX0BRFlXEUwAD7VUjXo6I6Wkm5GlJEVxSBVg069//XDl0azfRtY11w/vtD/v8IYwPOz3t7LOkA/6b6bbbYbwHeaG1m702zP95Pu7wUmULWKHxnw+qy3uwXDN4YA50n3qAiKgpWIQlmMHxj+c1W/+seIUihjGHANBCKKZkYx4AYQiCg5M44BI0AgokzICgaMAYGIMiJrGDABBCLKtaxiQA4QiCjDrGNAThCYehQnGKABAlOL4gwDNEFg6lCcYkABEJgaFOcYUBAEKo/iBQNKgEBlUbxhQEkQqByKVwwwAAKVQfGOAYZAIHgUERhgEASCRRGDAYZBIDgUURhgAQSCQRGHAZZAQDyKSAywCAJiUcRigGUQEIciGgMcgIAYFPEY4AgEvKMEgQEOQcAbSjAY4BgEnKMEhQEeQMAZSnAY4AkErKMEiQEeQcAaSrAY4BkEjKMEjQECQMAYSvAYIOsraYV3qEIdwMVX7YqsK+k7+JJAANRSa6eTwmMXg0k6MrJEnLKuVvT0pZtEDBAIAvZRpGKAUBCwhyIZAwSDgHkU6RggHATMoYSAAQGAQIayMQusFVlfoQ5Oe7uPEI4BUPM9ARcNSL/xPYe8hXCElHlcxcUGZNyMlyvpIKUxLjcUCIpkEGMYlxsMAEUqiHGMyw0LR5EIYg3jcgDBKNJArGNcDiQURRJIqY/fFepPNN+nKFiR9hA2KSClLi6d9nYfnSfHf1ThIWwSQIxd6RNyh2SpfIMYv+waOopPEGvXwENG8QVi/YaEUFF8gDi7OyREFNcgzm/VCQ3FJYi3+6ZCQnEF4v0mtlBQXIB4x8gKAcU2iBiMLOkoNkHEYWRJRrEFIhYjSyqKDRDxGFkSUUyDBIORJQ3FJEhwGFmSUEyBBIuRJQXFBEjwGFkSUMqCVAYjyzdKGZDKYWT5RCkKUlmMLF8oRUAqj5HlA0UXZGowslyj6IBMHUaWS5S8IFOLkeUKJQ/I1GNkuUCZBBIxrmUbZRxIxBiRTZRRIBFjQrZQbgKJGDmzgXIdJGJoZhrlKkjEKJhJlAwkYpTMFEodYLG1sw9s604ipCckuKjoEycUrDSa7Vv9pPtWLdx7cl+ltTe6g0t7Epugip9t1OBBvTG3cQgsa40YMcZ2nhwXPFLUfA1Y1VkphdcRY2LpSe/ZloJfNNdbraGxY+MLuFbpSW93WxNlUAM+5llSoQ4ihnZDlIunpuboUy1Vg86kpeJrRqmGp6/JKKkadOr9rx++NObaMyg2Riz3/LS3+yMRo1TnyfFRo9m+BazfuEDKz2e9vVd1gH7SfX/n7vpnhZoH7gL/AMepGmye9fZeOpt1xesn3bdj9vMrgH8BXe/H4/Sd0b0AAAAASUVORK5CYII=';
var markup_left_bone_masked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAh5JREFUeJzt3TFOG0EchfH3H0tbWcKSZYkKi4ZwAhfI3AAuwCEiuUCW0pOCFC7DPUjtKqYIoQdK17jYiHonhVemTQqYJ+33O8GTPu3OTrWh1sHx5VnkNJM0kdQXPsKrpF85msX68dsPSepJ0vhofhWK75IOJVUFB3ZNJekwFBeD4bSqN6tltE/GbellkHI056l9TcFA5DRL2p4Z8DBJknLpFdjpJ0n3pVfgTcrRLEqPwJte/XL3PBhOK4VOS49Bew+pN6vl3ujkIRT7kkbiLlJMlB7QFeNP83/6eErvPQT/hyBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghihiBmCGKGIGYIYoYgZghiZvc/9YPjy7PIaSZpIqlfblK3hSSNj+ZXCn0pPQZStE/Gbekh2ErtawomkrZnBkwkSbn0COz8SZLuS6/Azu+Uo1mUXoGtHM2iV7/cPQ+G00qh09KDOi3r6/rp+qYnSfVmtdwbnTyEYl/SSFJVdl1nvEr6maP5vH66vpGkv/XlTi4olJ2YAAAAAElFTkSuQmCC';
var markup_left_bone_unmasked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAA5BJREFUeJzt3cFLFGEcxvHnHZdICDKkbrkiuCt26ZKHyEt0MzwpQYf+CKE2UkSSDDt079ghCPQQ2L1OgdglIWP3EOY5oSiwQ+3bQX9Nxpq7M+/M/t55n8/JXWbeGfgys7sDvq/BgfJwbQIRZgCMATAA1i3wZKfeuwosNEG5MABQrtYWAcy13MLivYFZ3G6cXGGY7JmDK+PlsVsyTC6ig9vU8QwuWGOflyt7m4OVuzeAhSjjcwuSKVdr3wCc6nxPbNkmlncaH58BK7/cn1qYIgA20Z4Wo8bgaXlkaHOgUrsFTPe4PbUwRQDephpBwlSG3vFWll7P6bOXdw3MzdQjGZyDwVRf/8+pM/3ju192r30AXie7+gLW8/Xzm0Zf/5UTMBh3MiLDpGLkj/PVO5ORMQ9hMer0CPy63BFz+OVCNDCyN2Es7gO46PhI/FbWBtP6bYbpliOCCIbJ2zFBBMPkpc0ggmGy1mEQwTBZSRhEMIxrKYMIhnHFURDBMGk5DiIYJqmMggiG6VTGQQTDtCunIIJhjpNzEMEwR+lSEMEw/+pyEMEwQkkQwTDKgohwwygNIsILozyICCeMJ0FE8cN4FkQUN4ynQUTxwngeRBQnTEGCCP/DFCyI8DdMQYMI/8IUPIjwJ0wgQYT+MIEFEXrDBBpE6AsTeBChJwyDHNL9MAzSUvfCMMh/5R+GQdqyEA1WfkzbyM47/x9Mgy3TtLPbjUcv9l9SBzIMY7H0qbE8yyCJZBPGGkwySCrOw7xiECechfnOeUmUKXX7BPwmV8bevLUYTTiv0t82eMtKJLsPdV4hHXF+RcQslnbqy2u8QtrCH4ZK8NGJEny4qAQfvyvR/RDx5kHTEyLeLUj6QsS7B0VviHiYIOgPEQ9XaP6EiIctJP9CxMMXir8h4sMUgv8h4sN5rTgh4sN6qXgh4sN7pbgh4tPwQvFDxKejWjghhNIg4YUQyoKEG0IoCcIQglP8KcNJMJXhNLHKcCJlZTjVuDKcjF8ZLlehDBd0UYZLHinDRcGU4bJ5ynBhSWW49KoyXJxYGS7frQwXuFfGAEC5UnsAg3tOR2aIRMzAyO3rxkZrzkZkiFRKxkYzTkZiCCdKAC6lGoEf1k6VgITzEUiIOkO4VAKwAeBq23vIranOW1MWSmjiMaI2gvAzIhf7X3urtUUAcy23YIhc/flhWB6uTSDCDICxg/fXLfBkp967yhD5+Q3rsD/YobooMwAAAABJRU5ErkJggg==';
var markup_right_air_masked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAABBZJREFUeJzt3b9uE0EQBvCZrakMrVMlKYhoU8ArRIIGQg1PkBYEBVHc+jWI0oCUpwh0CIokVdwSV9S3FOdZFOzYt3ezOzN3+0mRYnv/TPT5pyuDYDjzrZ0DX/kjQNwHAADvL9DhdHRzdS48Wuug9ABtMx9vn3jAd6s+Q/CT0ez6fe6ZOGKykN/j7ecI+GXdGg/+xaPZ9ddcM3HFSQ8QGw/gEOB40zoEnHiDf5+5gefj3ZcA+KTB0sf1WlsxVUj9ja8ing3+ozUlpoaN0EExp8RMIfU33X9osfODJSVmBl180/dabN2zpMREIfHPjqUTzDxLTAzZ4tnxf8w8S9QX0l1HOMmEEvUDMuigmFCiuhA+HeFE9UpUD8eog6JeidpC+HWEk1UrUTtYpI6fi58mUa1EZSHxOvC4/ml8g1olKoeK1PFrNLs8G80uzwD8j4Z71CpRV0gLHZ8QoEKACsCdRNykUom6gdrooBd9UKKqkLY6wqseKFE1TBcdFOtK1BTSVUd417gSNYNw6KBYVqKiEC4d4VPDSlQMwamDYlWJeCHcOsIqo0rEB0ihg2JRiWghqXSE1QaViF6eUgfFmhKxQlLrCLuMKRG7OIcOiiUlIoXk0hF2G1IicmlOHRQrSrIXkltHOMWIkuwXSuigWFCStRApHeE0A0qyXiapg6JdSbZCpHWEU5UryXaRBh0UzUqyFKJFRzhdsZIsl2jSQdGqJHkh2nSEW5QqSX6BRh0UjUqSFqJVR7hNoZKkh2vWQdGmJFkh2nWEW5UpSXawBR0UTUqSFGJFR7hdkZIkh1rSQdGihL0QazrCFEqUsB9oUQdFgxLWQqzqoGhQwnqYZR0UaSVshVjXQZFWwnZQH3RQJJWwFNIXHRRJJSyH9EkHRUpJ50L6poMipaTzAX3UQZFQ0qmQvuqgSCjptLnPOii5lbQupO86KLmVtN44BB2UnEpaFTIUHZScSlptGpIOSi4l0YUMTQcll5LoDUPUQcmhJKqQoeqg5FAStXjIOiiplTQuZOg6KKmVNF5YdPxLSiWNCik67ialkkaLio7lpFKysZCiY3VSKdm4oOi4PymUrC2k6FifFErWflh0bA63knsLKTqahVvJvR8UHc3DqWRlIUVHXDiVrHyz6IgPl5KlQoqOduFSsvRG0dE+HEruFFJ0dAuHkjsvio7u6aokFFJ08KSrkvBL0cGXLkocQNHBnS5KEADgdrx7COA/pxmvpFnw9cPZ5alL9z9nS+JSK8Hbre1X4PFUepwSAEB/6MDjW+k5Shbx+MYBwFPpOUpCnjlA8NJTlIRUDir/TXqKkkW8/+7Q4VR6jpI66HDqRjdX5wh+Ij3M0IPgJ3UXi8y3dg585Y8AcR8AHgjONqT8Ae8v0OF0dHN1DgDwF0acNbPch/n4AAAAAElFTkSuQmCC';
var markup_right_air_unmasked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAADZtJREFUeJztnXtw3FUVx7/f+9tNSyFpspsmJdndPLrZTSk+8DEyCgi1WFEKCONbGAdExxEfFVF0xnFGdFQsCr51ENE6PkYZ5aGIpcVHQXQG8RVJNps2zauvZPOiaZPd3z3+sZvSpElzf799ZJPNZ6Yz2/Scc09/J/fu777OIYqcsfpWf9KTjEKrqAijpERArIfgbADnAKwC5Oy0NI8BMgzgeQifB+WQCGOkxKCl3QtPrKK/fWgx/z8LwcV2YDaHm5pqPbZ1CTS3gLgcQFOOmzgIcC+Jx2klH6vav/9Aju1nRVEEZLChZaPS8m4hr4MgWtDGBe0kHrBttXPdQEdHQdueg0ULyFhdpHpKyTtIXA/glYvlxyz+LsKdZaJ+tlhDW8EDkqhrDomybgXxXgBrCt2+CQJMUvAjaP15/0BXbyHbLlhARkKRZlvLR4R4P4FVhWo3S5IU/tzW/EKhhrO8ByTR3LxWpqzPgfggACvf7eWJFATfxmr5jD8eH8tnQ3kNSCIU3ibCbwMI5LOdAnJQyNv9PbGdBCQfDeQlIMONrY2i9X0iclk+7C86gt3Km7opH6/MOQ/I0WD4KgX+EIAv17aLjFFQbvb3xH+ZS6M5C4hs2lSWGJ26E8SHc2k3bRxHQDwLQQcp7ZoqprQ6opQ+BpwYXltefgwARsfHzwZWV2mtztZK1yrREZCtIogKcAGBdTn1CxCAd/sqvLezrW0qFwZz8uCONDaut2zvQ8jdfGKC4C5N2aNE76nq7WrLdswWgMOB1vNF2Zsp2CzAFuTutftvKU/q6tr9+w9nayjrgIw0RJtsrR8D0JKlKQ3IX0n1Y3uCP1s32DGerW9nojcQOGuNWn2laN5A4gpk/wa43xZ7a03fvs5sjGQVkOGG8AVa81EAtVmYOUHID2h5dlR1t3dn449bRhqiTSktHyfkRgCr3VviIaXliqr+zn+6tuBWcTjYcokGHgZQ4dLEMQDfsa3kXTXd3Yfc+pFLjjZsPFfp5K0APwD3w9kowW2+3thf3Ci7Ckgi0Poiof1nAJVu9EXwiOWxPrRYPWIhBgORegV8USjXuzQxppRcWnUg/qxTRccBGQmGN9jgk3A3THVT4Rbfgc7futAtOIlQeJtofANkgwv1gxZ5UWVPbJ8TJUcBGV8fXjflxV6AEWe+AYT8RnHqxsqenmGnuovJUDhcgUl8H+DbnOoK0KWt5EVOhmTjgMimTWWJsam9cPhqK8AkwFure2PfcqJXbAwGIreAssPFwujTvpqKS/jMM0kTYWVqNTE6dSeczzOGqOXSpR4MAKjui32TWi4DkHCoemHiyOiXTIWNekgi1HKlCB4ylc8wQLHe4Otr/48DnaJnsKFlIzV+DyDkQE0EvLa6N/abhQQXfMDDja2N2tb/AKTKvHm0Q+vXF3pzp1AM1W0IwqN2OdxuTtC2L/AN7Os5k9CCQ5ZofZ+jYAC9yzkYAOAf6OpFSl8OoM+Bmk8s696FhM4YkEQg/C6HS+hDorB1OQdjGv9AVy8oW+HsO+XyoVD4rWcSmHfISjQ3r5Wkpx2Q9SYtCTBJLZf6++NPO3BwyZMIRV8jovcAKDNU6dfH1cb51urm7yEp6w7TYKQNycdKLRgA4OvpeBLEJxyo1Kuz7M/O949z9pAjgeYWi9ZzMF0BFfzK39f5FgdOLSsEYCLY8gCANxuqpCwyOtcsfs4eYtG6HebL0d0ss99rKLssISDKSt4IEdMtXY+t5ZPz2JrJUN2GICwVh+GYSGKbr6fzEUNHljWDwfDVBBeca2RI0rbDs1+DT+shtPhJmH9B/XolGC9Q3Rt/UNJbEiZ4xbK2z/7hjB4yVhepTlrSA+AsA4PHaNvnLTTRKTUyO6htMHuGE15thU49tjqjh0x55J2GhiDEt1aCcTqVBzr2A/yuofialErNWEWeERCK8YbMCaHnbkPZksO2pu4EcMJEVsAZz/xkQAYbWjYCfIWJEULuXXfguYOOvCwharq7Dwlwv6H4hUfroifXxE4GRGnj3qGVsr7qwL+SxEN+BYA2kVWWfvfJz9MfBLjORJnkn9Lj5ApnorIntk8A04MO105/UABwNBitM92W1SI/duFfSUKRnYai56VjkAkIKVsMFSfkuHrAjXMlyWr8EsCEiagl+lJgOiCGS+wEd+X7ROFywh+Pjwmw20RWyM3A9HeIYUA0ZY9r70oUGj8zSQdkrC5SbXruSKXX/VdwgLL5R0PRptHAJp9Kem2zM1aCI1W9XW2uPStRKvs7/w1g0EQ2JZMRBa3MNuqJZ/N1jWs5w/RcxOjwtVgqokRoFhBBezaOlTKEGN3gVaKjihSjIYuUWHZulS5alNEvs5BRBaHRoWlNtehpJ5YqpDb7ZRZZr0Axut9B6KK4w7EUoViGz47nKIiUm4gqy34+G6dKGWXBdDJdoUCaBSS5ZmWG7hKVFNNnV64AnGMiWbEWKz3EJeVrtFFAJBOQFYoIBZj95o+NmvWkFU5nfEIZfS0QGFcQs/FNeyeMjK5wOtpr9j0NYEyBNBvfUmZRXuF0tA3TZzeuIDTK/6RpGR+8XmEmQtv02Y0rUIzycyjRjm/erpBGRJk9O+KwEqHRkojxIuQKp6GoW03kKGhXxouGLHD61mWEwOyXWZMxBaXNegjwMimSPL9LCUlPLS4wkaWtY8pjrzZaGiawbjjQen5W3pUgIw3hlwDwm8h6uCqm1va1JQB0myiIsjdn4VtJorUyvTS7b21fW2J66eQJEw0KVgLiEPNbzHwCyBwDEopRQAS4PNHcvNatc6XGUDhcQeJ1JrKE7AamAyKW0WEuAGdJ0rp2YbEVAAAn1Fthdt9GUlbyhR6yrrdjwPQQA4Eb3HtYYpgnQPvfdAqnk8vvJIzO7ApwyUgo0uzCvZJiJBjeAOBiI2Hy5LM/GRBbW6an2lVKcKsj70oQG7wNhvM2neJPpz/PUBgKtvwdBjmxBJgUqOZ1vR0DTh0tBYbqwwFRjBslOyP+6u/pfPX0X2fsGIrA6D4DgVUK9scce1oqkLeZZp6bfYckm2vRE8qyNhVrZtHFIpMk9L8wy/874ZGyYGZyDmBWD6kYiA0S8gPDtteIbd/jwNeSwBbeA/NkzN8/NRjAHJkcxJY7ARgllhfgqkQovM2w8WXPYCDyZhBvMpEVYFKEO2b//LSApJOP0fRuHETjG8ONja4SKi8nRkKhKlKM7+4TuL+6L9Y/++dzHgOyxf4SgJSZZTZo7V0wdd1yRgBqKbsP5okxk5ZSX57rH+YMSE1fVxwC89SugusGA5FbjOWXGYlg+KMCXmOsQNw939XyeScu6YzObAdwrkkbAkxS6c3+A11PGTu2DEiEIheJyG6YZ1Dqs0+Ubaw52jbnebh5Ty764/ExwnxGTmAVtPrdcH3kJaY6S52hUHiTiDwI82AAlO3zBQMwmNoPBVoeh+EScoY+2PrVyz0zaaKuOSSW9SScVaD7g7+3c+uZBBY826u8qZsAOkmgH4BH7UrUNTvJ/LykSAdD7YKzYAzRtm9eSGjBgFTt33+AlBvg5MKnICqW9XQiEH2xsc4SYSgUPk8sy2mFCBHITSb5xYxOv6fT+NFpfqxzhfqJRCj6God6RUsiGLkYovYCCDpUvau6N/6giaDxdQRfhfd2AH9z6IhPRO8ZCrV8ZCkfIRKAQ6HwRwXyuMO064DwKV9NxadNxZ0XdCnjX9zUPCfwkOLke5ZmQRd1LyCO8xITiCc9qYuclNNzdGGn/FD8qAW+EaDjC6ACXGXLqn8OBsNXO9VdLAaDkWswyf+4CQaAg0qp1zutbZhFUTD9J8fdN4MIHvFY6sPFmggtU2r866YLhXMwpjRe66Z8nutxPRGMXCyQhwG4PRZ0HMD3RLhjrkW2xWAwEKkn5DYQ74f7eoajJK/09cT2ulHOrrBkfctLteKjTpL2zyZdowo/9JBfcVrRLFdkNpVuE+A9LmpMncpBpXlFVX/sX24NZP3mk67Ak3rMTeW2053hM0LZ6bWtn5yaXDgfJJqb1+qU5+pMatzXIftnsc8WvbWmryuejZGcvIoeqt1Q4y1TDwK4MBf2ABwXwW5S9iit9lT2x/6dg+LEaqS+5cVayWUi3ExiC7Iqs3qqcT5VltLXlB+KH83WVO7Kd7/85d50NTJuz6XdDIMAniUkpqGeIyWmbB6hhXFq78j0HfqxUZwjKlkpNsq10rUiKqKoWzP3M14KoDrHfgmAu3w1FZ82LYu3EDmfrCVC4W0ivB+lUOBecJO/rzOnSUHzMnsebmpq0CnPvUjXLF+O/IG2fXM+ct/ndTkj01u+CWc1/4qZASE/Vd0Ty1vu4rym1vD1xB/Wx9X5AL8G0z364iQJYId9oiyaz2AABVzwG25sbdQpezuI9yFXbzf5Z4rCX6SQuqOmb19nIRos+Ars4aamWivp3U7Kh+C+iHxeEWCSgh9B5A5/f9xJ8cisWbQl8bH6Vv+U0m8n5HoAr1osP2bxNCE7LVn189knCgtFUexRHK1vjShlX490hYaNBW6+DeQDtk79pFDD0pkoioCcyqHaDTXeVXwtNLcgPZvO9eWggwD3knhcUvajxXYYo+gCMpuRUKgqpVdFoNiqREeFjEBQC6A886cSL2TFex7ACIBxAOMgDlMkpqk6oKXdoyZjxb5B9n99PS6m8V+ykQAAAABJRU5ErkJggg==';
var markup_right_bone_masked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAdxJREFUeJzt3TFuWkEURuF7Ry6pSA2V2QILSI0X89qkNi2L8VtF4h1Aheu8xqnfTQGOkNKkSeZInG8Fv3Q0Gk01GVfTerOruYbI3EbEIvQ//Iyqb9nysDwfx4iIjIiYVo/PFfml77b7llH75dvpa07rza4qXnoPUkRmPLWaa+g9RBc119Cud4YIMrctvMBJ5tZ7gW5UvRoEJFseHnqP0EVG7Zfn02iQvm4ehqcxIuKvg3x6O+a/26UP3iEwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8AYBMYgMAaBMQiMQWAMAmMQGIPAGATGIDAGgTEIjEFgDAJjEBiDwBgExiAwBoExCIxBYAwCYxAYg8Dkj9Wmeo+4Yzcf3B/HCE9Ib4vI/FwVL9Pq8TnCE4KSGU+eEJCaazAISea2RcZ77x36bW4x1/feK3RV9dqy5aH3Dl1ky0Nbno9jRu17j7l3GbW/tLia1ptdzTVE5jYiFh233ZM/Hoa/AFt5YTllbWrbAAAAAElFTkSuQmCC';
var markup_right_bone_unmasked = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAACcQAAAnEAGUaVEZAAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAxZJREFUeJzt3T1PFFEUxvHnDHyCJZa71UqBlYXhQwgdxo9AbalmCw0SbfBTmJjYEWmhx9KEgqWBrV0TpWWOhRxe4oK7M3d2zr33+Zeb5ewmv8y+zN7LCNjcUqAY9/obKLEJkVUACtVDWZCdzulwDwCk5eeYRQoU4+7yM0AHAB7dcbetpdFwQJAGmxLiKimwRpAGmhXi+g91nyABU6D42es/LUt5I4LHFUacEyRAASD+JvhNkBpVfmm6IxE5IEiFQkNYIlhfDDUshwxiDB0AGgwCAAS63Tk7+cojZIqaOiIuOyqhLx+MTnYBfjG8t2Bv1pM7UpEPS2fHnwS4sBsJMqE2ICyC3KhNCIsg8AFhZQ3iCcLKEsQjhJUViGcIKwuQGCCspEFigrCSBIkRwkoKJGYIKwmQFCCsqEFSgrCiBEkRwooKJGUIKwqQHCAs1yA5QVguQXKEsFyB5AxhuQAhxHWtghDi31oBIcTdzRWEEP9vLiCEmL5GQQgxe42AEKJ6QUEIUb8gIIQIVy0QQoSvEgghmmsmEEI031QghJhf94IQYv5NBCFEe90CIUT7XYGMe/11VXkPYCXwYxwB8rYzOv4iQBl4dnIJAIy7/XcKeRV4NiEqJJdHxm7AmYSo0SJQvAA0xCxCBGhRVZ/UnEGIgBVtPwF2u0JEvtWcsQLo53H34fcf3eXnSuRaFUD5MdAswgSIH3udxS+GzuKpE2fx5KKzePrdWfyByln8CddZXOTgLC4DchYXyjmLS0mdxcXWzuJ2BGdxw46zuKXNWdz06Sxui3YW/3GAs1yAWIRxBmLlDOMSxMoRxjWIlRNMFCBWDjBRgVgpw0QJYqUIEzWIlRJMEiBWCjBJgVgxwyQJYsUIkzSIFRNMFiBWDDBZgVieYbIEsTzCZA1ieYIhyI08wBBkQrw4sdN4+W6n8QL3TmsSRqDbndHJa4JUqCkYEawTpEahYUTkgCABCvjmf06QgAWA+UWQBqr8Uqa6T5AGmxVGCqwRZA5NCbO1NBoOCDLHFCjGvf4GSmxCZBWAQvVQFmSnczrcA4A/BLrXTL8v6FkAAAAASUVORK5CYII=';

var audiometry_chart;

function init_audiometry(data_source_selector) {
    var data = jQuery.parseJSON(data_source_selector);
    audiometry_chart = $("#test_chart").dxChart({
        dataSource: data,
        commonSeriesSettings: {
            argumentField: "frequency",
            type: "line",
            point: {
                visible: true
            }
        },
        "export": {
            enabled: false
        },
        tooltip: {
            enabled: true
        },
        legend: {
            visible: false,
            verticalAlignment: "bottom",
            horizontalAlignment: "center"
        },
        title: {
            text: "Audiometria tonale"
        },
        argumentAxis: {
            wholeRange: [0.5, 8.5],
            visualRange: [0.5, 8.5],
            grid: {
                visible: true
            },
            position: 'top',
            label: {
                customizeText: function () {
                    if (this.valueText == 1) {
                        return "250 Hz";
                    } else if (this.valueText == 2) {
                        return "500 Hz";
                    } else if (this.valueText == 3) {
                        return "1000 Hz";
                    } else if (this.valueText == 4) {
                        return "2000 Hz";
                    } else if (this.valueText == 5) {
                        return "3000 Hz";
                    } else if (this.valueText == 6) {
                        return "4000 Hz";
                    } else if (this.valueText == 7) {
                        return "6000 Hz";
                    } else if (this.valueText == 8) {
                        return "8000 Hz";
                    }
                }
            },
            allowDecimals: false
        },
        valueAxis: {
            wholeRange: [-10, 120],
            visualRange: [0, 110],
            tickInterval: 10,
            grid: {
                visible: true
            },
            inverted: true,
            label: {
                customizeText: function () {
                    if (this.valueText == -10) {
                        return "";
                    } else {
                        return this.valueText + " dB";
                    }
                }
            }
        },
        series: [{
            type: "line",
            valueField: "hearing_level_right_air_unmasked",
            name: "Orecchio destro / senza mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_air_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air_unmasked",
            name: "Orecchio sinistro / senza mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_air_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_air_masked",
            name: "Orecchio destro / con mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_air_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air_masked",
            name: "Orecchio sinistro / con mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_air_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone_unmasked",
            name: "Orecchio destro / senza mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_bone_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone_unmasked",
            name: "Orecchio sinistro / senza mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_bone_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone_masked",
            name: "Orecchio destro / con mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_bone_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone_masked",
            name: "Orecchio sinistro / con mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_bone_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_air",
            name: "Orecchio destro / via aerea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air",
            name: "Orecchio sinistro / via aerea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone",
            name: "Orecchio destro / via ossea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone",
            name: "Orecchio sinistro / via ossea",
            color: "#444444",
            point: {
                visible: false
            }
        }]
    }).dxChart("instance");
};

function test_charts(data_source_selector) {
    console.log(data_source_selector);
    audiometry_chart = $("#test_chart").dxChart({
        dataSource: data_source_selector,
        commonSeriesSettings: {
            argumentField: "frequency",
            type: "line",
            point: {
                visible: true
            }
        },
        "export": {
            enabled: true,
            formats: ["PNG", "PDF", "JPEG", "GIF", "SVG"]
        },
        tooltip: {
            enabled: true
        },
        legend: {
            visible: false,
            verticalAlignment: "bottom",
            horizontalAlignment: "center"
        },
        title: {
            text: "Audiometria tonale"
        },
        argumentAxis: {
            wholeRange: [0.5, 8.5],
            visualRange: [0.5, 8.5],
            grid: {
                visible: true
            },
            position: 'top',
            label: {
                customizeText: function () {
                    if (this.valueText == 1) {
                        return "250 Hz";
                    } else if (this.valueText == 2) {
                        return "500 Hz";
                    } else if (this.valueText == 3) {
                        return "1000 Hz";
                    } else if (this.valueText == 4) {
                        return "2000 Hz";
                    } else if (this.valueText == 5) {
                        return "3000 Hz";
                    } else if (this.valueText == 6) {
                        return "4000 Hz";
                    } else if (this.valueText == 7) {
                        return "6000 Hz";
                    } else if (this.valueText == 8) {
                        return "8000 Hz";
                    }
                }
            },
            allowDecimals: false
        },
        valueAxis: {
            wholeRange: [-10, 120],
            visualRange: [0, 110],
            tickInterval: 10,
            grid: {
                visible: true
            },
            inverted: true,
            label: {
                customizeText: function () {
                    if (this.valueText == -10) {
                        return "";
                    } else {
                        return this.valueText + " dB";
                    }
                }
            }
        },
        series: [{
            type: "line",
            valueField: "hearing_level_right_air_unmasked",
            name: "Orecchio destro / senza mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_air_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air_unmasked",
            name: "Orecchio sinistro / senza mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_air_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_air_masked",
            name: "Orecchio destro / con mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_air_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air_masked",
            name: "Orecchio sinistro / con mascheramento / via aerea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_air_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone_unmasked",
            name: "Orecchio destro / senza mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_bone_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone_unmasked",
            name: "Orecchio sinistro / senza mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_bone_unmasked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone_masked",
            name: "Orecchio destro / con mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_right_bone_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone_masked",
            name: "Orecchio sinistro / con mascheramento / via ossea",
            color: "#ffffff00",
            point: {
                image: { url: markup_left_bone_masked, width: 25, height: 25 }
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_air",
            name: "Orecchio destro / via aerea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_air",
                name: "Orecchio sinistro / via aerea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_right_bone",
            name: "Orecchio destro / via ossea",
            color: "#444444",
            point: {
                visible: false
            }
        }, {
            type: "line",
            valueField: "hearing_level_left_bone",
            name: "Orecchio sinistro / via ossea",
            color: "#444444",
            point: {
                visible: false
            }
        }]
    }).dxChart("instance");
};

function export_chart(writeUri) {
    return new Promise((completed, error) => {
        var chart_svg = audiometry_chart.svg();
        var chart_blob = new Blob([chart_svg], { type: 'image/svg+xml' });

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = () => {
            if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                completed();
            }
        }
        xhr.open("PUT", writeUri);
        xhr.send(chart_blob);
    });
};

function convert_chart(writeUri) {
    return new Promise((completed, error) => {
        var chart_svg = audiometry_chart.svg();
        var chart_blob = new Blob([chart_svg], { type: 'image/svg+xml' });

        $("#myImg").attr('src', 'data:image/svg+xml;charset=utf-8,' + encodeURIComponent(chart_svg));
    });
};