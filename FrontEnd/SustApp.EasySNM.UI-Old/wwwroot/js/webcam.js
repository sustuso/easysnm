﻿const constraints = {
    video: true
};

function start_webcam_function() {
    var video = document.querySelector("#videoElement")
    if (navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).
            then(function (stream) {
                video.srcObject = stream;
            }).catch(function (err0r) {
                console.error('Error: ', err0r);
            });
    }
};

function stop_webcam_function() {
    var video = document.querySelector("#videoElement");
    var tracks = video.srcObject && video.srcObject.getTracks();
    if (tracks) {
        tracks.forEach(function(track) {
            track.stop();
        });
    }
};

function take_screenshot_function(writeUri) {
    return new Promise((completed, error) => {
        var video = document.querySelector("#videoElement")
        var canvas = document.querySelector("#canvasElement");
        var img = document.querySelector("#imgElement");
        var ctx = canvas.getContext('2d');
        canvas.width = video.videoWidth;
        canvas.height = video.videoHeight;
        ctx.translate(video.videoWidth, 0);
        ctx.scale(-1, 1);
        ctx.drawImage(video, 0, 0);
        img.src = canvas.toDataURL('image/jpeg');

        canvas.toBlob(b => {
                var xhr = new XMLHttpRequest();
                // TODO: gestione errori o attesa
                // xhr.onerror
                xhr.onreadystatechange = () => {
                    if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                        completed();
                    }
                }
                xhr.open("PUT", writeUri);
                xhr.send(b);
            },
            'image/jpeg');

        $(video).hide();
        $(img).show();
    });
};