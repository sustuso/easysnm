﻿var interop = new function () {

    var self = this;

    var callbacks = {
    };

    var startConnection = function (connection) {
        var factory;
        factory = c => {
            connection.start().then(() => {
                console.log("Connected");

                c();
            }).catch(e => {
                // TODO: gestione errore SignalR
                //alert("Cannot connect: " + e);
                console.error(e);

                setTimeout(() => factory(c), 2000);
            });
        };

        return new Promise(completed => factory(completed));
    };

    //Connect to signalR Hub on SustApp.EasySNM.Jobs project for "Push" job
    this.connectToPush = function (uri, accessToken, callback) {

        self.connection = new signalR.HubConnectionBuilder()
            .withUrl(uri, {
                accessTokenFactory: s => {
                    return accessToken;
                }
            })
            .configureLogging(signalR.LogLevel.Warning)
            // Reconnect automatically after 2sec, forever
            .withAutomaticReconnect({ nextRetryDelayInMilliseconds: retryContext => 2000 })
            .build();

        //No Promise:
        self.connection.on("Push", (message) => {
            console.log(message);

            callback.invokeMethodAsync('pushReceived', JSON.stringify(message));
        });

        return startConnection(self.connection);
    };

    //Connect to signalR Hub on SustApp.EasySNM.UI.SysTray project for external devices
    this.connectToTray = function (callback) {
        self.connection = new signalR.HubConnectionBuilder()
            .withUrl("http://localhost:2020/services")
            .configureLogging(signalR.LogLevel.Warning)
            .withHubProtocol(new signalR.protocols.msgpack.MessagePackHubProtocol())
            // Reconnect automatically after 2sec, forever
            .withAutomaticReconnect({ nextRetryDelayInMilliseconds: retryContext => 2000 })
            .build();


        //self.connection.on("Login", thumbprint => {
        //    console.log(thumbprint);

        //    callback.invokeMethodAsync('loginReceived', thumbprint);
        //});

        self.connection.on("CmdEjected", () => {
            callback.invokeMethodAsync('cmdEjected');
        });

        self.connection.on("ErrorOperation", errorMessage => {
            console.log(errorMessage);

            callback.invokeMethodAsync('errorOperationReceived', errorMessage);
        });

        self.connection.on("MessageOperation", message => {
            console.log(message);

            callback.invokeMethodAsync('messageOperationReceived', message);
        });

        //self.connection.on("SmartCardDataReceived", (guid, data, pcType) => {
        self.connection.on("SmartCardDataReceived", (data) => {

            callback.invokeMethodAsync('smartCardDataReceived', JSON.stringify(data));
        });

        self.connection.on("CmdBackOfficeDataReceived", (data) => {
            console.log(data);

            callback.invokeMethodAsync('cmdBackOfficeDataReceived', JSON.stringify(data));
        });

        self.connection.on("CardInserting", () => {
            callback.invokeMethodAsync('cardInserting');
        });

        self.connection.on("SmartCardRemoved", () => {
            callback.invokeMethodAsync('smartCardRemoved');
        });

        //Acquired signature image:
        self.connection.on("AcquiredBiometricSignature", uri => {

            if (typeof callbacks.signBiometricWacom === "undefined") {
                return;
            }

            //console.log(uri);

            // Call blazor caller:
            callbacks.signBiometricWacom.completed(uri);
            callbacks.signBiometricWacom = undefined;
        });

        self.connection.on("FailedBiometricSignature", errorMessage => {

            if (typeof callbacks.signBiometricWacom === "undefined") {
                return;
            }

            console.log(errorMessage);

            // Call blazor error message:
            callbacks.signBiometricWacom.error(errorMessage);
            callbacks.signBiometricWacom = undefined;
        });

        //User Logged completed promise:
        self.connection.on("UserLoggedCompleted", userLoggedResponse => {

            if (typeof callbacks.userLogged === "undefined") {
                return;
            }

            // Call blazor caller and return UserLoggedResponse object:
            callbacks.userLogged.completed(userLoggedResponse);
            callbacks.userLogged = undefined;
        });

        //Digital signature:
        self.connection.on("DigitalSigningCompleted", hasError => {

            if (typeof callbacks.digitalSigning === "undefined") {
                return;
            }

            //console.log(uri);

            // Call blazor caller:
            callbacks.digitalSigning.completed(hasError);
            callbacks.digitalSigning = undefined;
        });

        self.connection.on("FailedDigitalSigning", errorMessage => {

            if (typeof callbacks.digitalSigning === "undefined") {
                return;
            }

            console.log(errorMessage);

            // Call blazor error message:
            callbacks.digitalSigning.error(errorMessage);
            callbacks.digitalSigning = undefined;
        });

        self.connection.on("ScanWIADocumentReady", uri => {
            if (typeof callbacks.scanWIADocument === "undefined") {
                return;
            }

            // Create blob as bmp
            //var blob = new Blob([data], { type: "image/bmp" });
            //var blob = new Blob([data], { type: "application/pdf" });
            // Create a browser universal URI
            //var uri = URL.createObjectURL(blob);
            //console.log(uri);

            // Call blazor
            callbacks.scanWIADocument.completed(uri);

            callbacks.scanWIADocument = undefined;
        });

        self.connection.on("ScanDocumentReady", uri => {
            if (typeof callbacks.scanDocument === "undefined") {
                return;
            }

            // Create blob as bmp
            //var blob = new Blob([data], { type: "image/bmp" });
            //var blob = new Blob([data], { type: "application/pdf" });
            // Create a browser universal URI
            //var uri = URL.createObjectURL(blob);
            //console.log(uri);

            // Call blazor
            callbacks.scanDocument.completed(uri);

            callbacks.scanDocument = undefined;
        });

        self.connection.on("FailedScanDocument", errorMessage => {

            if (typeof callbacks.scanDocument === "undefined") {
                return;
            }

            console.log(errorMessage);

            // Call blazor error message:
            callbacks.scanDocument.error(errorMessage);
            callbacks.scanDocument = undefined;
        });

        ////Activate Wacom tablet:
        //self.connection.on("SignTabletActivate", () => {
        //    if (typeof callbacks.signBiometricWacom === "undefined") {
        //        return;
        //    }       

        //    // Call blazor
        //    callbacks.signBiometricWacom.completed("Tavoletta attivata. Prego apporre la firma");

        //    callbacks.signBiometricWacom = undefined;
        //});

        return startConnection(self.connection);
    };

    this.uploadUri = function (uri) {
        return new Promise((completed, error) => {
            console.log("Uploading " + uri);
            //var xhr = new XMLHttpRequest();
            //xhr.open("POST", "http://test.it")
            //xhr.send(blob);

            setTimeout(() => completed(), 3000);

            URL.revokeObjectURL(uri);
        });
    };

    this.scanDocument = function (tmpUri) {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.scanDocument = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("ScanDocument", JSON.stringify(tmpUri)).catch(e => {
                callbacks.scanDocument = undefined;

                // TODO: gestiore errore
                error("Impossibile scannerizzare");
            });
        });
    };

    this.scanWIADocument = function (tmpUri) {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.scanWIADocument = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("ScanWIADocument", JSON.stringify(tmpUri)).catch(e => {
                callbacks.scanWIADocument = undefined;

                // TODO: gestiore errore
                error("Impossibile scannerizzare");
            });
        });
    };


    //Chiamata al ServerHub SENZA promise (quindi senza aspettare un ritorno --> Fire and Forget)
    this.pdfUriSigned = function (pdfUri) {
        //Call SysTray server Hub:
        self.connection.invoke("NotifyPdfUri", pdfUri).catch(e => {

            //Cristian come faccio a comunicarlo alla pagina chiamante?
            error("Impossibile comunicare il Pdf Viewer");
        })
    }

    this.userLogged = function () {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.userLogged = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("UserLogged").catch(e => {
                callbacks.userLogged = undefined;

                // TODO: gestiore errore
                error("Impossibile recuperare l'utente loggato");
            });
        });
    };

    this.signBiometricWacom = function (biometricSignatureRequest) {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.signBiometricWacom = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("SignBiometricWacom", JSON.stringify(biometricSignatureRequest)).catch(e => {
                callbacks.signBiometricWacom = undefined;

                // TODO: gestiore errore
                error("Impossibile firmare");
            });
        });
    };

    this.digitalSigning = function (signatureRequest) {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.digitalSigning = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("DigitalSigning", JSON.stringify(signatureRequest)).catch(e => {
                callbacks.digitalSigning = undefined;

                // TODO: gestiore errore
                error("Impossibile firmare con la CMD");
            });
        });
    };

    this.monitorDevices = function () {
        return new Promise((completed, error) => {
            // Keep a reference to promise callbacks
            callbacks.monitorDevices = { completed, error };

            //Call SysTray server Hub:
            self.connection.invoke("MonitorDevices").catch(e => {
                callbacks.monitorDevices = undefined;

                // TODO: gestiore errore
                error("Errore Monitor Device");
            });
        });
    };

    return self;
}();