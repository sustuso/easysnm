﻿window.PagetabsWidth = function () {
    $('.selpro_pagetabs > ul.nav-tabs > li').each(function () {
        /*wid = $(this).width() + 7;*/ //Vecchio metodo
        title_string = $(this).find("span:not(.image)").text();
        title_i_len = (title_string.match(/i/g) || []).length;
        title_I_len = (title_string.match(/I/g) || []).length;
        title_l_len = (title_string.match(/l/g) || []).length;

        title_short_letter_len = title_i_len + title_I_len + title_l_len;
        title_long_letter_len = title_string.length - title_short_letter_len;

        title_wid = (title_long_letter_len * 9) + (title_short_letter_len * 3); //Larghezza massima testo span
        padding_wid = 15; //Spaziature
        icon_wid = 34; //Spazio per l'icona
        //console.log(title_string + " title_wid: " + title_wid);

        wid = title_wid + padding_wid + icon_wid;
        //console.log(title_string + " wid: " + wid + 'px');
        $(this).find("span:not(.image)").css('width', title_wid + 'px');
        $(this).find("span:not(.image)").css('display: inline-block');
        $(this).css('width', wid + 'px');
    });
};

window.FocusElement = function (selector) {
    $(selector).focus();
}

window.HideProgressPanel = function () {
    this.setTimeout(function () {
        var elem = this.document.querySelectorAll(".dx_upload .dxuc-file-view")[0];
        elem.classList.add("d-none");
    }, 1000);

}
window.ShowProgressPanel = function () {
    var elem = this.document.querySelectorAll(".dx_upload .dxuc-file-view")[0];
    elem.classList.remove("d-none");
}

window.ChangeUrl = function (url) {
    if (window.location.pathname.split("/")[1] != url) {
        history.pushState(null, '', url);
    }
}

window.ReplaceUrl = function (url) {
    if (window.location.pathname.split("/")[1] != url) {
        history.replaceState(null, '', url);
    }
}

window.PopulateIframe = function (selector, url) {
    var iframe = $(selector)[0];
    iframe.contentWindow.location.replace(url);
}

window.GoBackUrl = function () {
    history.back();
}

window.JsDebug = function (message) {
    alert(message);
};

window.ActivateImageTooltip = function () {
    $('.image_tooltip_init').tooltip({
        animated: 'fade',
        placement: 'right',
        html: true
    });
};

window.ChangeZIndex = function (selector, zi) {
    $(selector).css('z-index', zi);
}


//###########################
//######## VIEWER.JS ########
//###########################

window.ShowZoomableImage = function (imgUrl) {
    if (!imgUrl) {
        imgUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGoAAABqCAQAAAD+KAwcAAADQElEQVR42u3c61LiMBiA4bdtSkXBw+5F7P1fxF6EF6GCQOkx2R8yLp6gSZM2dJLO+MMhlSenJt/XMfqrmFwR8GdipEdiJlgCKqACKqACKqACKqACKqACKqACKqCOj/MuS01JTUsLQERCyoyM6DJRioKc5tPvGhr2RMy5Jrk0VMMr9Qlwzp4F15eEKlmjzvbkhppbJwMxdkFa0S2YWHTAe4GqWWk1wNZ/lGKtWSOn9B2VHxZvnbKxPgRju/20M6jVUviM2hu2ee4zyrTFm08PaY9Q8sTj9lypfEXVPep6i2pGqusU1Y5U1/GcGq+2p4dENUXUJI/zsZ/36neryE9UMlLdgNIt6Uh1HfdUPD0UzEao6RyVGZMin3vK7Mtd+fzwjY36KvIbhVHMdW49oGkZlRqsY/aDz9b3fkttUuI/KtWaIbGTJIGDXfpS46ZLJwmd2MUtbzsv5VdwGSjIWHQaqLdwOSi44eYs6d5ZktTZyXdxsh8y7h0euh0msucIXr+J6EUsmTsNDTjNzqf8pmRP9R4rElwxdx4YcfzKAWRkQIMChONXDRyhFDUlFXD/4Qkkvvnklj0ZMzLrPWcNJSkpjwbaE3cn9uySNRVQUACCjMzi6dcKqib/kptSrFj8sLA3vHwIMzc07BDMLe3Ye6NaNj+morfU3H35msUPye6GDTuWFnYZPYdzztPJ7HrJ84eMhmJzMn8vWbPqnQOJ+ywK6w6Z9eaILVl1yO+WPPdK4PVAKVYdc7yKFbsDr+q46Lz0ertCmJN0UppbKq3PK9Y8GK+Hhj210c7SVgbN1g6JKtgPsC+QvBqm4gxQLRuGKZVh4xmgcqv52XNzUQ6Baq2/dHN6ZuVDoIYkvf095R5VDIxSBk8sTVQ94Hz6v8NwjKoYvlTue2r4IrXfXNJENYxRWrcoOUXUOP+8QrlFJaOgdJ87mkePX9RUVAPNrYiUGTPtI4jQbbO3OJ6kPlwuBmRCSkpqHCcUpgMiOwTAWprD1fYCxggEgoS0d0RJ9G/V5D2+J2lpkUhaFBKFQn6a6NH7FROTHH4mVmO3wu6Eji2/kDNw4MXnElABFVABFVABFVABFVABFVABFVABdXycf5wc6h/Cxehjvx8lbAAAAABJRU5ErkJggg==";
    }

    var image = new Image();
    image.src = imgUrl;

    var viewer = new Viewer(image, {
        hidden: function () {
            viewer.destroy();
        },
        navbar: 0,
        toolbar: {
            zoomIn: 1,
            zoomOut: 1,
            oneToOne: 1,
            reset: 0,
            prev: 0,
            play: 0,
            next: 0,
            rotateLeft: 1,
            rotateRight: 1,
            flipHorizontal: 1,
            flipVertical: 1,
        },
        title: 0,
    });

    viewer.show();
}
