﻿//using SustApp.EasySNM.UI.Components.UIElements.MainHeader;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Shared
{
    public partial class MainLayout
    {
        //MainHeader MainHeaderComponent;
        private bool _sidebarStatus = false;

        EventCallback _eventMain = EventCallback.Empty;
        EventCallback EventMain
        {
            get
            {
                if (_eventMain.Equals(EventCallback.Empty))
                    _eventMain = EventCallback.Factory.Create(this, RefreshComponent);
                return _eventMain;
            }
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await JsRuntime.InvokeVoidAsync("PagetabsWidth");
            }
        }

        public void ToggleSidebar(bool placeholder = true)
        {
            if (!placeholder)
            {
                _sidebarStatus = false;
                //MainHeaderComponent._sidebarStatus = false;
            }
            else
            {
                _sidebarStatus = !_sidebarStatus;
            }
            if (IsLandingPage() || IsBackofficePage())
            {
                StateHasChanged();
            }
        }

        private string SidebarVisibility()
        {
            if (_sidebarStatus)
            {
                return "visibility: visible; opacity: 1;";
            }
            else
            {
                return "visibility: hidden; opacity: 0;";
            }
        }

        private bool IsLandingPage()
        {
            var path = NavigationManager.Uri.Split(NavigationManager.BaseUri)[1].Split("/");
            if (path.Length <= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool IsBackofficePage()
        {
            var path = NavigationManager.Uri.Split(NavigationManager.BaseUri)[1].Split("/");
            if (path.Length > 1)
            {
                if (path[0] == "BackOffice")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public void RefreshComponent()
        {
            //MainHeaderComponent.Refresh();
        }
    }
}
