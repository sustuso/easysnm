﻿using System;
using System.Collections.Generic;
using System.Text;
using Blazored.Modal;
using Blazored.Toast;
using SustApp.EasySNM.UI.Forms.Services;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSelProForms(this IServiceCollection services)
        {
            services.AddBlazoredModal();
            services.AddBlazoredToast();

            return services;
        }
    }
}
