﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Forms
{
    public readonly struct LongString
    {
        private readonly string _value;

        public LongString(string value)
        {
            _value = value;
        }

        public override string ToString()
        {
            return _value;
        }

        public static implicit operator LongString(string value)
        {
            return new LongString(value);
        }
    }
}
