﻿using System.Threading.Tasks;
using Blazored.Toast.Services;
using Microsoft.AspNetCore.Components;

namespace SustApp.EasySNM.UI.Forms.Services
{
    public interface IDialogService
    {
        async Task Show<TComponent>(string title)
            where TComponent : ComponentBase
        {
            await Show<TComponent, object>(title, null);
        }

        async Task Show<TComponent>(string title, object parameter, string cssClass = "")
            where TComponent : ComponentBase
        {
            await Show<TComponent, object>(title, parameter, cssClass);
        }

        Task<TResult> Show<TComponent, TResult>(string title, object parameter, string cssClass = "")
            where TComponent : ComponentBase;

        Task ShowErrorAsync(string title, string message);

        Task<bool> ConfirmAsync(string title, string message);

        Task ToastAsync(string title, string message, ToastLevel toastLevel);

        /// <summary>
        /// Show message rendering HTML unescaped
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        Task ShowUnescapedMessageAsync(string title, MarkupString message);
    }
}
