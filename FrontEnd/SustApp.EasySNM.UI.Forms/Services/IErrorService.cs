﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Forms.Services
{
    public interface IErrorService
    {
        /// <summary>
        /// Call async method using CancellationToken without spinner animation
        /// </summary>
        /// <param name="task"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task RunAsync(Func<CancellationToken, Task> task, CancellationToken token = default) => RunAsync(task, null, token);

        /// <summary>
        /// Call async method using CancellationToken and start spinner animation
        /// </summary>
        /// <param name="task"></param>
        /// <param name="spinnerContext"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        Task RunAsync(Func<CancellationToken, Task> task, SpinnerContext spinnerContext, CancellationToken token = default);

        bool HasError { get; set; }
    }
}
