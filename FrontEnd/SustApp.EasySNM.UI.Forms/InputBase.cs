﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;

namespace SustApp.EasySNM.UI.Forms
{
    public abstract class InputBase<TItem> : ComponentBase
    {
        [Parameter] public bool Disabled { get; set; }
        //[Parameter] public string Label { get; set; }
        //[Parameter] public string Help { get; set; }
        [Parameter] public TItem Value { get; set; }
        [Parameter] public EventCallback<TItem> ValueChanged { get; set; }
        [Parameter] public Expression<Func<TItem>> ValueExpression { get; set; }
        [Parameter(CaptureUnmatchedValues = true)]
        public IReadOnlyDictionary<string, object> AdditionalAttributes { get; set; }
        [Parameter] public bool IsTextArea { get; set; }
        [Parameter] public string Placeholder { get; set; }

        protected RenderFragment Input { get; set; }

        protected override void OnParametersSet()
        {
            base.OnParametersSet();

            if (ValueExpression != null)
            {
                if (String.IsNullOrWhiteSpace(Placeholder))
                {
                    var memberExpression = ValueExpression.Body as MemberExpression;
                    Placeholder = memberExpression.Member.GetCustomAttribute<DisplayAttribute>()?.GetName() ??
                            memberExpression.Member.Name;
                }
            }

            Input = RenderInput;
        }

        protected virtual void OpenComponent(RenderTreeBuilder builder)
        {
            Type itemType = typeof(TItem);
            //itemType = Nullable.GetUnderlyingType(itemType) ?? itemType;

            if (itemType == typeof(string))
            {
                if (IsTextArea)
                {
                    builder.OpenComponent<InputTextArea>(0);
                }
                else
                {
                    builder.OpenComponent<InputText>(0);
                }
            }
            else if (itemType == typeof(float))
            {
                builder.OpenComponent<InputNumber<float>>(0);
            }
            else if (itemType == typeof(double))
            {
                builder.OpenComponent<InputNumber<double>>(0);
            }
            else if (itemType == typeof(decimal))
            {
                builder.OpenComponent<InputNumber<decimal>>(0);
            }
            else if (itemType == typeof(int))
            {
                builder.OpenComponent<InputNumber<int>>(0);
            }
            else if (itemType == typeof(short))
            {
                builder.OpenComponent<InputNumber<short>>(0);
            }
            else if (itemType == typeof(byte))
            {
                builder.OpenComponent<InputNumber<byte>>(0);
            }
            else if (itemType == typeof(uint))
            {
                builder.OpenComponent<InputNumber<uint>>(0);
            }
            else if (itemType == typeof(ushort))
            {
                builder.OpenComponent<InputNumber<ushort>>(0);
            }
            else if (itemType == typeof(ulong))
            {
                builder.OpenComponent<InputNumber<ulong>>(0);
            }
            else if (itemType == typeof(DateTime))
            {
                builder.OpenComponent<InputDate<DateTime>>(0);
            }
            else if (itemType == typeof(bool))
            {
                builder.OpenComponent<InputCheckbox>(0);
            }
            else
                throw new NotSupportedException($"Type {itemType} is not supported");
        }

        protected virtual void RenderInput(RenderTreeBuilder builder)
        {
            OpenComponent(builder);

            builder.AddMultipleAttributes(1, AdditionalAttributes);
            if (AdditionalAttributes == null || !AdditionalAttributes.ContainsKey("class"))
            {
                builder.AddAttribute(2, "class", "form-control");
            }
            //builder.AddAttribute(3, "placeholder", Label);
            //if (typeof(TItem) == typeof(LongString))
            //{
            //    var valueExpression = Expression.Lambda<Func<string>>(Expression.MakeMemberAccess(Expression.Constant(model), member));

            //    builder.AddAttribute(4, nameof(InputBase<TItem>.Value), Value.ToString());
            //    builder.AddAttribute(5, nameof(InputBase<TItem>.ValueChanged), EventCallback.Factory.Create(this, (string e) => ValueChanged.InvokeAsync((TItem)(object)new LongString(e))));
            //    builder.AddAttribute(6, nameof(InputBase<TItem>.ValueExpression), ValueExpression.Replace);
            //}
            //else
            //{

            //Type itemType = Nullable.GetUnderlyingType(typeof(TItem));
            //if (itemType != null)
            //{
                
            //}

            builder.AddAttribute(4, nameof(Value), Value);
            builder.AddAttribute(5, nameof(ValueChanged), ValueChanged);
            builder.AddAttribute(6, nameof(ValueExpression), ValueExpression);
            //}
            builder.AddAttribute(7, "disabled", Disabled);
            builder.CloseComponent();
        }

    }

}
