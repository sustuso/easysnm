﻿using System;

namespace SustApp.EasySNM.UI.Forms
{
    public class BadgeIdParams
    {
        public string BadgeId { get; set; }
        public string SiteId { get; set; }
        public Guid ContestId { get; set; }
    }
}
