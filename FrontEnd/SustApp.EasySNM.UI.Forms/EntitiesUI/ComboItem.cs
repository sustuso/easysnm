﻿namespace SustApp.EasySNM.UI.Forms
{
    public class ComboItem<Tkey> //where T : struct
    {
        public Tkey ID { get; set; }
        public string Value { get; set; }
    }
}
