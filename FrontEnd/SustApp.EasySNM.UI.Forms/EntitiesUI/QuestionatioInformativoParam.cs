﻿using System;

namespace SustApp.EasySNM.UI.Forms
{
    /// <summary>
    /// Used ONLY for send data from PsychometricsTestsConsoleOverview to PsychometricsTestsCandidateWizard pop-up
    /// </summary>
    public class QuestionatioInformativoParam
    {
        public string BadgeId { get; set; }
        public Guid ContestId { get; set; }
        public string SiteId { get; set; }
        public bool Wartegg { get; set; }
        //public int RoomNumber { get; set; }
    }
}