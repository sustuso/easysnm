﻿using Microsoft.AspNetCore.Components.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Forms
{
    public class Input<T> : InputBase<T>
    {
        protected override void BuildRenderTree(RenderTreeBuilder builder)
        {
            Input(builder);
        }
    }
}
