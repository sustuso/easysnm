﻿using System;

namespace SustApp.EasySNM.UI.Forms
{
    public class SpinnerContext
    {
        /// <summary>
        /// "StateHasChanged" method of "Spinner.razor"
        /// </summary>
        private readonly Action _callback;

        /// <summary>
        /// Notify the subscriber of the change of Spinner behavior
        /// </summary>
        public event EventHandler IsActiveChanged;

        public SpinnerContext() : this(null)
        {
        }

        public SpinnerContext(Action callback)
        {
            //The callback is "StateHasChanged" method of "Spinner.razor" file:
            _callback = callback;
        }

        private int _activeCounter;

        public bool IsActive { get; private set; }

        public void IncrementActive()
        {
            _activeCounter++;
            if (!IsActive)
            {
                IsActive = true;
                _callback?.Invoke();
                RaiseIsActiveChanged();
            }
        }

        public void DecrementActive()
        {
            _activeCounter--;
            if (IsActive && _activeCounter <= 0)
            {
                IsActive = false;
                _activeCounter = 0;
                _callback?.Invoke();
                RaiseIsActiveChanged();
            }
        }

        private void RaiseIsActiveChanged()
        {
            IsActiveChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
