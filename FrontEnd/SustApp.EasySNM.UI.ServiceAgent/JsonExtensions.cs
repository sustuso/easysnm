﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.DomainModel.Json;
using Newtonsoft.Json;

namespace Newtonsoft.Json
{
    public static class JsonExtensions
    {
        public static JsonSerializerSettings SetCommonSettings(this JsonSerializerSettings serializerSettings)
        {
            serializerSettings.TypeNameHandling = TypeNameHandling.Objects;
            serializerSettings.SerializationBinder = new SimpleSerializationBinder(typeof(IEntity), typeof(GetTempResponse));
            serializerSettings.ContractResolver = new DeepContractResolver();

            return serializerSettings;
        }
    }
}
