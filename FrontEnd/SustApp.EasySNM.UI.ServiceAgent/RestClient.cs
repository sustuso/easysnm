﻿using SustApp.EasySNM.UI.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.ServiceAgent
{
    public class RestClient
    {
        private readonly HttpClient _client;

        private static readonly JsonSerializerSettings Settings = new JsonSerializerSettings().SetCommonSettings();

        public RestClient(HttpClient client)
        {
            _client = client;
        }

        public Uri BaseAddress
        {
            get => _client.BaseAddress;
            set => _client.BaseAddress = value;
        }

        #region CRUD Operations        

        /// <summary>
        /// Get a IReadOnlyList of entity domain class quering by razor component
        /// Es. await XXAgent.GetAsync(uri, q => q.Take(5).Select(r => new { r.Id }), CancellationToken.None);
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="uri"></param>
        /// <param name="request"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns>IReadOnlyList</returns>
        public Task<TList> GetAsync<T, TList, TResult>(Uri uri, GetRequest<T, TResult> request, CancellationToken token = default)
        {
            //async/await ????????

            //Retry the correct query string filter:
            string queryString = UriFactory.GetRelativeUriForQuery(request.QueryFactory, request.ExpandExpressions);

            // Count required
            if (request.WithCount)
            {
                queryString += "&$count=true";
            }
            

            return GetAsync<TList>(new Uri(uri + queryString, UriKind.Relative), token);

        }

        /// <summary>
        /// Execute a GET call to REST WS and return a T cast type
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(Uri uri, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().GetAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<T>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a GET call to REST WS and return a string value
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> GetAsync(Uri uri, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().GetAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return bodyResponse;
            }
        }

        /// <summary>
        /// Execute a Post call to REST WS (Add)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public Task<T> PostAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            return PostAsync<T, T>(uri, entity, token);
        }

        /// <summary>
        /// Execute a Post call to REST WS (Add)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task<TResult> PostAsync<T, TResult>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PostAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<TResult>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Add a list of Entities
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entities"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public Task<T> PostRangeAsync<T>(Uri uri, List<T> entities, CancellationToken token = default)
        {
            return PostRangeAsync<T, T>(uri, entities, token);
        }

        /// <summary>
        /// Add a list of Entities
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entities"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TResult> PostRangeAsync<T, TResult>(Uri uri, List<T> entities, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PostAsync(uri, new JsonContent(entities, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<TResult>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Put call to REST WS (Update)
        /// </summary>
        /// <typeparam name="T">Cast parameter object</typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public Task<T> PutAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            return PutAsync<T, T>(uri, entity, token);
        }

        /// <summary>
        /// Execute a Put call to REST WS (Update)
        /// </summary>
        /// <typeparam name="T">Cast parameter object</typeparam>
        /// <typeparam name="TResult">Cast return Objct</typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TResult> PutAsync<T, TResult>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PutAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<TResult>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Put call to REST WS (Update) and return a STRING
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> PutStringAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PutAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return bodyResponse;
            }
        }

        public async Task<T> PutRangeAsync<T>(Uri uri, List<T> entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PutAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<T>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Delete call to REST WS
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public async Task DeleteAsync<T>(Uri uri, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().DeleteAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();
            }
        }

        /// <summary>
        /// Execute a Delete call to REST WS by Entity as parameter
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity">Entity to delete</param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public Task<T> DeleteAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            return DeleteAsync<T, T>(uri, entity, token);

            //var request = new HttpRequestMessage(HttpMethod.Delete, uri) { Content = new JsonContent(entity, Settings) };

            //using (var response = await _client.PrepareEIClient().SendAsync(request, token))
            //{
            //    response.EnsureSuccessStatusCode();
            //    var bodyResponse = await response.Content.ReadAsStringAsync();
            //    token.ThrowIfCancellationRequested();
            //}
        }

        /// <summary>
        /// Execute a Delete call to REST WS by Entity as parameter and TResult
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<TResult> DeleteAsync<T, TResult>(Uri uri, T entity, CancellationToken token = default)
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, uri) { Content = new JsonContent(entity, Settings) };

            using (var response = await _client.PrepareEIClient().SendAsync(request, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return JsonConvert.DeserializeObject<TResult>(bodyResponse, Settings);
            }
        }

        /// <summary>
        /// Execute a Delete call to REST WS and return a string message from WS
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<string> DeleteStringAsync(Uri uri, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().DeleteAsync(uri, token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                return bodyResponse;
            }
        }

        #endregion

        /// <summary>
        /// POST call and return an open Stream
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<Stream> PostStreamAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PostAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();

                var bodyResponse = await response.Content.ReadAsStreamAsync();
                token.ThrowIfCancellationRequested();

                //Se non copio lo Stream, al chiamante arriva chiuso.....PERCHE'???                                
                Stream responseStream = Util.CopyStream(bodyResponse);
                return responseStream;
            }
        }

        /// <summary>
        /// POST call and return a String
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns>String</returns>
        public async Task<string> PostStringAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PostAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();
                var bodyResponse = await response.Content.ReadAsStringAsync();
                token.ThrowIfCancellationRequested();

                response.EnsureSuccessStatusCode();

                return bodyResponse;
            }
        }

        /// <summary>
        /// POST call and return an open Byte[]
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="uri"></param>
        /// <param name="entity"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<byte[]> PostByteAsync<T>(Uri uri, T entity, CancellationToken token = default)
        {
            using (var response = await _client.PrepareEIClient().PostAsync(uri, new JsonContent(entity, Settings), token))
            {
                response.EnsureSuccessStatusCode();

                var bodyResponse = await response.Content.ReadAsByteArrayAsync();
                token.ThrowIfCancellationRequested();

                return bodyResponse;
            }
        }
    }

    public class JsonContent : StringContent
    {
        public JsonContent(object obj, JsonSerializerSettings settings) :
            base(JsonConvert.SerializeObject(obj, settings), System.Text.Encoding.UTF8, "application/json")
        { }
    }
}