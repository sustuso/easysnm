﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace System.Linq
{
    public static class LinqExtensions
    {
        internal static readonly MethodInfo WhereODataMethod = new Func<IQueryable<object>, string, IQueryable<object>>(WhereOData<object>).Method.GetGenericMethodDefinition();

        public static IQueryable<T> WhereOData<T>(this IQueryable<T> source, string filter)
        {
            var callExpression = Expression.Call(WhereODataMethod.MakeGenericMethod(typeof(T)), source.Expression, Expression.Constant(filter));

            return source.Provider.CreateQuery<T>(callExpression);
        }
    }
}
