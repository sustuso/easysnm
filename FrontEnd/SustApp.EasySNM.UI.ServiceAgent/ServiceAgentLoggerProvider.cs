﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.UI.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SustApp.EasySNM.UI.ServiceAgent
{
    public class ServiceAgentLoggerProvider : ILoggerProvider
    {
        private readonly BatchBlock<Event> _buffer;
        private readonly ActionBlock<Event[]> _action;
        private Timer _timer;

        public ServiceAgentLoggerProvider()
        {
            _buffer = new BatchBlock<Event>(20);
            _action = new ActionBlock<Event[]>(OnFlush, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 1 });
            _buffer.LinkTo(_action);

            _timer = new Timer(s => _buffer.TriggerBatch(), null, 10000, 10000);
        }

        private async Task OnFlush(Event[] events)
        {
            if (Services == null) return;

            try
            {
                var requests = events.Select(e => e.GetRequest()).ToArray();

                var logServiceAgent = Services.GetRequiredService<ILogServiceAgent>();
                await logServiceAgent.AddLogsAsync(requests);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        public void Dispose()
        {
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new ServiceAgentLogger(this, categoryName);
        }

        public IServiceProvider Services { get; set; }

        private class ServiceAgentLogger : ILogger, IDisposable
        {
            private readonly ServiceAgentLoggerProvider _parent;
            private readonly string _categoryName;

            public ServiceAgentLogger(ServiceAgentLoggerProvider parent, string categoryName)
            {
                _parent = parent;
                _categoryName = categoryName;
            }

            public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
            {
                string message = null;
                object[] stateValues = null;
                if (state is IEnumerable<KeyValuePair<string, object>> values)
                {
                    message = values.FirstOrDefault(v => v.Key == "{OriginalFormat}").Value?.ToString();
                    stateValues = values.Select(v => v.Value).ToArray();
                }    
                message ??= formatter(state, exception);
                _parent._buffer.Post(new Event(_categoryName, logLevel, eventId, message, stateValues, exception));
            }

            public bool IsEnabled(LogLevel logLevel)
            {
                return logLevel >= LogLevel.Warning;
            }

            public IDisposable BeginScope<TState>(TState state)
            {
                return this;
            }

            public void Dispose()
            {
            }
        }

        private class Event
        {
            public string Category { get; }

            public LogLevel LogLevel { get; }

            public EventId EventId { get; }

            public string Message { get; }

            public object[] StateValues { get; }

            public Exception Exception { get; }

            public Event(string category, LogLevel logLevel, in EventId eventId, string message, object[] stateValuesValues, Exception exception)
            {
                Category = category;
                LogLevel = logLevel;
                EventId = eventId;
                Message = message;
                StateValues = stateValuesValues;
                Exception = exception;
            }

            public LogRequest GetRequest()
            {
                return new LogRequest
                {
                    LogLevel = LogLevel,
                    EventId = EventId.Id,
                    EventName = EventId.Name,
                    Category = Category,
                    Message = Message,
                    StateValues = StateValues,
                    Exception = Exception?.ToString(),
                };
            }
        }
    }
}
