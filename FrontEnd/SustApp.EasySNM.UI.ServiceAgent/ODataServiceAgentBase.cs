﻿using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.UI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Simple.OData.Client;

namespace SustApp.EasySNM.UI.ServiceAgent
{
    public class ODataServiceAgentBase<T, TKey> : IServiceAgent<T, TKey>
        where T : class, IEntity<TKey>
    {
        private readonly ODataClient<T, TKey> _client;

        public Uri BaseAdress => throw new NotImplementedException();

        public ODataServiceAgentBase(ODataClient<T, TKey> client)
        {
            _client = client;
        }


        #region CRUD Operations
        /// <summary>
        /// Add T entity domain class
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task<T> AddAsync(T entity, CancellationToken token)
        {
            return _client.InsertAsync(entity, token);
        }

        public Task<T> AddRangeAsync(List<T> entities, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Update T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task<T> UpdateAsync(T entity, CancellationToken token)
        {
            return _client.UpdateAsync(entity, token);
        }

        public Task<T> UpdateRangeAsync(List<T> entities, CancellationToken token)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Delete T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task DeleteAsync(T entity, CancellationToken token)
        {
            return _client.DeleteAsync(entity, token);
        }

        /// <summary>
        /// Get a single entity domain class by PK
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task<T> GetAsync(TKey id, CancellationToken token)
        {
            return _client.Entities.Key(id).FindEntryAsync(token);
        }

        /// <summary>
        /// Get a IReadOnlyList of entity domain class quering by razor component
        /// Es. await XXAgent.GetAsync(q => q.Take(5).Select(r => new { r.Id }), CancellationToken.None);
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <param name="request"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual async Task<CountResult<TResult>> GetAsync<TResult>(GetRequest<T, TResult> request, CancellationToken token)
            where TResult : class
        {
            // Get query option from expression tree
            string queryString = UriFactory.GetRelativeUriForQuery(request.QueryFactory, request.ExpandExpressions);
            if (queryString.Length > 0)
            {
                // Remove quotation mark?
                queryString = queryString.Substring(1);
            }

            var annotations = new ODataFeedAnnotations();

            // Result and orginal type are the same
            if (typeof(TResult) == typeof(T))
            {
                IBoundClient<T> boundClient = _client.Entities.QueryOptions(queryString);
                if (request.WithCount)
                {
                    boundClient = boundClient.Count();
                }
                IEnumerable<T> result = await boundClient.FindEntriesAsync(annotations, token);
                return new CountResult<TResult>(result.Cast<TResult>().ToArray(), (int)annotations.Count.GetValueOrDefault(0));
            }

            // Use untyped client
            IBoundClient<IDictionary<string, object>> unboundClient = _client.Client.For(typeof(T).Name).QueryOptions(queryString);
            if (request.WithCount)
            {
                unboundClient = unboundClient.Count();
            }
            IEnumerable<IDictionary<string, object>> partialResult = await unboundClient.FindEntriesAsync(annotations, token);

            if (typeof(TResult).GetCustomAttribute<CompilerGeneratedAttribute>() != null)
            {
                // Get the first ctor
                var constructorInfos = typeof(TResult).GetConstructors();
                var constructorInfo = constructorInfos[0];
                // Prepare list of parameters
                var parameters = constructorInfo.GetParameters().ToArray();

                var result = new List<TResult>();
                foreach (IDictionary<string, object> item in partialResult)
                {
                    // Prepare value for each parameter
                    var parameterValues = parameters.Select(p =>
                    {
                        if (item.TryGetValue(p.Name, out object v))
                            return v;
                        // Create default value or null for ref types
                        return p.ParameterType.IsValueType ? Activator.CreateInstance(p.ParameterType) : null;
                    }).ToArray();

                    // Create object
                    TResult obj = (TResult) constructorInfo.Invoke(parameterValues);
                    result.Add(obj);
                }

                return new CountResult<TResult>(result, (int)annotations.Count.GetValueOrDefault(0));
            }

            throw new NotSupportedException("Only anonymous types are supported");
        }

        #endregion
    }
}