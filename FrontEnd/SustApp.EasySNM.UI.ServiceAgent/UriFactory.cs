﻿using Microsoft.OData.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;

namespace SustApp.EasySNM.UI.ServiceAgent
{
    /// <summary>
    /// As Is, get a correct URI for call WS end point (to add at BaseUri)
    /// </summary>
    public static class UriFactory
    {
        public static string GetRelativeUriForQuery<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFactory)
        {
            return GetRelativeUriForQuery<TEntity, TResult>(queryFactory, null);
        }

        public static string GetRelativeUriForQuery<TEntity, TResult>(Func<IQueryable<TEntity>, IQueryable<TResult>> queryFactory, IEnumerable<Expression<Func<TEntity, object>>> expandExpressions)
        {
            return GetRelativeUriForQuery<TEntity, TResult>("", queryFactory, expandExpressions);
        }

        public static string GetRelativeUriForQuery<TEntity, TResult>(string path,
            Func<IQueryable<TEntity>, IQueryable<TResult>> queryFactory,
            IEnumerable<Expression<Func<TEntity, object>>> expandExpressions)
        {
            var context = new DataServiceContext(new Uri("http://localhost"), ODataProtocolVersion.V4);
            var query = context.CreateQuery<TEntity>("_");

            if (expandExpressions != null)
            {
                foreach (Expression<Func<TEntity, object>> expandExpression in expandExpressions)
                {
                    query = query.Expand(GetExpand(expandExpression));
                }
            }

            var resultQuery = ApplyFix(queryFactory(query), out var fix) as DataServiceQuery<TResult>;
            if (resultQuery == null) throw new InvalidOperationException("queryFactory has returned invalid query");

            // Estrapolo la lista dei campi specifici da estrarre
            string select = String.Empty;
            if (typeof(TResult) != typeof(TEntity))
            {
                select = String.Join(",", resultQuery.ElementType.GetRuntimeProperties().Select(p => p.Name));
            }
            var uri = new UriBuilder(fix(resultQuery.RequestUri.ToString()));

            // Workaround: il ? va rimosso altrimenti viene messo doppio
            string q = uri.Query.Replace("?", "");

            if (select.Length > 0)
            {
                if (q.Length > 0) q += "&";
                q += "$select=" + WebUtility.UrlEncode(select);
            }
            // Applico una fix per i campi di tipo data
            uri.Query = new EnumFix().ReplaceValue(resultQuery.Expression, q);
            uri.Path = path;

            return uri.Uri.PathAndQuery.Substring(1);
        }

        private static string GetExpand<TEntity>(Expression<Func<TEntity, object>> expandExpression)
        {
            var result = new StringBuilder();
            int n = 0;
            foreach (Expression expression in expandExpression.Body.Visit())
            {
                if (expression is MemberExpression me)
                {
                    if (result.Length > 0)
                    {
                        result.Append("($expand=");
                        n++;
                    }
                    result.Append(me.Member.Name);
                }
            }

            // Close expression
            for (int x = 0; x < n; x++)
            {
                result.Append(")");
            }

            return result.ToString();
        }

        private static IQueryable<T> ApplyFix<T>(IQueryable<T> query, out Func<string, string> fix)
        {
            var fixToApply = new Dictionary<string, string>();
            var customFilters = new List<string>();

            Expression replace = query.Expression.Replace(
                e => (e is MethodCallExpression me && me.Object != null &&
                      typeof(IDictionary<string, object>).IsAssignableFrom(me.Object.Type))
                     ||
                     (e is MethodCallExpression me2 && me2.Method.IsGenericMethod && me2.Method.GetGenericMethodDefinition() == LinqExtensions.WhereODataMethod),
                e =>
                {
                    MethodCallExpression methodCallExpression = (MethodCallExpression)e;
                    // WhereOData implementation
                    if (methodCallExpression.Method.IsGenericMethod && methodCallExpression.Method.GetGenericMethodDefinition() == LinqExtensions.WhereODataMethod)
                    {
                        // Keep custom filter into a list
                        customFilters.Add(((ConstantExpression)methodCallExpression.Arguments[1]).Value.ToString());

                        // Remove original expression
                        return methodCallExpression.Arguments[0];
                    }

                    MemberExpression dictionaryPropertyExpression = (MemberExpression)methodCallExpression.Object;

                    string key = methodCallExpression.Arguments.GetConstantValue<string>();
                    string name = $"{dictionaryPropertyExpression.Member.Name}_{key}";

                    PropertyInfo stringProperty = dictionaryPropertyExpression.Member.DeclaringType.GetRuntimeProperties().FirstOrDefault(p => p.PropertyType == typeof(string));
                    if (stringProperty == null) throw new InvalidOperationException("Cannot find a property of type string which is required in order to support Dictionaries");

                    MemberExpression stringPropertyExpression = Expression.MakeMemberAccess(dictionaryPropertyExpression.Expression, stringProperty);
                    MethodInfo method = typeof(string).GetMethod("Concat", BindingFlags.Static | BindingFlags.Public, null, new[] { typeof(string), typeof(string) }, null);

                    fixToApply.Add($"concat({stringProperty.Name},'{name}')", name);

                    return Expression.Call(method, stringPropertyExpression, Expression.Constant(name));
                }, false);

            fix = s =>
            {
                foreach (var pair in fixToApply)
                {
                    s = s.Replace(pair.Key, pair.Value);
                }

                // Apply custom filters
                if (customFilters.Count > 0)
                {
                    var uriBuilder = new UriBuilder(s);
                    // Parse query string key/value
                    var queryString = HttpUtility.ParseQueryString(uriBuilder.Query);
                    const string filterKey = "$filter";

                    if (!String.IsNullOrWhiteSpace(queryString[filterKey]))
                    {
                        queryString[filterKey] = $"({queryString[filterKey]})";
                    }

                    foreach (string customFilter in customFilters)
                    {
                        string filter = queryString[filterKey];
                        // If filter already exists apply 'and'
                        if (!String.IsNullOrWhiteSpace(filter))
                        {
                            filter += " and ";
                        }

                        filter += customFilter;
                        queryString[filterKey] = filter;
                    }

                    // Rebuild query string
                    uriBuilder.Query = queryString.ToString();
                    s = uriBuilder.ToString();
                }

                return s;
            };


            return query.Provider.CreateQuery<T>(replace);
        }

        private class EnumFix : System.Linq.Expressions.ExpressionVisitor
        {
            private HashSet<DateTimeOffset> _dates = new HashSet<DateTimeOffset>();
            private HashSet<Type> _enums = new HashSet<Type>();

            internal string ReplaceValue(Expression expression, string query)
            {
                Visit(expression);

                // Sostituisco ogni data trovata
                foreach (DateTimeOffset d in _dates)
                {
                    string formattedDate = System.Xml.XmlConvert.ToString(d);
                    string correctedValue = formattedDate;
                    query = query.Replace(WebUtility.UrlEncode(formattedDate), WebUtility.UrlEncode(correctedValue));
                }

                foreach (Type e in _enums)
                {
                    query = query.Replace(e.FullName, "");
                }

                return query;
            }

            private bool ShouldVisit(Expression node)
            {
                return node != null && node.NodeType != (ExpressionType)10000;
            }

            public override Expression Visit(Expression node)
            {
                if (!ShouldVisit(node)) return node;
                return base.Visit(node);
            }

            protected override Expression VisitUnary(UnaryExpression node)
            {
                if (node.Operand.Type.IsEnum)
                {
                    _enums.Add(node.Operand.Type);
                }
                return base.VisitUnary(node);
            }

            protected override Expression VisitMethodCall(MethodCallExpression node)
            {
                Expression obj = this.Visit(node.Object);

                IEnumerable<Expression> args = node.Arguments
                    // Devo escludere la navigazione al Queryable di partenza sennò da errore
                    .Where(ShouldVisit)
                    .Select(a => Visit(a))
                    .ToArray();

                return node;
            }

            protected override Expression VisitConstant(ConstantExpression node)
            {
                AddValue(node.Value);

                Type nodeType = node.Value?.GetType() ?? typeof(object);
                // Se è un tipo complesso (per esempio se è variabile di stack)
                // ci navigo all'interno per cercare tutte le date
                if (!nodeType.IsPrimitive && nodeType != typeof(String))
                {
                    // Guardo nelle proprietà
                    foreach (var prop in nodeType.GetRuntimeProperties())
                    {
                        AddValue(prop.GetValue(node.Value));
                    }
                    // Guardo nei campi
                    foreach (var field in nodeType.GetRuntimeFields())
                    {
                        AddValue(field.GetValue(node.Value));
                    }
                }
                return base.VisitConstant(node);
            }

            private void AddValue(object value)
            {
                // Aggiungo la data alla lista se è un valore compatibile
                if (value != null && value is DateTimeOffset d)
                {
                    if (!_dates.Contains(d))
                        _dates.Add(d);
                }
            }
        }
    }
}
