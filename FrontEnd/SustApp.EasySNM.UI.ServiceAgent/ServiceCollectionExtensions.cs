﻿using Microsoft.Extensions.DependencyInjection;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.UI.ServiceAgent;
using SustApp.EasySNM.UI.Services;
using System;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceAgent(this IServiceCollection services)
        {
            //Constant operations:
            services.AddTransient<RestClient>();
            services.AddTransient(typeof(ODataClient<,>));

            //Variable operations:
            //services.AddRestServiceAgent<Site, string>();
            //services.AddRestServiceAgent<Exam, Guid>();
            //services.AddRestServiceAgent<FormField, Guid>();
            //services.AddRestServiceAgent<MeasureUnit, string>();
            //services.AddRestServiceAgent<Measurement, string>();
            //services.AddRestServiceAgent<MedicalSpecialization, string>();
            //services.AddRestServiceAgent<FormFieldResult, Guid>();
            //services.AddRestServiceAgent<ExamResult, Guid>();
            //services.AddRestServiceAgent<MilitaryRank, string>();
            //services.AddRestServiceAgent<ContestStatus, string>();
            //services.AddRestServiceAgent<DocumentType, Guid>();
            //services.AddRestServiceAgent<Document, Guid>();
            //services.AddRestServiceAgent<Candidate, Guid>();
            //services.AddRestServiceAgent<Contest, Guid>();
            //services.AddRestServiceAgent<CandidateStatus, string>();
            //services.AddRestServiceAgent<ContestTemplate, Guid>();
            //services.AddRestServiceAgent<PhysicalTestRule, Guid>();
            //services.AddRestServiceAgent<PsychometricsTestClassroomInfo, Guid>();
            //services.AddRestServiceAgent<PcStation, Guid>();
            //services.AddRestServiceAgent<PsychometricsTestResult, Guid>();
            //services.AddRestServiceAgent<UserRole, Guid>();
            //services.AddRestServiceAgent<PostponementMedicalTest, Guid>();
            //services.AddRestServiceAgent<CandidatePostponementMedicalTest, Guid>();
            //services.AddRestServiceAgent<GeneralMedicine, Guid>();
            //services.AddRestServiceAgent<Role, string>();
            //services.AddRestServiceAgent<PsychologicalEvaluation, Guid>();
            //services.AddRestServiceAgent<PhysicalTestType, Guid>();

            //services.AddTransient<IContestServiceAgent>(p => p.GetRequiredService<Contest, Guid>());
            //services.AddTransient<IFormFieldResultServiceAgent, FormFieldResultServiceAgent>();
            //services.AddTransient<IContestServiceAgent, ContestServiceAgent>();
            //services.AddTransient<ITestClassroomServiceAgent, TestClassroomServiceAgent>();
            //services.AddTransient<ISiteServiceAgent, SitServiceAgent>();
            //services.AddTransient<IMunicipalityServiceAgent, MunicipalityServiceAgent>();
            //services.AddTransient<IProvinceServiceAgent, ProvinceServiceAgent>();
            //services.AddTransient<IRegionServiceAgent, RegionServiceAgent>();
            //services.AddTransient<IExamServiceAgent, ExamServiceAgent>();
            //services.AddTransient<ICandidateServiceAgent, CandidateServiceAgent>();
            //services.AddTransient<IPsychometricsTestClassroomInfoServiceAgent, PsychometricsTestClassroomInfoServiceAgent>();
            //services.AddTransient<IPsychologicalEvaluationRoomServiceAgent, PsychologicalEvaluationRoomServiceAgent>();
            //services.AddTransient<IUserRoleServiAgent, UserRoleServiAgent>();
            //services.AddTransient<IUserServiceAgent, UserServiceAgent>();
            //services.AddTransient<IPhysicalTestTypeServiceAgent, PhysicalTestTypeServiceAgent>();
            //services.AddTransient<ICommissionServiceAgent, CommissionServiceAgent>();
            //
            //services.AddRestServiceAgent<Calendar, Guid>();
            //services.AddRestServiceAgent<Region, string>();
            //services.AddRestServiceAgent<Province, string>();
            //services.AddRestServiceAgent<Municipality, int>();
            //services.AddRestServiceAgent<MultiPostalCode, int>();
            //services.AddRestServiceAgent<MetropolitanCity, int>();
            //services.AddRestServiceAgent<ContestProvinceSite, Guid>();
            //services.AddRestServiceAgent<Commission, Guid>();
            //services.AddRestServiceAgent<CommissionMemberRole, string>();
            //services.AddRestServiceAgent<CommissionType, Guid>();
            //services.AddRestServiceAgent<PostponementMedicalTest, Guid>();
            //services.AddRestServiceAgent<MedicalCondition, Guid>();
            //services.AddRestServiceAgent<CandidateAssessments, Guid>();
            //
            //services.AddTransient<IBiometricSignatureServiceAgent, BiometricSignatureServiceAgent>();
            //services.AddTransient<IFilesServiceAgent, FilesServiceAgent>();
            //services.AddTransient<ICalendarServiceAgent, CalendarServiceAgent>();
            //services.AddTransient<IKioskQueueServiceAgent, KioskQueueServiceAgent>();
            //services.AddTransient<IPhysicalTestServiceAgent, PhysicalTestServiceAgent>();
            //services.AddTransient<IMedicalConditionServiceAgent, MedicalConditionServiceAgent>();
            //services.AddTransient<ILogServiceAgent, LogServiceAgent>();
            //services.AddTransient<IGeneralServiceAgent, GeneralServiceAgent>();


            return services;
        }

        private static IServiceCollection AddRestServiceAgent<TEntity, TKey>(this IServiceCollection services)
            where TEntity : class, IEntity<TKey>
        {
            return services.AddTransient<IServiceAgent<TEntity, TKey>, RestServiceAgentBase<TEntity, TKey>>();
        }
    }
}

namespace System.Net.Http
{
    public static class ClientExtensions
    {
        /// <summary>
        /// Set a BaseAddress property (e.g. https://localhost:44334/)
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public static HttpClient PrepareEIClient(this HttpClient client)
        {
//#if DEBUG
//            var baseUri = new Uri("https://localhost:44334/");
//            if (client.BaseAddress != baseUri)
//            {
//                client.BaseAddress = baseUri;
//            }
//#endif

            return client;
        }


    }
}
