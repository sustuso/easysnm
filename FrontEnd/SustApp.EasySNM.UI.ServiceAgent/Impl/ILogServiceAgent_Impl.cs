﻿using SustApp.EasySNM.ApiModel;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.Services
{
    public interface ILogServiceAgent
    {
        Task AddLogsAsync(LogRequest[] requests, CancellationToken token = default);
    }
}
