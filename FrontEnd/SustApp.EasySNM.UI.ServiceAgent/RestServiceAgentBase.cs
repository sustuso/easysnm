﻿using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.UI.Services;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace SustApp.EasySNM.UI.ServiceAgent
{
    public class RestServiceAgentBase<T, TKey> : IServiceAgent<T, TKey>
        where T : class
    {
        protected RestClient Client { get; }

        public Uri BaseUri { get; }

        private Uri _baseAdress;
        public Uri BaseAdress
        {
            get 
            {
                _baseAdress = Client?.BaseAddress;
                return _baseAdress; 
            }
        }


        public RestServiceAgentBase(RestClient client) : this(client, new Uri($"/api/{typeof(T).Name}", UriKind.Relative))
        {
        }

        public RestServiceAgentBase(RestClient client, Uri baseUri)
        {
            Client = client;
            BaseUri = baseUri;
        }

        #region CRUD Operations

        /// <summary>
        /// Add T entity domain class
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual async Task<T> AddAsync(T entity, CancellationToken token)
        {
            try
            {
                return await Client.PostAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                throw new SustAppEntityException(entity, "Error while adding entity", we);
            }
        }

        /// <summary>
        /// Add a list of entities
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public virtual async Task<T> AddRangeAsync(List<T> entities, CancellationToken token)
        {
            try
            {
                var newUri = new Uri($"/api/{typeof(T).Name}/PostRangeAsync", UriKind.Relative);
                return await Client.PostRangeAsync(newUri, entities, token);
            }
            catch (WebException we)
            {
                throw new SustAppEntityException(entities, "Error while adding entities", we);
            }
        }

        /// <summary>
        /// Update T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual async Task<T> UpdateAsync(T entity, CancellationToken token)
        {
            try
            {
                return await Client.PutAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(entity, "Error while updating entity", we);
            }
        }

        public async Task<T> UpdateRangeAsync(List<T> entities, CancellationToken token)
        {
            try
            {
                var newUri = new Uri($"/api/{typeof(T).Name}/PutRangeAsync", UriKind.Relative);
                return await Client.PutRangeAsync(newUri, entities, token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(entities, "Error while updating entities", we);
            }
        }

        /// <summary>
        /// Delete T entity domain class 
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task DeleteAsync(T entity, CancellationToken token)
        {
            try
            {
                return Client.DeleteAsync(BaseUri, entity, token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(entity, "Error while deleting entity", we);
            }
        }

        /// <summary>
        /// Get a single entity domain class by PK 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token">CancellationToken</param>
        /// <returns></returns>
        public virtual Task<T> GetAsync(TKey id, CancellationToken token)
        {
            try
            {
                return Client.GetAsync<T>(new Uri($"{BaseUri}/{id}", UriKind.Relative), token);
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException(id, "Error while Get entity", we);
            }
        }

        /// <summary>
        /// Get a IReadOnlyList of entity domain class quering by razor component
        /// Es. await XXAgent.GetAsync(q => q.Take(5).Select(r => new { r.Id }), CancellationToken.None);
        /// </summary>
        /// <typeparam name="TResult"></typeparam>
        /// <returns></returns>
        public virtual async Task<CountResult<TResult>> GetAsync<TResult>(GetRequest<T, TResult> request, CancellationToken token) where TResult : class
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            try
            {
                if (request.WithCount)
                {
                    return await Client.GetAsync<T, CountResult<TResult>, TResult>(BaseUri, request, token);
                }
                else
                {
                    var items = await Client.GetAsync<T, IReadOnlyList<TResult>, TResult>(BaseUri, request, token);
                    token.ThrowIfCancellationRequested();

                    return new CountResult<TResult>(items, 0);
                }
            }
            catch (WebException we)
            {
                // TODO: versione
                //if (we.Status == (WebExceptionStatus)409)
                throw new SustAppEntityException("?? Cosa passiamo??", "Error while deleting entity", we);
            }
        }

        #endregion
    }
}