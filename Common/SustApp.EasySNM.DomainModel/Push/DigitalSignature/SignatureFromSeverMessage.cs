﻿using System;

namespace SustApp.EasySNM.DomainModel
{
    public class SignatureFromServerMessage : Message
    {
        public Uri PdfUri { get; set; }
    }
}
