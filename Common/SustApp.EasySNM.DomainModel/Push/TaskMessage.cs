﻿using System;

namespace SustApp.EasySNM.DomainModel
{
    public class TaskMessage : Message
    {
        /// <summary>
        /// Starting job operation or not
        /// </summary>
        public bool Starting { get; set; }

        /// <summary>
        /// PK
        /// </summary>
        public Guid EntityId { get; set; }

        /// <summary>
        /// Description in item Task component
        /// </summary>
        public string TaskDescription { get; set; }

        /// <summary>
        /// Successfull operation or not
        /// </summary>
        public bool Successfull { get; set; }

        /// <summary>
        /// Message for tooltip item in Task component
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Where task is fired
        /// </summary>
        //public eTaskType TaskType { get; set; }
    }
}
