﻿namespace SustApp.EasySNM.DomainModel
{
    public class PcStationChangedMessage : Message
    {
        public string PcStationName { get; }
        public string SiteId { get; }
        public int RoomNumber { get; }

        public PcStationChangedMessage(string pcStationName, string siteId, int roomNumber)
        {
            PcStationName = pcStationName;
            SiteId = siteId;
            RoomNumber = roomNumber;
        }
    }
}
