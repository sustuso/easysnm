﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class FileMessage : Message
    {
        public FileMessage(Uri uri)
        {
            Uri = uri;
        }

        public Uri Uri { get; }
    }
}
