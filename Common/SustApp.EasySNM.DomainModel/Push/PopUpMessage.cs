﻿using System.Collections.Generic;

namespace SustApp.EasySNM.DomainModel
{
    public class PopUpMessage : Message
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public List<string> Roles { get; set; } = new List<string>();
        public string UserName { get; set; }
    }
}
