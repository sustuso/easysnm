﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class ToastMessage : Message
    {
        public string Title { get; set; }

        public string Description { get; set; }
    }
}
