﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using SustApp.EasySNM.DomainModel;

namespace SustApp.EasySNM.DomainModel
{
    public class SustAppValidationException : SustAppEntityException
    {

        public IEnumerable<ValidationResult> Results { get; }

        public SustAppValidationException(IEntity entity, IEnumerable<ValidationResult> results) : base(entity, GetMessage(results))
        {
            Results = results ?? throw new ArgumentNullException(nameof(results));
        }

        private static string GetMessage(IEnumerable<ValidationResult> results)
        {
            return $"Validation errors: {String.Join(", ", results.SelectMany(r => r.MemberNames))}";
        }
    }
}
