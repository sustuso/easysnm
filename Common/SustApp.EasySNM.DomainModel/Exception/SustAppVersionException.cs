﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.EasySNM.DomainModel;

namespace SustApp.EasySNM.DomainModel
{
    public class SustAppVersionException : SustAppEntityException
    {
        public SustAppVersionException(IEntity entity) : this(entity, null)
        {
        }

        public SustAppVersionException(Exception innerException) : base("Version specified is not valid", innerException)
        {
        }

        public SustAppVersionException(IEntity entity, Exception innerException) : base (entity, "Version specified is not valid", innerException)
        {
        }
    }
}
