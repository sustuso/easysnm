﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM;

namespace SustApp.EasySNM.DomainModel
{
    public class SustAppEntityException : SustAppException
    {
        public SustAppEntityException()
        {
        }

        protected SustAppEntityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public SustAppEntityException(string message) : base(message)
        {
        }

        public SustAppEntityException(string message, Exception innerException) : base(message)
        {
        }

        public SustAppEntityException(IEntity entity, string message) : base(message)
        {
            Entity = entity ?? throw new ArgumentNullException(nameof(entity));
            EntityId = entity.ObjectId;
        }

        public SustAppEntityException(IEntity entity, string message, Exception innerException) : base(message, innerException)
        {
            Entity = entity ?? throw new ArgumentNullException(nameof(entity));
            EntityId = entity.ObjectId;
        }

        public SustAppEntityException(object entityId, string message) : base(message)
        {
            EntityId = entityId;
        }

        public SustAppEntityException(object entityId, string message, Exception innerException) : base(message, innerException)
        {
            EntityId = entityId;
        }

        public object EntityId { get; }

        public IEntity Entity { get; }
    }
}
