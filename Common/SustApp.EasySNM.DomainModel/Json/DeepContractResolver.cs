using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace SustApp.EasySNM.DomainModel.Json
{
    public class DeepContractResolver : CamelCasePropertyNamesContractResolver
    {
        protected override List<MemberInfo> GetSerializableMembers(Type objectType)
        {
            // Remove any child property of type IEntity
            return base.GetSerializableMembers(objectType).Where(p =>
            {
                if (!GetIsEntityType(objectType, true)) return true;

                return !(p is PropertyInfo pi) || !GetIsEntityType(pi.PropertyType, false);
            }).ToList();
        }

        protected override JsonObjectContract CreateObjectContract(Type objectType)
        {
            var contract = base.CreateObjectContract(objectType);

            // Avoid to serialize type name for all types
            contract.ItemTypeNameHandling = TypeNameHandling.None;

            return contract;
        }

        protected override JsonArrayContract CreateArrayContract(Type objectType)
        {
            var contract = base.CreateArrayContract(objectType);

            // Avoid to serialize type name for all types
            contract.ItemTypeNameHandling = TypeNameHandling.None;

            // Serialize with a $type json field in case of abstract base type
            if (GetIsEntityType(contract.CollectionItemType, true))
            {
                contract.ItemTypeNameHandling = TypeNameHandling.Objects;
            }

            return contract;
        }

        private bool GetIsEntityType(Type type, bool isAbstractToo)
        {
            bool r = typeof(IEntity).IsAssignableFrom(type);
            if (!r || !isAbstractToo) return r;

            do
            {
                if (type.IsAbstract) return true;

                type = type.BaseType;
            } while (type != null && !type.IsGenericType);

            return false;
        }
    }
}