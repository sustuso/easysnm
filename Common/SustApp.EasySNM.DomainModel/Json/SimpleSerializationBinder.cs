﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Serialization;

namespace SustApp.EasySNM.DomainModel.Json
{
    public class SimpleSerializationBinder : ISerializationBinder
    {
        private readonly Type[] _assemblyTypes;

        public SimpleSerializationBinder(params Type[] assemblyTypes)
        {
            _assemblyTypes = assemblyTypes;
        }

        public Type BindToType(string assemblyName, string typeName)
        {
            Type type = _assemblyTypes
                .Select(t => Type.GetType($"{t.Namespace}.{typeName}, {t.Assembly.FullName}", false))
                .FirstOrDefault(t => t != null);
            if (type == null)
            {
                throw new ArgumentException($"Cannot find type for name {typeName}", nameof(typeName));
            }

            return type;
        }

        public void BindToName(Type serializedType, out string assemblyName, out string typeName)
        {
            // TODO: find a way to exclude $type serialization
            //if (serializedType.IsGenericType && serializedType.GetGenericTypeDefinition() == typeof(PageResult<>))
            //{
            //    assemblyName = null;
            //    typeName = null;
            //    return;
            //}

            assemblyName = null;
            typeName = serializedType.Name;
        }
    }
}
