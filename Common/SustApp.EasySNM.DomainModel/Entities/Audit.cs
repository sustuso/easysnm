﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SustApp.EasySNM.DomainModel
{
    public class Audit : EntityBase<Guid>
    {
        public Audit(string user, string message, IEntity entity, string auditActivityId) : this(user, message, entity.GetType().Name, entity.ObjectId.ToString(), auditActivityId, GetAdditionalData(entity))
        {

        }

        private static Dictionary<string, object> GetAdditionalData(IEntity entity)
        {
            var dictionary = new Dictionary<string, object>();
            foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(entity))
            {
                if (property.PropertyType.IsPrimitive || property.PropertyType.IsValueType ||
                    property.PropertyType == typeof(string))
                {
                    object value = property.GetValue(entity);
                    if (value == null) continue;
                    
                    dictionary.Add(property.Name, value);
                }
            }

            return dictionary;
        }

        public Audit(string user, string message, string entityName, string entityId, string auditActivityId) : this(user, message, entityName, entityId, auditActivityId, null)
        {
        }

        public Audit(string user, string message, string entityName, string entityId, string auditActivityId, Dictionary<string, object> additionalData)
        {
            User = user;
            Message = message;
            EntityName = entityName;
            EntityId = entityId;
            AdditionalData = additionalData;
            AuditActivityId = auditActivityId;
            AuditDate = DateTimeOffset.Now;
        }

        public DateTimeOffset AuditDate { get; private set; }

        [Required]
        [StringLength(100)]
        public string User { get; }

        [Required]
        public string Message { get; }

        [Required]
        [StringLength(100)]
        public string EntityName { get; }

        [StringLength(30)]
        public string AuditActivityId { get; }

        public string EntityId { get; }

        public AuditActivity AuditActivity { get; set; }

        public Dictionary<string, object> AdditionalData { get; }
    }
}
