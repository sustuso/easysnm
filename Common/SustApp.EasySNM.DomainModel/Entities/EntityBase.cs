﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public abstract class EntityBase<TKey> : IEntity<TKey>
    {
        object IEntity.ObjectId
        {
            get => Id;
            set => Id = (TKey) value;
        }

        public virtual TKey Id { get; set; }
    }

    public abstract class VersionedEntityBase<TKey> : EntityBase<TKey>, IVersionedEntity<TKey>
    {
        public byte[] Version { get; set; }
    }
}
