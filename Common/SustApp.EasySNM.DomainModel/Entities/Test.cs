﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class Test : VersionedEntityBase<Guid>
    {
        [StringLength(10)]
        public string? String10 { get; set; }
        public int? Number { get; set; }
        public bool? Boolean { get; set; }
        public DateTime? DateTime { get; set; }
        
        //Navigation Property
        public List<TestNP> TestNPs { get; set; } = new List<TestNP>();

    }
}
