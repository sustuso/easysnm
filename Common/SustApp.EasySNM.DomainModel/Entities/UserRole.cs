﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SustApp.EasySNM.DomainModel
{
    public class UserRole : EntityBase<Guid>
    {
        [Required]
        [StringLength(50)]
        public string Username { get; set; }
        [Required]
        [StringLength(50)]
        public string RoleId { get; set; }
        [StringLength(200)]
        public string FirstName { get; set; }
        [StringLength(200)]
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public bool IsActive { get; set; }
        public Int16 CardType { get; set; } //0 = CMD, 1 = SmartCard

        public Role Role { get; set; }
    }
}
