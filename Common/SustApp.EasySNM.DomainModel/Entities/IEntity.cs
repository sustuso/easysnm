﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public interface IEntity
    {
        object ObjectId { get; set; }
    }

    public interface IEntity<TKey> : IEntity
    {
        TKey Id { get; set; }
    }

    public interface IVersionedEntity<TKey> : IEntity<TKey>
    {
        byte[] Version { get; set; }
    }
}
