﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class Role : EntityBase<string>
    {
        [StringLength(10)]
        public override string Id { get; set; }
        [MaxLength(50)]
        public string Description { get; set; }
    }
}
