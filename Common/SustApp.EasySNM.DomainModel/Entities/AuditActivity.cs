﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class AuditActivity : EntityBase<string>
    {
        private static readonly char[] ActivityIdSeparator = new[] { '.' };

        public AuditActivity()
        {
            Id = GetActivityId();
            Name = Activity.Current?.GetBaggageItem(nameof(Activity.OperationName));
            Date = DateTimeOffset.Now;
        }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTimeOffset Date { get; set; }

        public List<Audit> Audits { get; set; } = new List<Audit>();

        private static string GetActivityId()
        {
            Activity a = Activity.Current;
            if (a != null)
            {
                string[] data = a.Id.Split(ActivityIdSeparator);

                return String.Join('.', data.Take(2));
            }

            return null;
        }
    }
}
