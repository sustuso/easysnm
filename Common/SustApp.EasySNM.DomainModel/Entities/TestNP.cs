﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SustApp.EasySNM.DomainModel
{
    public class TestNP : EntityBase<Guid>
    {
        [Required]
        public string Description { get; set; }
        [Required]
        public Guid TestId { get; set; }
        [JsonIgnore]
        public Test? Test { get; set; }
    }
}
