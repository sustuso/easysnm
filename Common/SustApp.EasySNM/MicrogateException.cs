﻿using System;
using System.Runtime.Serialization;

namespace SustApp.EasySNM
{
    public class SustAppException : ApplicationException
    {
        public SustAppException()
        {
        }

        protected SustAppException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public SustAppException(string message) : base(message)
        {
        }

        public SustAppException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
