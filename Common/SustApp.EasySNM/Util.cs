﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace SustApp.EasySNM
{
    public static class Util
    {
        /// <summary>
        /// Create a hash of a string using SHA256
        /// </summary>
        /// <param name="rawData"></param>
        /// <returns></returns>
        public static string ComputeSha256Hash(string rawData)
        {
            // Create a SHA256   
            using (SHA256 sha256Hash = SHA256.Create())
            {
                // ComputeHash - returns byte array  
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(rawData));

                // Convert byte array to a string   
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < bytes.Length; i++)
                {
                    sb.Append(bytes[i].ToString("x2"));
                }
                return sb.ToString();
            }
        }

        public static string MemoryStreamToString(MemoryStream stream)
        {
            return Encoding.UTF8.GetString(stream.ToArray());
        }

        /// <summary>
        /// Loop all items of a specific Enum
        /// Es. var list = Util.GetValues<eCalendarTypeDay>();
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        /// <summary>
        /// Return DateTimeFormatInfo
        /// </summary>
        /// <param name="date"></param>
        /// <returns>DateTimeFormatInfo</returns>
        public static DateTimeFormatInfo GetDateTimeFormat()
        {
            var cultureInfo = new System.Globalization.CultureInfo("it-IT");
            return cultureInfo.DateTimeFormat;
        }

        /// <summary>
        /// Extract seconds from string time without round. Ex: 01:18:38.689
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static decimal SecondsWithoutRound(string time)
        {
            DateTime dateValue;
            if (DateTime.TryParse(time, out dateValue))
            {
                return (dateValue.Hour * 60 * 60) + (dateValue.Minute * 60) + dateValue.Second;
            }

            return 0;
        }

        /// <summary>
        /// Get all date between 2 dates
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> AllDatesBetween(DateTime start, DateTime end)
        {
            for (var day = start.Date; day <= end; day = day.AddDays(1))
                yield return day;
        }

        /// <summary>
        /// Get all date in a specific year/month
        /// </summary>
        /// <param name="year"></param>
        /// <param name="month"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> AllDatesInMonth(int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            for (int day = 1; day <= days; day++)
            {
                yield return new DateTime(year, month, day);
            }
        }

        /// <summary>
        /// Copy stream in a new Stream object
        /// </summary>
        /// <param name="inputStream"></param>
        /// <returns></returns>
        public static Stream CopyStream(Stream inputStream)
        {
            const int readSize = 256;
            byte[] buffer = new byte[readSize];
            MemoryStream ms = new MemoryStream();

            int count = inputStream.Read(buffer, 0, readSize);
            while (count > 0)
            {
                ms.Write(buffer, 0, count);
                count = inputStream.Read(buffer, 0, readSize);
            }
            ms.Seek(0, SeekOrigin.Begin);
            inputStream.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        /// <summary>
        /// http://blogs.msdn.com/b/dbrowne/archive/2010/05/21/using-new-transactionscope-considered-harmful.aspx
        /// </summary>
        /// <returns>transaction scope</returns>
        public static TransactionScope CreateTransactionScope(bool isAsync = false)
        {
            //Alza il timeout che di default è 10min, senza dover modificare il machine.config
            //https://stackoverflow.com/questions/12055511/transaction-scope-timeout-on-10-minutes
            //SetTransactionManagerField("_cachedMaxTimeout", true);
            //SetTransactionManagerField("_maximumTimeout", TransactionManager.MaximumTimeout);

            var transactionOptions = new TransactionOptions();
            transactionOptions.IsolationLevel = IsolationLevel.ReadCommitted;
            //transactionOptions.Timeout = TransactionManager.MaximumTimeout;

            if (isAsync)
                return new TransactionScope(TransactionScopeOption.Required, transactionOptions, TransactionScopeAsyncFlowOption.Enabled);
            else
                return new TransactionScope(TransactionScopeOption.Required, transactionOptions);
        }

        #region Distance
        /// <summary>
        /// Giovanni
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lon1"></param>
        /// <param name="lat2"></param>
        /// <param name="lon2"></param>
        /// <param name="unit"></param>
        /// <returns></returns>
        public static decimal Distance(double lat1, double lon1, double lat2, double lon2, char unit)
        {
            if ((lat1 == lat2) && (lon1 == lon2))
            {
                return 0;
            }
            else
            {
                double theta = lon1 - lon2;
                double dist = Math.Sin(deg2rad(lat1)) * Math.Sin(deg2rad(lat2)) + Math.Cos(deg2rad(lat1)) * Math.Cos(deg2rad(lat2)) * Math.Cos(deg2rad(theta));
                dist = Math.Acos(dist);
                dist = rad2deg(dist);
                dist = dist * 60 * 1.1515;
                if (unit == 'K')
                {
                    dist = dist * 1.609344;
                }
                else if (unit == 'N')
                {
                    dist = dist * 0.8684;
                }

                return (Convert.ToDecimal(dist)); //CONTROLLARE !!! Gianni 23/3/21
            }
        }

        /// <summary>
        /// Giovanni
        /// </summary>
        /// <param name="deg"></param>
        /// <returns></returns>
        private static double deg2rad(double deg)
        {
            return (deg * Math.PI / 180.0);
        }

        /// <summary>
        /// Giovanni
        /// </summary>
        /// <param name="rad"></param>
        /// <returns></returns>
        private static double rad2deg(double rad)
        {
            return (rad / Math.PI * 180.0);
        }
        #endregion

        /// <summary>
        /// Get all date between From, Thru
        /// </summary>
        /// <param name="from"></param>
        /// <param name="thru"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> EachDay(DateTime from, DateTime thru)
        {
            for (var day = from.Date; day.Date <= thru.Date; day = day.AddDays(1))
                yield return day;
        }

        /// <summary>
        /// Giovanni
        /// </summary>
        /// <param name="fieldName"></param>
        /// <param name="value"></param>
        private static void SetTransactionManagerField(string fieldName, object value)
        {
            typeof(TransactionManager).GetField(fieldName, System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static).SetValue(null, value);
        }

        /// <summary>
        /// Compone an encoded parameter url
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string ParameterUrl(string param)
        {
            return "?param=" + WebUtility.UrlEncode(param);
        }

        /// <summary>
        /// Copy object property from source to destination
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TU"></typeparam>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        public static void CopyPropertiesTo<T, TU>(this T source, TU dest)
        {
            var sourceProps = typeof(T).GetProperties().Where(x => x.CanRead).ToList();
            var destProps = typeof(TU).GetProperties()
                    .Where(x => x.CanWrite)
                    .ToList();

            foreach (var sourceProp in sourceProps)
            {
                if (destProps.Any(x => x.Name == sourceProp.Name))
                {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    if (p.CanWrite)
                    { // check if the property can be set or no.
                        p.SetValue(dest, sourceProp.GetValue(source, null), null);
                    }
                }

            }

        }

        /// <summary>
        /// Copy object property from source to destination exlcuded 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="TU"></typeparam>
        /// <param name="source"></param>
        /// <param name="dest"></param>
        public static void CopyPropertiesToExcluded<T, TU>(this T source, TU dest, List<string> fieldToExcluded)
        {
            var sourceProps = typeof(T).GetProperties().Where(x => x.CanRead).ToList();
            var destProps = typeof(TU).GetProperties()
                    .Where(x => x.CanWrite)
                    .ToList();

            foreach (var sourceProp in sourceProps)
            {
                if (destProps.Any(x => x.Name == sourceProp.Name) && !fieldToExcluded.Any(x => x == sourceProp.Name))
                {
                    var p = destProps.First(x => x.Name == sourceProp.Name);
                    if (p.CanWrite)
                    { // check if the property can be set or no.
                        p.SetValue(dest, sourceProp.GetValue(source, null), null);
                    }
                }
                else
                {
                    var a = 1;
                }

            }

        }


        public static async Task<Stream> UrlToStream(Uri fileToDownload)
        {
            MemoryStream targetStreamSecondary = new MemoryStream();
            using (var client = new HttpClient())
            {
                //var fileToDownload = await _files.GetReadUriAsync(pdfUri, TimeSpan.FromMinutes(12 * 60));
                using (var sourceStream = await client.GetStreamAsync(fileToDownload))
                {
                    await sourceStream.CopyToAsync(targetStreamSecondary);
                }
                targetStreamSecondary.Position = 0;
                return targetStreamSecondary;
            }
        }

        /// <summary>
        /// Return true if date is between From and To
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public static bool DateIsInRange(DateTime toCheck, DateTime From, DateTime To)
        {
            if (toCheck > From && toCheck < To)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Change PML for candidate medical postponement
        /// </summary>
        /// <param name="pml"></param>
        /// <param name="NotConsider"></param>
        /// <returns></returns>
        public static string ChangePml(string pml, List<string> NotConsider)
        {
            StringBuilder res = new StringBuilder(pml);
            foreach (var s in NotConsider)
            {
                var i = pml.IndexOf(s + "=");
                if (i != -1)
                    res[i + s.Length + 1] = '-';
            }
            return res.ToString();
        }


    }
}
