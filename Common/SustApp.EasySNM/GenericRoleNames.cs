﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SustApp.EasySNM
{
    public static class GenericRoleNames
    {
        public static IReadOnlyList<string> Roles => new[] {Admin, BackOffice};

        public const string Admin = "Admin";

        public const string BackOffice = "Admin";

    }
}
