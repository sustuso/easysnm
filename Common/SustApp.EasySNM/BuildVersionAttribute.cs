﻿using System;
using System.Linq;
using System.Reflection;

namespace SustApp.EasySNM
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public class BuildVersionAttribute : Attribute
    {
        public BuildVersionAttribute(string version)
        {
            Version = version;
        }

        public string Version { get; }

        public static string Get(Assembly assembly = null)
        {
            return (assembly ?? typeof(BuildVersionAttribute).Assembly)?.GetCustomAttributes(typeof(BuildVersionAttribute))
                       .OfType<BuildVersionAttribute>()
                       .FirstOrDefault()?.Version ?? "";
        }
    }
}
