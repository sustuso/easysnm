﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class GetPdfTempRequest
    {
        public Uri UriFile { get; set; }
    }
}
