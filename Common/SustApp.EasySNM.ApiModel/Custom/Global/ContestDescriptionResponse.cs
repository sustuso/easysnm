﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class ContestDescriptionResponse
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
    }
}
