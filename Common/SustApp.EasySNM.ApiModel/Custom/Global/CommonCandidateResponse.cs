﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class CommonCandidateResponse
    {
        public Guid CandidateId { get; set; }
        public string Alias { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime BirthDate { get; set; }
        public string CF { get; set; }
        public string SiteId { get; set; }
        //public string SiteName { get; set; }
        public string Gender { get; set; }
        //public string BadgeId { get; set; }
    }
}
