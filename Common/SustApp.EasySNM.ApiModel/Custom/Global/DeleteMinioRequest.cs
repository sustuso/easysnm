﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class DeleteMinioRequest
    {
        public Guid CandidateId { get; set; }
        public Uri UriFile { get; set; }
        public string FileName { get; set; }
    }

    public class DeleteMinioTempRequest
    {
        public string BucketName { get; set; }
    }
}
