﻿namespace SustApp.EasySNM.ApiModel
{
    public class SitesComboResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
