﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class UriDTO
    {
        public Uri UriField { get; set; }
    }
}
