﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class GlobalCandidateOverview
    {
        public Guid ContestId { get; set; }
        public Guid CandidateId { get; set; }
        public string Alias { get; set; }
        public decimal? Bib { get; set; }
        public string CandidateInfo { get; set; }

        //PhysicalTest
        public int PhysicalTestToDo { get; set; }
        public int PhysicalTestDone { get; set; }
        public string PhysicalTestStatus { get; set; }
        //PsychometricTest
        public string PsychometricTestDone { get; set; } 
        //PsycologicalTest
        public int PsychologicalTestToDo { get; set; }
        public int PsychologicalTestDone { get; set; }
        public string PsychologicalInteview { get; set; }
        public string PsychologicalStatus { get; set; }
        public string PsychologicalEvaluationDone { get; set; }
        //MedicalTest
        public string GeneralMedicine { get; set; }
        public string Cardiology { get; set; }
        public string Ophthalmology { get; set; }
        public string Otolaryngology { get; set; }
        public string MuscleStrength { get; set; }
        public string Psychiatric { get; set; }

        //Other info
        public string PhotoUrl { get; set; }
        public string FakePhoto { get; set; }

        public bool MedicalTestDone { get; set; }
        public bool AllCommissionMemberSigned { get; set; }

    }



}
