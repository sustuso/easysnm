﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SustApp.EasySNM.ApiModel
{
    public  class TestDTO
    {
        public Guid Id { get; set; }
        public string? String10 { get; set; }
        public int? Number { get; set; }
        public List<string> Descriptions { get; set;} = new List<string>();
        public int? Count { get; set; }

    }
    
}
