﻿using System.Collections.Generic;

namespace SustApp.EasySNM.ApiModel.AdHoc
{
    /// <summary>
    /// Class to valorize for create a json request for Adhoc WS
    /// </summary>
    public class AdHocRequest
    {
        public string oggetto { get; set; }
        public bool hasDatiSensibili { get; set; }
        public bool isPec { get; set; }
        public string casellaPostale { get; set; }
        public string tipoDocumento { get; set; }
        public Mittente mittente { get; set; } = new Mittente();
        public string segnaturaMittente { get; set; }
        public System.DateTime dataProtocolloMittente { get; set; } //DateTime ????????
        public Classificazione classificazione { get; set; } = new Classificazione();
        public List<Allegati> allegati { get; set; } = new List<Allegati>();
        public List<DestinatariInterni> destinatariInterni { get; set; } = new List<DestinatariInterni>();
        public string note { get; set; }
    }

    public class Mittente
    {
        public string denominazione { get; set; }
        public string indirizzo { get; set; }
        public string emailCertificata { get; set; }
        public string email { get; set; }
        public string citta { get; set; }
    }

    public class Classificazione
    {
        public FascicoloV5 fascicoloV5 { get; set; }
    }

    public class FascicoloV5
    {
        public string identificativo { get; set; }
    }

    public class Allegati
    {
        public string fileName { get; set; }
        public bool primario { get; set; }
        public string content { get; set; }
    }

    public class DestinatariInterni
    {
        public string denominazione { get; set; }
        public int idMittDest { get; set; }
        public bool invioPerConoscenza { get; set; }
    }
}
