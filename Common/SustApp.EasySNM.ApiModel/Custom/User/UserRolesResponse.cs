﻿namespace SustApp.EasySNM.ApiModel
{
    public class UserRolesResponse
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }
}
