﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SustApp.EasySNM.ApiModel
{
    public class UserBaseModel
    {
        [Required]
        [StringLength(100)]
        public string Username { get; set; }

        [Required]
        [StringLength(255)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(255)]
        public string LastName { get; set; }
        
        [StringLength(50)]
        public string MilitaryRank { get; set; }

        public DateTime BirthDate { get; set; }

        [StringLength(255)]
        public string? BirthMunicipality { get; set; }

        [StringLength(2)]
        public string? Province { get; set; }

        public string PhotoUrl { get; set; }

        public bool Active { get; set; }

        [StringLength(100)]
        public string SmartCardPin { get; set; } //Only for SmartCard mode

        [StringLength(5000)]
        public string UriValue { get; set; }

        //public Int16 CardType { get; set; } //0 = CMD, 1 = SmartCard

        [StringLength(20)]
        public string SiteWork { get; set; }

        public bool InternalUser { get; set; }
    }
    
    public class UserModel : UserBaseModel
    {
        public string Id { get; set; }

        //[Required] //Come si valida una [] ??????
        public string[] Roles { get; set; }

        ////[Required(ErrorMessage = "Cmd Thumbprint obbligatorio")]
        //[StringLength(50)]
        //public string Thumbprint { get; set; } //Only for Cmd mode

        public bool ForceProviderKey { get; set; }

        public List<UserLogins> UserLoginsList { get; set; } = new List<UserLogins>();
    }

    public class UserLogins
    {
        public string Provider { get; set; }
        public string ProviderKey { get; set; }
    }
}
