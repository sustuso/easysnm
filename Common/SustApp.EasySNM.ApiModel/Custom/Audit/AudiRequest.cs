﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class AuditRequest
    {
        public Guid? CandidateId { get; set; }
        public DateTime? DateFrom { get; set; } 
        public DateTime? DateTo { get; set; }
        public string User { get; set; }
        public string Message { get; set; }
        public string AuditActivityName { get; set; }

        public int Take { get; set; }
        public int Skip { get; set; }
    }
}
