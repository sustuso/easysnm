﻿using System;
using System.Collections.Generic;

namespace SustApp.EasySNM.ApiModel
{
    public class AuditActivityResponse
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public DateTimeOffset Date { get; set; }
        public List<AuditResponse> Audits = new List<AuditResponse>();
    }

    public class AuditResponse
    {
        public string Id { get; set; }
        public DateTimeOffset AuditDate { get; set; }
        public string User { get; set; }
        public string Message { get; set; }
        public string EntityName { get; set; }
        public string AuditActivityId { get; set; }
        public string EntityId { get; set; }
        public string AdditionalData { get; set; }
        public string FirstName { get; set; }
        public string Surname { get; set; }
    }

    public class AuditInfo
    {
        public List<AuditActivityResponse> AuditActivityResponse { get; set; } = new List<AuditActivityResponse>();
        public int Count { get; set; } = 0;
    }
}
