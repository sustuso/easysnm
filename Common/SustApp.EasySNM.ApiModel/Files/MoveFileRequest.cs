﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class MoveFileRequest
    {
        public Uri SourceUri { get; set; }
        public string BucketName { get; set; }
        public string FileName { get; set; }

        public Guid CandidateId { get; set; }
    }
}
