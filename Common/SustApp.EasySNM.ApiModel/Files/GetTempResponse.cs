﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class GetTempResponse
    {
        public Uri WriteUri { get; set; }

        public Uri ReadUri { get; set; }
    }
}
