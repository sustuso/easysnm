﻿namespace SustApp.EasySNM.ApiModel
{
    public class ClientResponseMessage
    {
        /// <summary>
        /// Message to show in Blazor client 
        /// </summary>
        public string Message { get; set; }
    }
}
