﻿using System;
using System.Collections.Generic;

namespace SustApp.EasySNM.ApiModel
{
    public class SignatureRequest
    {
        public string PinCard { get; set; }
        public string Location { get; set; }
        public string Surname { get; set; }
        public string Reason { get; set; }
        public bool InvisibleStamp { get; set; }
        public List<SigninFiles> SigninFilesList { get; set; } = new List<SigninFiles>();
    }

    public class SigninFiles
    {
        public Uri ReadUri { get; set; }
        public Uri WriteUri { get; set; }
        //public string SignedDocName { get; set; }
        public Guid CandidateId { get; set; }
    }
}