﻿using System;

namespace SustApp.EasySNM.ApiModel
{
    public class BiometricSignatureRequest
    {
        public Uri ReadUri { get; set; }
        public Uri WriteUri { get; set; }
    }
}
