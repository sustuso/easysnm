﻿namespace SustApp.EasySNM.ApiModel
{
    public class MinIoBuckets
    {
        public string ContestName { get; set; }
        public string SiteName { get; set; }
        public string CandidateCF { get; set; }
    }
}
