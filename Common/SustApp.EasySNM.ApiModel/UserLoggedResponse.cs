﻿namespace SustApp.EasySNM.ApiModel
{
    public class UserLoggedResponse
    {
        public string UserName { get; set; }
    }
}
