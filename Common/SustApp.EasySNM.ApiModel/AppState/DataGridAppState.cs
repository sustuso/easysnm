﻿using System.Collections.Generic;

namespace SustApp.EasySNM.ApiModel
{
    public class DataGridAppState
    {
        public DictionaryOverride StateJsonDG { get; set; } = new DictionaryOverride();
    }

    public class DictionaryOverride : Dictionary<string, string>
    {
        public new void Add(string key, string value)
        {
            if (this.ContainsKey(key))
            {
                base.Remove(key);
            }

            base.Add(key, value);
        }
    }
}
