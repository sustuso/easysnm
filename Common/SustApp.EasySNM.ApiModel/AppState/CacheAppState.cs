﻿using System;
using System.Collections.Generic;

namespace SustApp.EasySNM.ApiModel
{
    public class CacheAppState
    {
        /// <summary>
        /// Serve x valorizzare "SelProPageContext.PageName"
        /// </summary>
        public Dictionary<Guid, string> Contests { get; set; } = new Dictionary<Guid, string>();

        /// <summary>
        /// Ruoli dell'utente loggato
        /// </summary>
        public List<string> Roles { get; set; } = new List<string>();
    }
}
