﻿namespace SustApp.EasySNM.ApiModel
{
    public class CmdBackOfficeData : Data
    {
        public string Thumbprint { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string TaxCode { get; set; }
        public string ProviderType { get; set; }
    }
}
