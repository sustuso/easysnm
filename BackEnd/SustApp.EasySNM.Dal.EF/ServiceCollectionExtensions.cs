﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.EasySNM.Dal;
using SustApp.EasySNM.Dal.EF;
using SustApp.EasySNM.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace Microsoft.Extensions.DependencyInjection
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddEFDataLayer(this IServiceCollection services, Action<DbContextOptionsBuilder> optionsBuilder)
        {
            services.AddHostedService<MigrationsService>();
            services.AddDbContext<EasySNMDbContext>(optionsBuilder);

            services.AddDataLayer<Test, Guid>();
            services.AddTransient<ITestDal, TestDataLayer>();

            //services.AddDataLayer<Site, string>();
            //services.AddDataLayer<Audit, Guid>();
            //services.AddDataLayer<AuditActivity, string>();
            //services.AddDataLayer<FormField, Guid>();
            //services.AddDataLayer<Measurement, string>();
            //services.AddDataLayer<MeasureUnit, string>();
            //services.AddDataLayer<MedicalSpecialization, string>();
            //services.AddDataLayer<Exam, Guid>();
            //services.AddDataLayer<FormFieldResult, Guid>();
            //services.AddDataLayer<ExamResult, Guid>();
            ////services.AddDataLayer<MilitaryRank, string>();
            //services.AddDataLayer<ContestStatus, string>();
            //services.AddDataLayer<DocumentType, Guid>();
            //services.AddDataLayer<Document, Guid>();
            //services.AddDataLayer<Candidate, Guid>();
            //services.AddDataLayer<Contest, Guid>();
            //services.AddDataLayer<ContestTemplate, Guid>();
            //services.AddDataLayer<Calendar, Guid>();
            //services.AddDataLayer<CandidateStatus, string>();
            //services.AddDataLayer<PostponementMedicalTest, Guid>();
            //services.AddDataLayer<GeneralMedicine, Guid>();
            //services.AddDataLayer<Cardiology, Guid>();
            //services.AddDataLayer<Otolaryngology, Guid>();
            //services.AddDataLayer<Ophthalmology, Guid>();
            //services.AddDataLayer<MuscleStrength, Guid>();
            //services.AddDataLayer<Psychiatry, Guid>();
            //
            //services.AddDataLayer<Region, string>();
            //services.AddDataLayer<Province, string>();
            //services.AddDataLayer<Municipality, int>();
            //services.AddDataLayer<MultiPostalCode, int>();
            //services.AddDataLayer<MetropolitanCity, int>();
            //services.AddDataLayer<ContestProvinceSite, Guid>();
            //services.AddDataLayer<PhysicalTestRule, Guid>();
            //services.AddDataLayer<PhysicalTestType, Guid>();
            //
            //services.AddDataLayer<PsychologicalEvaluationRoom, Guid>();
            //
            //services.AddDataLayer<Role, string>();


            #region Custom
            //services.AddTransient<IExamResult, ExamResultDataLayer>();
            //services.AddTransient<ICalendarDal, CalendarDataLayer>();
            //services.AddTransient<IContestDal, ContestDataLayer>();
            //services.AddTransient<ICandidateDal, CandidateDataLayer>();
            //services.AddTransient<IMunicipalityDal, MunicipalityDataLayer>();
            //services.AddTransient<ISiteDal, SiteDataLayer>();
            //services.AddTransient<ITestClassroomDal, TestClassroomDataLayer>();
            //services.AddTransient<IProvinceDal, ProvinceDataLayer>();
            //services.AddTransient<IRegionDal, RegionDataLayer>();
            //services.AddTransient<ICandidateStatusDal, CandidateStatusDataLayer>();
            //services.AddTransient<IContestTemplateDal, ContestTemplateDataLayer>();
            //services.AddTransient<IContestStatusDal, ContestStatusDataLayer>();
            //services.AddTransient<IExamDal, ExamDataLayer>();
            //services.AddTransient<IFormFieldResultDal, FormFieldResultDataLayer>();
            //services.AddTransient<IMeasureUnitDal, MeasureUnitDataLayer>();
            //services.AddTransient<IMeasurementDal, MeasurementDataLayer>();
            //services.AddTransient<IMedicalSpecializationDal, MedicalSpecializationDataLayer>();
            //services.AddTransient<IContestProvinceSiteDal, ContestProvinceSiteDataLayer>();
            //services.AddTransient<IPersomilProvinceDal, PersomilProvinceDataLayer>();
            //services.AddTransient<IMultiPostalCodeDal, MultiPostalCodeDataLayer>();
            //services.AddTransient<IPhysicalTestRuleDal, PhysicalTestRuleDataLayer>();
            //services.AddTransient<IPsychometricsTestClassroomInfoDal, PsychometricsTestClassroomInfoDataLayer>();
            //services.AddTransient<IPcStationDal, PcStationDataLayer>();
            //services.AddTransient<IPsychometricsTestResultDal, PsychometricsTestResultDataLayer>();
            //services.AddTransient<IPhysicalTestDal, PhysicalTestDataLayer>();
            //services.AddTransient<IPsychometricsTestClassroomNoteDal, PsychometricsTestClassroomNoteDataLayer>();
            //services.AddTransient<ICommissionDal, CommissionDataLayer>();
            //services.AddTransient<ICommissionMemberRoleDal, CommissionMemberRoleDataLayer>();
            //services.AddTransient<ICommissionMemberDal, CommissionMemberDataLayer>();
            //services.AddTransient<ICommissionTypeDal, CommissionTypeDataLayer>();
            //services.AddTransient<IUserRoleDal, UserRoleDataLayer>();
            //services.AddTransient<IDataLayer<UserRole, Guid>, UserRoleDataLayer>();
            //services.AddTransient<IPostponementMedicalTestDal, PostponementMedicalTestDataLayer>();
            //services.AddTransient<ICandidatePostponementMedicalTestDal, CandidatePostponementMedicalTestDataLayer>();
            //services.AddTransient<IMedicalConditionDal, MedicalConditionDataLayer>();
            //services.AddTransient<ICandidateAssessmentsDal, CandidateAssessmentsDataLayer>();
            //services.AddTransient<IGeneralMedicineDal, GeneralMedicineDataLayer>();
            //services.AddTransient<ICardiologyDal, CardiologyDataLayer>();
            //services.AddTransient<IOphthalmologyDal, OphthalmologyDataLayer>();
            //services.AddTransient<IOtolaryngologyDal, OtolaryngologyDataLayer>();
            //services.AddTransient<IPsychiatryDal, PsychiatryDataLayer>();
            //services.AddTransient<IPsychiatryTemplateDal, PsychiatryTemplateDataLayer>();
            //services.AddTransient<IMuscleStrengthDal, MuscleStrengthDataLayer>();
            //services.AddTransient<IPPTDal, PPTDataLayer>();
            //services.AddTransient<IPsychologicalEvaluationDal, PsychologicalEvaluationDataLayer>();
            //services.AddTransient<ICandidateAttachmentsDal, CandidateAttachmentsDataLayer>();
            //services.AddTransient<IMedicalPostponementDal, MedicalPostponementDataLayer>();
            //services.AddTransient<IMedicalPostponementMedicalTestDal, MedicalPostponementMedicalTestDataLayer>();
            //services.AddTransient<ICandidateEligibilityDal, CandidateEligibilityDataLayer>();
            //services.AddTransient<IMilitaryRankDal, MilitaryRankDataLayer>();
            //services.AddTransient<IPhysicalTestTypeDal, PhysicalTestTypeDataLayer>();
            //services.AddTransient<IContestCategoryDal, ContestCategoryDataLayer>();
            //services.AddTransient<IAptitudeTestDal, AptitudeTestDataLayer>();
            //services.AddTransient<IAptitudeFieldDal, AptitudeFieldDataLayer>();
            //services.AddTransient<IDisableDeskDal, DisableDeskDataLayer>();
            //services.AddTransient<IFrozenAptitudeSessionDal, FrozenAptitudeSessionDataLayer>();
            //services.AddTransient<IFrozenPcStationDal, FrozenPcStationDataLayer>();
            //services.AddTransient<IFrozenPsychometricsTestClassroomInfoDal, FrozenPsychometricsTestClassroomInfoDataLayer>();
            //services.AddTransient<IDeferredContestDateDal, DeferredContestDateDataLayer>();
            //services.AddTransient<IKioskQueueDal, KioskQueueDataLayer>();
            //services.AddTransient<IMedicalCharacteristicDal, MedicalCharacteristicDataLayer>();
            //services.AddTransient<IMedicalPostponementCharacteristicDal, MedicalPostponementCharacteristicDataLayer>();


            #endregion

            return services;
        }

        private static IServiceCollection AddDataLayer<T, TKey>(this IServiceCollection services)
            where T : class, IEntity<TKey>
        {
            return services.AddTransient<IDataLayer<T, TKey>, DataLayer<T, TKey>>();
        }
    }
}
