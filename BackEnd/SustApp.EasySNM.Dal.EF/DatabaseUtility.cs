﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace SustApp.EasySNM.Dal.EF
{
    public static class DatabaseUtility
    {
        public static void EnsureCreated(string connectionString)
        {
            int retries = 0;
            while (true)
            {
                try
                {
                    var dbContextOptionsBuilder = new DbContextOptionsBuilder<DbContext>()
                        .UseSqlServer(connectionString);
                    using (var context = new DbContext(dbContextOptionsBuilder.Options))
                    {
                        context.Database.EnsureCreated();
                    }

                    return;
                }
                catch (SqlException) when (retries < 3)
                {
                    retries++;
                    Thread.Sleep(500 * retries);
                }
            }
        }
    }
}
