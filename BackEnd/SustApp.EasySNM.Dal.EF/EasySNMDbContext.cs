﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using SustApp.EasySNM.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;


namespace SustApp.EasySNM.Dal.EF
{
    public class EasySNMDbContext : DbContext
    {

        public EasySNMDbContext()
        {

        }
        public EasySNMDbContext(DbContextOptions<EasySNMDbContext> options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Test>(e =>
            {
                e.ToTable("Tests");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.Property(p => p.Version).IsRowVersion();
                e.HasMany(p => p.TestNPs).WithOne(p => p.Test).HasForeignKey(p => p.TestId).OnDelete(DeleteBehavior.Cascade); //Per eliminare a cascata
            });
            modelBuilder.Entity<TestNP>(e =>
            {
                e.ToTable("TestsNP");
                e.Property(p => p.Id).HasDefaultValueSql("newsequentialid()");
                e.HasOne(p => p.Test).WithMany(p => p.TestNPs).OnDelete(DeleteBehavior.Cascade); //Per eliminare a cascata
            });
            //
        }
    }
}
