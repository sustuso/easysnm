﻿using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Dal.EF
{
    public class TestDataLayer : DataLayer<Test, Guid>, ITestDal
    {
        public TestDataLayer(EasySNMDbContext context) : base(context)
        {
        }
    }
}
