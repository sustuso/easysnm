﻿using SustApp.EasySNM.DomainModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace SustApp.EasySNM.Dal.EF
{
    public class DataLayer<T, TKey> : IDataLayer<T, TKey>
        where T : class, IEntity<TKey>
    {
        protected EasySNMDbContext Context { get; }

        public DataLayer(EasySNMDbContext context)
        {
            Context = context;
        }

        public DbSet<T> Set => Context.Set<T>();

        #region IQuerable Interface

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => Query.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Query.GetEnumerator();

        Type IQueryable.ElementType => Query.ElementType;

        Expression IQueryable.Expression => Query.Expression;

        IQueryProvider IQueryable.Provider => Query.Provider;

        private IQueryable<T> Query => Set;

        #endregion


        #region CRUD Operations

        /// <summary>
        /// Add multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            try
            {
                Set.AddRange(entities);
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException("An error has occurred while adding entities", e);
            }
        }

        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task AddAsync(T entity)
        {
            try
            {
                Set.Add(entity);
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while adding entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Update a list of T entity in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public async Task UpdateRangeAsync(IEnumerable<T> entities)
        {
            try
            {
                foreach (T entity in entities)
                {
                    Context.Entry(entity).State = EntityState.Modified;
                }
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                //Cristian, come gestaimo questo? Al client dovrebbe arrivare un errore parlante tipo: "Errore di concorrenza.."
                throw new SustAppVersionException(e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException("An error has occurred while update entities", e);
            }
        }

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            try
            {
                Context.Entry(entity).State = EntityState.Modified;

                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                //Cristian, come gestaimo questo? Al client dovrebbe arrivare un errore parlante tipo: "Errore di concorrenza.."
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while updating entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public async Task PatchAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties)
        {
            try
            {
                var entityEntry = Context.Entry(entity);
                SetPropertiesToPatch(entityEntry, properties);

                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                //Cristian, come gestaimo questo? Al client dovrebbe arrivare un errore parlante tipo: "Errore di concorrenza.."
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while updating entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public async Task PatchRangeAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties)
        {
            var enumerable = properties as Expression<Func<T, object>>[] ?? properties.ToArray();
            try
            {
                foreach (T entity in entities)
                {
                    var entityEntry = Context.Entry(entity);
                    SetPropertiesToPatch(entityEntry, enumerable);
                }
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                //Cristian, come gestaimo questo? Al client dovrebbe arrivare un errore parlante tipo: "Errore di concorrenza.."
                throw new SustAppVersionException(e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException("An error has occurred while update entities", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id, byte[] version)
        {
            T entity = await Set.FindAsync(id);
            if (entity is IVersionedEntity<TKey> v)
            {
                v.Version = version;
            }

            try
            {
                Context.Entry(entity).State = EntityState.Deleted;
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while deleting entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Delete an entity with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task DeleteAsync(T entity)
        {
            try
            {
                Context.Entry(entity).State = EntityState.Deleted;
                await Context.SaveChangesAsync();
            }
            catch (DBConcurrencyException e)
            {
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateConcurrencyException e)
            {
                //Cristian, come gestaimo questo? Al client dovrebbe arrivare un errore parlante tipo: "Errore di concorrenza.."
                throw new SustAppVersionException(entity, e);
            }
            catch (DbUpdateException e)
            {
                throw new SustAppEntityException(entity, $"An error has occurred while deleting entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetAsync(TKey id)
        {
            try
            {
                return await Set.FindAsync(id);
            }
            catch(Exception ex)
            {
                throw new SustAppEntityException(typeof(T), $"An error has occurred while reading entity {id}", ex);
            }
        }

        #endregion

        private static void SetPropertiesToPatch(EntityEntry<T> entityEntry, IEnumerable<Expression<Func<T, object>>> properties)
        {
            foreach (var expression in properties)
            {
                var propertyEntry = entityEntry.Property(expression);
                if (!propertyEntry.Metadata.IsPrimaryKey())
                {
                    try //TO Eliminare!!!!
                    {
                        propertyEntry.IsModified = true;
                    }
                    catch(Exception ex)
                    {
                        throw;
                    }
                }
            }
        }
    }
}