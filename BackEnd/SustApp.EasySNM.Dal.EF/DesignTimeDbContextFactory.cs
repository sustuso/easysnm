﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace SustApp.EasySNM.Dal.EF
{
    /// <summary>
    /// Factory class for give the connection string to EF Core, since this layer is not a sturtup project of the solution
    /// </summary>
    internal class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<EasySNMDbContext>
    {
        /// <summary>
        /// Using by EF Core, to retrive the connection string
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public EasySNMDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<EasySNMDbContext>();
            optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=EasySNM;Integrated Security=SSPI");

            return new EasySNMDbContext(optionsBuilder.Options);
        }
    }
}
