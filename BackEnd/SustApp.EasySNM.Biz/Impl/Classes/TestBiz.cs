﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.Dal;
using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Biz
{
    public class TestBiz : BaseBiz<Test, Guid>, ITestBiz
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ITestDal _dataLayer;
        public TestBiz(IServiceProvider serviceProvider, ITestDal dataLayer) : base(dataLayer)
        {
            _serviceProvider = serviceProvider;
            _dataLayer = dataLayer;
        }
        /// <summary>
        /// Test DTO
        /// </summary>
        /// <returns></returns>
        public async Task<List<TestDTO>> GetDTO()
        {
            try
            {
                var descr = Query.SelectMany(s => s.TestNPs).Select(s => s.Description).ToList();
                //var res = _dataLayer.Include(i => i.TestNPs).ToList();
                var res = _dataLayer.Include(i => i.TestNPs).ToList();

                //AutoMapper Example ********************************
                var config = new MapperConfiguration(cfg => cfg.CreateMap<Test, TestDTO>()
                    .ForMember(dest => dest.Descriptions, opts => opts.MapFrom(src => src.TestNPs.Select(ci => ci.Description).ToList()))
                    .ForMember(dest => dest.Count, opts => opts.MapFrom(src => src.TestNPs.Count()))
                    );
                //var config = new MapperConfiguration(cfg => cfg.CreateMap<Test, TestDTO>());

                var mapper = new Mapper(config);

                var testDTO = mapper.Map<List<TestDTO>>(res);
                return testDTO;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
