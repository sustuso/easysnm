﻿using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Biz
{
    public interface ITestBiz : IBiz<Test, Guid>
    {
        /// <summary>
        /// Test DTO
        /// </summary>
        /// <returns></returns>
        Task<List<TestDTO>> GetDTO();
    }
}
