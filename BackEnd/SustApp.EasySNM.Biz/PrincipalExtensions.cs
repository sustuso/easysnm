﻿using System.Security.Claims;

namespace SustApp.EasySNM.Biz
{
    public static class PrincipalExtensions
    {
        public static string GetPcName(this ClaimsPrincipal principal)
        {
            return principal.FindFirst("PcName")?.Value;
        }

        public static string GetRoomNumber(this ClaimsPrincipal principal)
        {
            return principal.FindFirst("RoomNumber")?.Value;
        }

        //public static string GetUserId(this ClaimsPrincipal principal)
        //{
        //    return principal.FindFirst("UserId")?.Value;
        //}

        public static string GetSite(this ClaimsPrincipal principal)
        {
            string id = principal.FindFirst("Site")?.Value;
            //if (id != null && Guid.TryParse(id, out Guid guid))
            if (id != null )
            {
                return id;
            }

            return default;
        }
    }
}
