﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security;
using System.Threading.Tasks;
using SustApp.EasySNM.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;

namespace SustApp.EasySNM.Biz
{
    public abstract class AbstractBiz<T, TKey> : IBiz<T, TKey>
        where T : IEntity<TKey>
    {
        #region IQueryable implementations

        IEnumerator<T> IEnumerable<T>.GetEnumerator() => Query.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => Query.GetEnumerator();

        Type IQueryable.ElementType => Query.ElementType;

        Expression IQueryable.Expression => Query.Expression;

        IQueryProvider IQueryable.Provider => Query.Provider;

        protected abstract IQueryable<T> Query { get; }

        #endregion

        #region Abstarct Methods for BaseBiz override

        protected abstract Task AddInternalAsync(T entity);

        protected abstract Task AddRangeInternalAsync(IEnumerable<T> entities);

        protected abstract Task UpdateInternalAsync(T entity);

        protected abstract Task UpdateRangeInternalAsync(IEnumerable<T> entities);

        protected abstract Task PatchInternalAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties);

        protected abstract Task PatchRangeInternalAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties);

        protected abstract Task DeleteInternalAsync(T entity);

        protected abstract Task DeleteInternalAsync(TKey id, byte[] version);

        #endregion

        #region CRUD Operation used from controllers
        
        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task AddAsync(T entity)
        {
            try
            {
                await AddInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while adding entity {entity.Id}", e);
            }
        }

        /// <summary>
        /// Add multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            try
            {
                await AddRangeInternalAsync(entities);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException("Error while updating entities", e);
            }
        }

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity)
        {
            try
            {
                await UpdateInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while updating entity with id {entity.Id}", e);
            }
        }

        /// <summary>
        /// Update multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public async Task UpdateRangeAsync(IEnumerable<T> entities)
        {
            try
            {
                await UpdateRangeInternalAsync(entities);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException("Error while updating entities", e);
            }
        }

        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public async Task PatchRangeAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties)
        {
            try
            {
                await PatchRangeInternalAsync(entities, properties);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException("Error while patching entities", e);
            }
        }

        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public async Task PatchAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties)
        {
            try
            {
                await PatchInternalAsync(entity, properties);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while patching entity with id {entity.Id}", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK without RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id)
        {
            try
            {
                await DeleteInternalAsync(id);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while deleting entity with id {id}", e);
            }
        }

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public async Task DeleteAsync(TKey id, byte[] version)
        {
            try
            {
                await DeleteInternalAsync(id, version);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while deleting entity with id {id}", e);
            }
        }

        /// <summary>
        /// Delete an entity without RowVersion
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public async Task DeleteAsync(T entity)
        {
            try
            {
                await DeleteInternalAsync(entity);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(entity, $"Error while deleting entity with id {entity.Id}", e);
            }
        }

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<T> GetAsync(TKey id)
        {
            try
            {
                return await GetInternalAsync(id);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        #endregion

        #region Virtual Methods        

        protected virtual Task DeleteInternalAsync(TKey id) => DeleteInternalAsync(id, null);

        protected virtual Task<T> GetInternalAsync(TKey id)
        {
            return Query.FirstOrDefaultAsync(i => i.Id.Equals(id));
        }

        /// <summary>
        /// Try to validate the param object
        /// </summary>
        /// <param name="entity">T</param>
        /// <returns></returns>
        protected virtual Task ValidateEntityAsync(T entity)
        {
            return ValidateEntityAsync(entity, null);
        }

        /// <summary>
        /// Try to validate the param object
        /// </summary>
        /// <param name="entity">T</param>
        /// <param name="properties"></param>
        /// <returns></returns>
        protected virtual Task ValidateEntityAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            var result = new List<ValidationResult>();

            // Validate entire object
            if (properties == null || !properties.Any())
            {
                if (!Validator.TryValidateObject(entity, new ValidationContext(entity), result, true))
                {
                    throw new SustAppValidationException(entity, result);
                }
            }
            else
            {
                foreach (var property in properties)
                {
                    PropertyInfo propertyInfo = property.GetPropertyAccess();
                    object propertyValue = propertyInfo.GetValue(entity);
                    var validationContext = new ValidationContext(entity)
                    {
                        MemberName = propertyInfo.Name
                    };
                    Validator.TryValidateProperty(propertyValue, validationContext, result);
                }

                if (result.Count > 0)
                {
                    throw new SustAppValidationException(entity, result);
                }
            }

            return Task.CompletedTask;
        }

        protected virtual Task CheckAccessAsync(TKey id)
        {
            return CheckAccessAsync(Enumerable.Repeat(id, 1));
        }

        protected virtual Task CheckAccessAsync(params TKey[] ids)
        {
            return CheckAccessAsync((IEnumerable<TKey>) ids);
        }

        protected virtual async Task CheckAccessAsync(IEnumerable<TKey> ids)
        {
            var enumerable = ids as TKey[] ?? ids.ToArray();
            if (!await Query.AnyAsync(i => enumerable.Contains(i.Id)))
            {
                if (enumerable.Skip(1).Any())
                {
                    throw new SecurityException($"Cannot access to entities of type {typeof(T).Name}");
                }

                throw new SecurityException($"Cannot access to entity with id {enumerable.FirstOrDefault()} of type {typeof(T).Name}");
            }
        }

        #endregion
    }
}