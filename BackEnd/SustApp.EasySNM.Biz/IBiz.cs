﻿using System;
using System.Collections.Generic;
using SustApp.EasySNM.DomainModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Biz
{
    public interface IBiz<T, in TKey> : IQueryable<T>
        where T : IEntity<TKey>
    {
        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task AddAsync(T entity);

        /// <summary>
        /// Add multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task AddRangeAsync(IEnumerable<T> entities);

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync(T entity);

        /// <summary>
        /// Update multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task UpdateRangeAsync(IEnumerable<T> entities);


        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties);

        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchAsync(T entity, params Expression<Func<T, object>>[] properties) => PatchAsync(entity, properties.AsEnumerable());

        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchRangeAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties);

        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task PatchAsync<TSub>(TSub entity) => PatchRangeAsync(Enumerable.Repeat(entity, 1));

        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task PatchRangeAsync<TSub>(IEnumerable<TSub> entities)
        {
            var entityProperties = typeof(T).GetRuntimeProperties().ToDictionary(r => r.Name, StringComparer.OrdinalIgnoreCase);
            var subProperties = typeof(TSub).GetRuntimeProperties().ToDictionary(r => r.Name, StringComparer.OrdinalIgnoreCase);

            // Match common properties between projection and entity
            var matchedProperties = subProperties.Select(s => new
                {
                    Sub = s.Value,
                    Entity = entityProperties.ContainsKey(s.Key) ? entityProperties[s.Key] : null
                })
                .ToArray();

            // Look for any non matched properties
            var notFound = matchedProperties
                .Where(m => m.Entity == null)
                .ToArray();
            // Raise an error
            if (notFound.Length > 0)
            {
                string names = string.Join(", ", notFound.Select(n => n.Sub.Name));
                throw new ArgumentException($"Cannot find properties {names} on type {typeof(T)}");
            }

            // Create expressions for each property
            // p => p.Property
            var expressions = matchedProperties.Select(m =>
            {
                var parameter = Expression.Parameter(typeof(T));
                Expression body = Expression.MakeMemberAccess(parameter, m.Entity);
                if (body.Type.IsValueType)
                {
                    body = Expression.Convert(body, typeof(object));
                }

                return Expression.Lambda<Func<T, object>>(body, parameter);
            }).ToArray();

            return PatchRangeAsync(GetEntities(), expressions);

            IEnumerable<T> GetEntities()
            {
                foreach (TSub sub in entities)
                {
                    // Create an empty entity object
                    var entity = Activator.CreateInstance<T>();
                    // Copy from projection to the entity
                    foreach (var pair in matchedProperties)
                    {
                        object subValue = pair.Sub.GetValue(sub);
                        pair.Entity.SetValue(entity, subValue);
                    }

                    yield return entity;
                }
            }
        }

        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchRangeAsync(IEnumerable<T> entities, params Expression<Func<T, object>>[] properties) => PatchRangeAsync(entities, properties.AsEnumerable());

        /// <summary>
        /// Delete an entity by PK without RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(TKey id) => DeleteAsync(id, null);

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(TKey id, byte[] version);

        /// <summary>
        /// Delete an entity without RowVersion
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task DeleteAsync(T entity);

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetAsync(TKey id);
    }
}
