﻿using SustApp.EasySNM.Dal;
using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.EasySNM.Biz
{
    public class AuditableBiz<T, TKey> : BaseBiz<T, TKey>
        where T : IEntity<TKey>
    {
        public IServiceProvider ServiceProvider { get; }
        private readonly IDataLayer<Audit, Guid> _auditDataLayer;

        private static readonly TimeSpan AuditExpiration = TimeSpan.FromSeconds(5);

        protected ClaimsPrincipal Principal { get; }

        protected string IdentityName => String.IsNullOrWhiteSpace(Principal.Identity?.Name) ? "Anonymous" : Principal.Identity.Name;

        public AuditableBiz(IServiceProvider serviceProvider) : base(serviceProvider.GetRequiredService<IDataLayer<T, TKey>>())
        {
            ServiceProvider = serviceProvider;
            _auditDataLayer = serviceProvider.GetRequiredService<IDataLayer<Audit, Guid>>();
            Principal = serviceProvider.GetRequiredService<ClaimsPrincipal>();
        }

        /// <summary>
        /// Add internal T entity in database
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override async Task AddInternalAsync(T entity)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.AddInternalAsync(entity);

            string auditActivityId = await AddAuditActivityAsync();
            await _auditDataLayer.AddAsync(new Audit(IdentityName, "Add", entity, auditActivityId));

            scope.Complete();
        }

        protected override async Task AddRangeInternalAsync(IEnumerable<T> entities)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var enumerable = entities as T[] ?? entities.ToArray();
            await base.AddRangeInternalAsync(enumerable);

            string auditActivityId = await AddAuditActivityAsync();
            await _auditDataLayer.AddRangeAsync(enumerable.Select(e => new Audit(IdentityName, "Add", e, auditActivityId)));

            scope.Complete();
        }

        /// <summary>
        /// Update internal T entity in database
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override async Task UpdateInternalAsync(T entity)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.UpdateInternalAsync(entity);

            string auditActivityId = await AddAuditActivityAsync();
            await _auditDataLayer.AddAsync(new Audit(IdentityName, "Update", entity, auditActivityId));

            scope.Complete();
        }

        protected override async Task UpdateRangeInternalAsync(IEnumerable<T> entities)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var enumerable = entities as T[] ?? entities.ToArray();
            await base.UpdateRangeInternalAsync(enumerable);

            string auditActivityId = await AddAuditActivityAsync();

            await _auditDataLayer.AddRangeAsync(enumerable.Select(e => new Audit(IdentityName, "Update", e, auditActivityId)));

            scope.Complete();
        }

        protected override async Task PatchInternalAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.PatchInternalAsync(entity, properties);

            string auditActivityId = await AddAuditActivityAsync();

            string names = String.Join(", ", properties.Select(p => p.GetPropertyAccess().Name));
            await _auditDataLayer.AddAsync(new Audit(IdentityName, $"Patch {names}", entity, auditActivityId));

            scope.Complete();
        }

        protected override async Task PatchRangeInternalAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            var enumerable = entities as T[] ?? entities.ToArray();
            await base.PatchRangeInternalAsync(enumerable, properties);

            string auditActivityId = await AddAuditActivityAsync();
            string names = String.Join(", ", properties.Select(p => p.GetPropertyAccess().Name));
            await _auditDataLayer.AddRangeAsync(enumerable.Select(e => new Audit(IdentityName, $"Patch {names}", e, auditActivityId)));

            scope.Complete();
        }

        /// <summary>
        /// Delete internal an entity by PK with RowVersion for optimistic concurrency
        /// No Try/Catch because present in AbstractBiz
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        protected override async Task DeleteInternalAsync(TKey id, byte[] version)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.DeleteInternalAsync(id, version);

            string auditActivityId = await AddAuditActivityAsync();
            await _auditDataLayer.AddAsync(new Audit(IdentityName, "Delete", typeof(T).Name, id.ToString(), auditActivityId));

            scope.Complete();
        }

        /// <summary>
        /// Delete internal an entity by entity parameter
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected override async Task DeleteInternalAsync(T entity)
        {
            using var scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled);

            await base.DeleteInternalAsync(entity);

            string auditActivityId = await AddAuditActivityAsync();
            await _auditDataLayer.AddAsync(new Audit(IdentityName, "Delete", entity, auditActivityId));

            scope.Complete();
        }

        /// <summary>
        /// Get internal a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        protected override Task<T> GetInternalAsync(TKey id)
        {
            return GetAsync(id, true);
        }

        /// <summary>
        /// Get Async by ID and audit
        /// </summary>
        /// <param name="id"></param>
        /// <param name="audit"></param>
        /// <returns></returns>
        public virtual async Task<T> GetAsync(TKey id, bool audit)
        {
            try
            {
                T entity = await base.GetInternalAsync(id);

                if (audit)
                {
                    string auditActivityId = await AddAuditActivityAsync();
                    await _auditDataLayer.AddAsync(new Audit(IdentityName, "Read", typeof(T).Name, id.ToString(), auditActivityId));
                }

                return entity;
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        private async Task<string> AddAuditActivityAsync()
        {
            var auditActivity = new AuditActivity();
            if (auditActivity.Name == null) return null;

            IMemoryCache memoryCache = ServiceProvider.GetRequiredService<IMemoryCache>();
            await memoryCache.GetOrCreateAsync(auditActivity.Id, async e =>
            {
                // Add an entry if missing
                var auditActivityDataLayer = ServiceProvider.GetRequiredService<IDataLayer<AuditActivity, string>>();
                if (!await auditActivityDataLayer.AnyAsync(a => a.Id == auditActivity.Id))
                {
                    await auditActivityDataLayer.AddAsync(auditActivity);
                }

                e.SetSlidingExpiration(AuditExpiration);

                return auditActivity.Id;
            });

            return auditActivity.Id;
        }
    }
}
