﻿using SustApp.EasySNM.Dal;
using SustApp.EasySNM.DomainModel;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using IServiceProvider = System.IServiceProvider;

namespace SustApp.EasySNM.Biz
{
    /// <summary>
    /// Used only for derived class like Evaluetion where the methdos know the derived class and execute
    /// operations on that, but return the base class (e.g. FlagEvaluation and base Evaluation class)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MultiBiz<T, TKey> : AbstractBiz<T, TKey>
        where T : IEntity<TKey>
    {
        private readonly IServiceProvider _serviceProvider;
        private static readonly MethodInfo AddMethod;
        private static readonly MethodInfo UpdateMethod;
        private static readonly MethodInfo DeleteMethod;

        static MultiBiz()
        {
            AddMethod = new Func<IServiceProvider, T, Task>(AddTypedAsync).GetMethodInfo().GetGenericMethodDefinition();
            UpdateMethod = new Func<IServiceProvider, T, Task>(UpdateTypedAsync).GetMethodInfo().GetGenericMethodDefinition();
            DeleteMethod = new Func<IServiceProvider, TKey, byte[], Task>(DeleteTypedAsync<T>).GetMethodInfo().GetGenericMethodDefinition();
        }

        public MultiBiz(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }


        protected override IQueryable<T> Query => throw new NotSupportedException();

        public virtual IQueryable<TEntity> GetQuery<TEntity>()
            where TEntity : T
        {
            return _serviceProvider.GetRequiredService<IDataLayer<TEntity, TKey>>();
        }

        public virtual IQueryable<T> GetQuery(Type type)
        {
            return (IQueryable<T>)_serviceProvider.GetRequiredService(typeof(IDataLayer<,>).MakeGenericType(type, typeof(TKey)));
        }

        #region Override Methods
        
        protected override Task<T> GetInternalAsync(TKey id)
        {
            throw new NotSupportedException();
        }

        protected override Task AddRangeInternalAsync(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        protected override Task AddInternalAsync(T entity)
        {
            return (Task)AddMethod.MakeGenericMethod(entity.GetType()).Invoke(null, new object[] { _serviceProvider, entity });
        }

        protected override Task UpdateInternalAsync(T entity)
        {
            return (Task)UpdateMethod.MakeGenericMethod(entity.GetType()).Invoke(null, new object[] { _serviceProvider, entity });
        }

        protected override Task UpdateRangeInternalAsync(IEnumerable<T> entities)
        {
            throw new NotImplementedException();
        }

        protected override Task PatchInternalAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties)
        {
            throw new NotImplementedException();
        }

        protected override Task PatchRangeInternalAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties)
        {
            throw new NotImplementedException();
        }
        
        protected override Task DeleteInternalAsync(TKey id, byte[] version)
        {
            throw new NotImplementedException();
        }

        protected override Task DeleteInternalAsync(T entity)
        {
            byte[] version = null;
            if (entity is IVersionedEntity<TKey> ve)
            {
                version = ve.Version;
            }

            return DeleteAsync(entity.GetType(), entity.Id, version);
        }

        #endregion

        #region CRUD Operations        

        public Task<T> GetAsync(Type type, TKey id)
        {
            try
            {
                return GetQuery(type).FirstOrDefaultAsync(e => e.Id.Equals(id));
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        public async Task<TEntity> GetAsync<TEntity>(TKey id)
            where TEntity : T
        {
            try
            {
                return await GetQuery<TEntity>().FirstOrDefaultAsync(e => e.Id.Equals(id));
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        public async Task DeleteAsync(Type type, TKey id, byte[] version)
        {
            try
            {
                await (Task)DeleteMethod.MakeGenericMethod(type).Invoke(null, new object[] { _serviceProvider, id, version });
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        public async Task DeleteAsync<TEntity>(TKey id, byte[] version)
            where TEntity : T
        {
            try
            {
                IDataLayer<TEntity, TKey> dataLayer = _serviceProvider.GetRequiredService<IDataLayer<TEntity, TKey>>();
                await dataLayer.DeleteAsync(id, version);
            }
            catch (SustAppEntityException)
            {
                throw;
            }
            catch (Exception e)
            {
                throw new SustAppEntityException(id, $"Error while reading entity with id {id}", e);
            }
        }

        #endregion

        #region Private static Methods
        
        private static Task AddTypedAsync<TEntity>(IServiceProvider serviceProvider, TEntity entity)
            where TEntity : T
        {
            IDataLayer<TEntity, TKey> dataLayer = serviceProvider.GetRequiredService<IDataLayer<TEntity, TKey>>();
            return dataLayer.AddAsync(entity);
        }

        private static Task UpdateTypedAsync<TEntity>(IServiceProvider serviceProvider, TEntity entity)
            where TEntity : T
        {
            IDataLayer<TEntity, TKey> dataLayer = serviceProvider.GetRequiredService<IDataLayer<TEntity, TKey>>();
            return dataLayer.UpdateAsync(entity);
        }

        private static Task DeleteTypedAsync<TEntity>(IServiceProvider serviceProvider, TKey id, byte[] version)
            where TEntity : T
        {
            IDataLayer<TEntity, TKey> dataLayer = serviceProvider.GetRequiredService<IDataLayer<TEntity, TKey>>();
            return dataLayer.DeleteAsync(id, version);
        }

        #endregion
    }
}