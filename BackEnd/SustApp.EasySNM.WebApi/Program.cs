//var builder = WebApplication.CreateBuilder(args);
//
//// Add services to the container.
//
//builder.Services.AddControllers();
//// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
//
//var app = builder.Build();
//
//// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
//    app.UseSwagger();
//    app.UseSwaggerUI();
//}
//
//app.UseHttpsRedirection();
//
//app.UseAuthorization();
//
//app.MapControllers();
//
//app.Run();


using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hangfire;
using Hangfire.Common;
using Hangfire.States;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using SustApp.EasySNM.Biz;
using SustApp.EasySNM.Web.Dependencies;

namespace SustApp.EasySNM.WebApi
{
    public class Program
    {
        //public static Task Main(string[] args)
        //{
        //    return Test();
        //}

        public static void Main(string[] args)
        {
            CreateHostBuilder(args)
                .Build()
                .CheckDependencies(c => c.AddSqlConnectionsCheck())
                .Run();
        }

        private static async Task Test()
        {
            var host = Host.CreateDefaultBuilder()
                .ConfigureServices(services =>
                {
                    services.AddEFDataLayer(b => b.UseSqlServer(Environment.GetEnvironmentVariable("db")));
                    services.AddBiz();
                    services.AddSingleton(p => new ClaimsPrincipal());

                    services.AddTransient<IBackgroundJobClient, TestClient>();
                })
                .Build();
            host.Start();

            //using (var serviceScope = host.Services.CreateScope())
            //{
            //    TestBiz contestBiz = serviceScope.ServiceProvider.GetRequiredService<TestBiz>();
            //    Test contest = contestBiz.AsNoTracking().FirstOrDefault();
            //    await contestBiz.ContestGenerator(contest.Id);
            //}

            host.Dispose();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSelProSerilog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        private class TestClient : IBackgroundJobClient
        {
            public string Create(Job job, IState state)
            {
                return "";
            }

            public bool ChangeState(string jobId, IState state, string expectedState)
            {
                return true;
            }
        }
    }
}
