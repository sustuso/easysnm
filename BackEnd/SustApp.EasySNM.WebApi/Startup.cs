using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OData.Edm;
using Microsoft.OpenApi.Models;
using System.Collections;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Hangfire;
using IdentityModel;
using IdentityModel.AspNetCore.OAuth2Introspection;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.OData.Extensions;
using Serilog;
using SustApp.EasySNM.WebApi.Components;
using Microsoft.AspNetCore.OData.Formatter;
using SustApp.EasySNM.Web.Authentication;
using SustApp.EasySNM.DomainModel.Json;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.Dal.EF;
using SustApp.EasySNM.ApiModel;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.OData.Extensions;
using Microsoft.AspNetCore.OData;
using Microsoft.OData.ModelBuilder;

namespace SustApp.EasySNM.WebApi
{
    public class Startup
    {
        private readonly IEdmModel _edmModel;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

            _edmModel = GetEdmModel();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddEFDataLayer(b => { b.UseSqlServer(Configuration.GetConnectionString("db")); b.EnableSensitiveDataLogging(); }); //For Debug
            services.AddEFDataLayer(b => b.UseSqlServer(Configuration.GetConnectionString("db")));
            services.AddBiz();
            //services.AddOData();
            services.AddControllers().AddOData(options => options.Select().Filter().OrderBy());
            services.AddHttpContextAccessor();
            services.AddTransient(p => p.GetRequiredService<IHttpContextAccessor>().HttpContext?.User ?? new ClaimsPrincipal());

            //services.RemoveAll<IPushChannel>();
            //services.AddScoped<IPushChannel, SignalRPushChannel>();
            //
            //services.Configure<MinioOptions>(Configuration.GetSection("Minio"));
            //services.Configure<PdfOptions>(Configuration.GetSection("Pdf"));
            //services.Configure<CorsOptions>(Configuration.GetSection("Cors"));
            //services.Configure<BrainHqOptions>(Configuration.GetSection("BrainHq"));

#if FAKE_AUTH
            // Autenticazione fittizia per avere l'utente impersonificato
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddScheme<AuthenticationSchemeOptions, FakeAuthenticationHandler>(IdentityServerAuthenticationDefaults.AuthenticationScheme, null);
#else
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    //Url IdentityServer (AuthorizationServer)
                    options.Authority = Configuration.GetValue<string>("IdentityProvider");
                    options.ApiName = "EasySNM.Api"; //ApiResourceName in IdentityServer Config file
                    options.NameClaimType = JwtClaimTypes.Subject;
                    options.SupportedTokens = SupportedTokens.Jwt;
                    options.SaveToken = false;
                    options.JwtValidationClockSkew = TimeSpan.FromSeconds(5);
                    options.RequireHttpsMetadata = false;

                    // Non ci serve
                    options.EnableCaching = false;

                    // Personalize where token can be retrieve
                    options.TokenRetriever = req =>
                    {
                        var fromHeader = TokenRetrieval.FromAuthorizationHeader();
                        // Query string is need in case of signalr negotation
                        var fromQuery = TokenRetrieval.FromQueryString();
                        return fromHeader(req) ?? fromQuery(req);
                    };
                }
            );
#endif

            services.AddTransient<IClaimsTransformation, RolesEnricher>();

            //add to use Swagger
            services.AddMvc(options =>
            {
                options.Filters.Add(new SustAppExceptionFilter());

                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            });

            // Definizione centralizzata delle policy
            services.AddAuthorization(options =>
                {
                    options.AddPolicy(PolicyNames.Base,
                        policy => policy.RequireAuthenticatedUser().RequireClaim("scope", "EasySNMApi"));

                    options.AddPolicy(PolicyNames.Admin,
                        policy => policy.RequireRole(GenericRoleNames.Admin));
                });

            services.AddControllers(o =>
            {
                o.EnableEndpointRouting = false;
            }).AddNewtonsoftJson(o => SetJsonSerializer(o.SerializerSettings));

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "API",
                    Description = "Test API with ASP.NET Core 3.0",
                    Contact = new OpenApiContact()
                    {
                        Name = "EasySNM Detail",
                        Email = "gianni.morbiani@SustApp.it",
                        Url = new Uri("http://www.SustApp.it/")
                    },
                    License = new OpenApiLicense()
                    {
                        Name = "API SustApp",
                        Url = new Uri("http://www.SustApp.it")
                    },
                });

                //Puntamento al file "SSOUsers.xml" per la documentazione dei metodi:
                var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile);

                //Aggiunta a Swagger di tali documentazioni:
                //c.IncludeXmlComments(xmlPath);
            });

            //services.AddSelProHangfire(Configuration.GetConnectionString("Hangfire"), "push");
            //services.AddHangfireServer(o => o.Queues = new[] { "push" });

            // Custom IUserIdProvider which resolves userid using identity.name
            //services.AddSingleton<IUserIdProvider, IdentityUserIdProvider>();
            //services.AddSignalR()
            //    .AddNewtonsoftJsonProtocol(o => SetJsonSerializer(o.PayloadSerializerSettings));

            services.AddHealthChecks();

            // Support injection from NGINX of ip and scheme
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
                options.KnownProxies.Clear();
                options.KnownNetworks.Clear();
            });
        }

        private static void SetJsonSerializer(JsonSerializerSettings settings)
        {
            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Converters.Add(new PageResultConverter());
            settings.SerializationBinder = new SimpleSerializationBinder(typeof(IEntity), typeof(GetTempResponse));
            settings.ContractResolver = new DeepContractResolver();
            //settings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            //DatabaseUtility.EnsureCreated(Configuration.GetConnectionString("Hangfire"));

            app.UsePathBase();

            // Support for forward headers coming from NGINX
            app.UseForwardedHeaders();

            // Set current service provider for Hangfire pipeline
            //app.Use((context, next) =>
            //{
            //    PrincipalFilterAttribute.SetProvider(context.RequestServices);
            //    return next();
            //});

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //app.UseCors(c => c.WithOrigins(app.ApplicationServices.GetRequiredService<IOptions<CorsOptions>>().Value.ParsedOrigins)
            //    .WithHeaders(HeaderNames.ContentType, HeaderNames.Authorization, "x-requested-with", "request-id")
            //    .AllowAnyMethod()
            //    .AllowCredentials());

            app.UseStaticFiles();

            // Se commentato non invia più le richieste HTTP con Serilog
            //app.UseSerilogRequestLogging();
            app.UseSerilogRequestLogging(options =>
            {
                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    //Add user logged information from ClaimsPrincipal:
                    diagnosticContext.Set("UserLogged", httpContext.User.Identity.Name);
                };
            });

            app.UseRouting();

            var supportedCultures = new[] { "it" };
            var localizationOptions = new RequestLocalizationOptions().SetDefaultCulture(supportedCultures[0])
                .AddSupportedCultures(supportedCultures)
                .AddSupportedUICultures(supportedCultures);

            app.UseRequestLocalization(localizationOptions);

            //Adds the authentication middleware to the pipeline so authentication will be performed 
            //automatically on every call into the host:
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMvc(routeBuilder =>
            {
                //routeBuilder.EnableDependencyInjection();
                //routeBuilder.Select().Filter().OrderBy().Count().Expand().MaxTop(100); //Enable OData operations
                //routeBuilder.MapODataServiceRoute("odata", "odata", _edmModel);
            });

            //https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-3.0&tabs=visual-studio
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //Il 2° parametro, verra mostrato da Swagger in alto a dx:
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");

                //Mostra Swagger nella pagina che apre il WS:
                //Se si commenta la riga sotto -->https://localhost:54846/swagger   json -->  https://localhost:54846/swagger/v1/swagger.json
                //c.RoutePrefix = string.Empty;
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc");

            });
            //app.UseSignalR(o => o.MapHub<PushHub>("/api/push"));
        }

        private IEdmModel GetEdmModel()
        {
            var odataBuilder = new ODataConventionModelBuilder();
            
            return odataBuilder.GetEdmModel();
        }


    }
}
