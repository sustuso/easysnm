using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.AspNetCore.OData;
using Microsoft.AspNetCore.OData.Results;
using Newtonsoft.Json;

namespace SustApp.EasySNM.WebApi.Components
{
    public class PageResultConverter : JsonConverter
    {
        private readonly MethodInfo _getItemsMethod;

        public PageResultConverter()
        {
            _getItemsMethod = new Func<PageResult<int>, IEnumerable<int>>(GetItems).GetMethodInfo().GetGenericMethodDefinition();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var pageResult = value as PageResult;

            writer.WriteStartObject();
            writer.WritePropertyName("count");
            writer.WriteValue(pageResult.Count);

            writer.WritePropertyName("items");

            // Get generic type for PageResult<T>
            Type type = pageResult.GetType().GetGenericArguments()[0];
            // Invoke GetItems method with generic type
            var items = _getItemsMethod.MakeGenericMethod(type).Invoke(null, new[] {pageResult});
            serializer.Serialize(writer, items);

            writer.WriteEndObject();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            throw new NotSupportedException();
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(PageResult).IsAssignableFrom(objectType);
        }

        private static IEnumerable<T> GetItems<T>(PageResult<T> result)
        {
            return result.Items;
        }
    }
}