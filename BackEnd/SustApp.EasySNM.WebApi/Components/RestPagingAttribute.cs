﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.OData.Abstracts;
using Microsoft.AspNetCore.OData.Extensions;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Results;

namespace SustApp.EasySNM.WebApi.Components
{
    /// <summary>
    /// Extensions of EnableQuery which supports pagination result
    /// </summary>
    public class RestPagingAttribute : EnableQueryAttribute
    {
        public RestPagingAttribute()
        {
            MaxNodeCount = 500;
            MaxExpansionDepth = 2; // per aumentare la prof
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);

            if (context.Result is ObjectResult or && IsSuccessStatusCode(or.StatusCode) && or.Value is IQueryable queryable)
            {
                IODataFeature oDataFeature = context.HttpContext.ODataFeature();
                // Check if $count has been specified
                if (oDataFeature.TotalCount.HasValue)
                {
                    Type pageType = typeof(PageResult<>).MakeGenericType(queryable.ElementType);
                    object result = Activator.CreateInstance(pageType, queryable, null, oDataFeature.TotalCount);
                    or.Value = result;
                }
            }
        }

        private bool IsSuccessStatusCode(int? statusCode)
        {
            return statusCode == null || (statusCode >= 200 && statusCode < 300);
        }
    }
}
