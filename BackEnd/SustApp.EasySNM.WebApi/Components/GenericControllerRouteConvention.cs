﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.OData.Edm;

namespace SustApp.EasySNM.WebApi.Components
{
    public class GenericControllerRouteConvention : IControllerModelConvention
    {
        private readonly Type _genericControllerType;
        private readonly Dictionary<string, string> _types;

        public GenericControllerRouteConvention(IEdmModel edmModel, Type genericControllerType)
        {
            _genericControllerType = genericControllerType;
            _types = edmModel.EntityContainer.Elements.OfType<EdmEntitySet>()
                .ToDictionary(e => ((EdmCollectionType)e.Type).ElementType.FullName(), e => e.Name);
        }

        public void Apply(ControllerModel controller)
        {
            if (controller.ControllerType.IsGenericType && controller.ControllerType.GetGenericTypeDefinition() == _genericControllerType
                && _types.TryGetValue(controller.ControllerType.GetGenericArguments()[0].FullName, out string name))
            {
                controller.ControllerName = name;
            }
        }
    }
}
