﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.OData.Edm;

namespace SustApp.EasySNM.WebApi.Components
{
    public class GenericTypeControllerFeatureProvider : IApplicationFeatureProvider<ControllerFeature>
    {
        private readonly Type _genericControllerType;
        private readonly IEnumerable<Type> _types;

        //public GenericTypeControllerFeatureProvider(IEdmModel edmModel, Type genericControllerType)
        //{
        //    _types = edmModel.EntityContainer.Elements
        //        .OfType<EdmEntitySet>()
        //        .Select(e => Type.GetType(((EdmCollectionType)e.Type).ElementType.FullName()))
        //        .ToArray();
        //}

        public GenericTypeControllerFeatureProvider(Assembly assembly, Type genericControllerType) : this(assembly.GetExportedTypes(), genericControllerType)
        {
        }

        public GenericTypeControllerFeatureProvider(IEnumerable<Type> types, Type genericControllerType)
        {
            _types = types;
            _genericControllerType = genericControllerType;
        }

        public void PopulateFeature(IEnumerable<ApplicationPart> parts, ControllerFeature feature)
        {
            foreach (var candidate in _types)
            {
                feature.Controllers.Add(_genericControllerType.MakeGenericType(candidate).GetTypeInfo());
            }
        }
    }
}
