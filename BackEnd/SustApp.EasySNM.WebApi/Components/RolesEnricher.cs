﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using SustApp.EasySNM.Biz;
using SustApp.EasySNM.DomainModel;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace SustApp.EasySNM.WebApi.Components
{
    public class RolesEnricher : IClaimsTransformation
    {
        private readonly IMemoryCache _memoryCache;
        private readonly ILogger<RolesEnricher> _logger;
        private readonly IServiceProvider _serviceProvider;

        public RolesEnricher(IMemoryCache memoryCache, ILogger<RolesEnricher> logger, IServiceProvider serviceProvider)
        {
            _memoryCache = memoryCache;
            _logger = logger;
            _serviceProvider = serviceProvider;
        }

        public async Task<ClaimsPrincipal> TransformAsync(ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated) return principal;

            //var roles = await _memoryCache.GetOrCreateAsync(principal.Identity.Name,
            //    async e =>
            //    {
            //        // Set a 5 minutes duration
            //        e.SetAbsoluteExpiration(TimeSpan.FromMinutes(5));
            //
            //        return await GetRolesAsync(principal.Identity.Name);
            //    });
            string[] roles = new string[] { "admin" }; //TODO Fix

            var identity = principal.Identities.OfType<ClaimsIdentity>().First();
            foreach (string role in roles)
            {
                identity.AddClaim(new Claim(identity.RoleClaimType, role));
            }

            return principal;
        }

        private Task<string[]> GetRolesAsync(string userName)
        {
            var userRoleBiz = _serviceProvider.GetRequiredService<IBiz<UserRole, Guid>>();
            // Load roles from local db
            return userRoleBiz
                .Where(u => u.Username == userName)
                .Select(u => u.RoleId)
                .ToArrayAsync();
        }
    }
}
