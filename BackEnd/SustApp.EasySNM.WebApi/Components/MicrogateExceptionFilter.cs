﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.DomainModel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace SustApp.EasySNM.WebApi.Components
{
    public class SustAppExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            if (context.Exception is SustAppVersionException)
            {
                context.Result = new ConflictResult();
                context.ExceptionHandled = true;
            }
            else if (context.Exception is SustAppValidationException mve)
            {
                // Transform error into a serializable object
                var error = new SerializableError();
                foreach (var validationResult in mve.Results)
                {
                    if (validationResult.MemberNames.Any())
                    {
                        foreach (string memberName in validationResult.MemberNames)
                        {
                            error[memberName] = validationResult.ErrorMessage;
                        }
                    }
                    else
                    {
                        error["Generic"] = validationResult.ErrorMessage;
                    }
                }

                context.Result = new BadRequestObjectResult(error);
                context.ExceptionHandled = true;
            }
        }
    }
}
