﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.Biz;
using SustApp.EasySNM.DomainModel;

namespace SustApp.EasySNM.WebApi.Controllers
{
    public class TestControler : BaseRestController<Test, Guid>
    {
        //private readonly IBackgroundJobClient _backgroundJobClient;
        public TestControler(IBiz<Test, Guid> biz) : base(biz)
        {
            //this._backgroundJobClient = backgroundJobClient;
        }
        [AllowAnonymous]
        [HttpGet("TestDTO")]
        [EnableQuery] // To enable OData feature
        public virtual async Task<List<TestDTO>> TestDTO()
        {
            return await ((ITestBiz)Biz).GetDTO();
        }
    }
}
