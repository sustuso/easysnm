﻿using SustApp.EasySNM.Biz;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.Web.Authentication;
using SustApp.EasySNM.WebApi.Components;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace SustApp.EasySNM.WebApi.Controllers
{
    //https://localhost:44383/.well-known/openid-configuration
    [Authorize(PolicyNames.Base)]
    [Route("/api/[controller]")]
    public class BaseRestController<T, TKey> : ControllerBase
        where T : class, IEntity<TKey>
    {
        protected IBiz<T, TKey> Biz { get; }

        /// <summary>
        /// CTOR with biz DI
        /// </summary>
        /// <param name="biz"></param>
        public BaseRestController(IBiz<T, TKey> biz)
        {
            Biz = biz;
        }


        #region CRUD Operations

        /// <summary>
        /// Get all data from repository
        /// </summary>
        /// <returns></returns>
        [RestPaging]
        [HttpGet]
        public virtual IEnumerable<T> Get()
        {
            return Biz;
        }

        /// <summary>
        /// Get single item from repository by PK
        /// </summary>
        /// <returns></returns>
        [RestPaging]
        [HttpGet("{id}")]
        public virtual async Task<ActionResult<T>> Get(TKey id)
        {
            T entity = await Biz.GetAsync(id);
            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }

        /// <summary>
        /// Adds an entity to the repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public virtual async Task<IActionResult> Post([FromBody] T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.AddAsync(item);

            return CreatedAtAction("Post", item);
        }

        /// <summary>
        /// Add a list of entities in repository
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        [HttpPost("PostRangeAsync")]
        public virtual async Task<IActionResult> PostRangeAsync([FromBody] List<T> items)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.AddRangeAsync(items);

            return CreatedAtAction("PostRangeAsync", items); //E' corretto usare questa e non OK ?????
        }

        /// <summary>
        /// Modify an entity in repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut]
        public virtual async Task<IActionResult> Put([FromBody] T items)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.UpdateAsync(items);

            return Ok();
        }

        /// <summary>
        /// Modify a list of entities in repository
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPut("PutRangeAsync")]
        public virtual async Task<IActionResult> PutRangeAsync([FromBody] List<T> items)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await Biz.UpdateRangeAsync(items);

            return Ok();
        }

        /// <summary>
        /// Delete an entity in repository by PK
        /// </summary>
        /// <param name="id">PK</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public virtual async Task<IActionResult> Delete(TKey id)
        {
            if (id.Equals(default))
            {
                return BadRequest("Id empty");
            }

            await Biz.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Delete an entity in repository by PK
        /// </summary>
        /// <param name="id">PK</param>
        /// <returns></returns>
        [HttpDelete("{id}/{version}")]
        public virtual async Task<IActionResult> Delete(TKey id, byte[] version)
        {
            if (id.Equals(default))
            {
                return BadRequest("Id empty");
            }

            await Biz.DeleteAsync(id, version);

            return Ok();
        }



        /// <summary>
        /// Delete an entity in repository by PK
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public virtual async Task<IActionResult> Delete([FromBody]T entity)
        {
            await Biz.DeleteAsync(entity);

            return Ok();
        }

        #endregion

        /// <summary>
        /// Call WS using OData syntax and return typed data in controler caller
        /// </summary>
        /// <typeparam name="TOData"></typeparam>
        /// <param name="uri">OData syntaxt</param>
        /// <returns></returns>
        protected async Task<TOData> GetODataWS<TOData>(Uri uri)
        {
            using (var httpClient = new HttpClient())
            {                
                httpClient.BaseAddress = CalculateBaseUri();
                var response = await httpClient.GetAsync(uri);
                response.EnsureSuccessStatusCode();

                var bodyResponse = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TOData>(bodyResponse);
            }
        }

        /// <summary>
        /// Return a BaseUri of Controller called
        /// </summary>
        /// <returns></returns>
        private Uri CalculateBaseUri()
        {
            //This is OK:
            //var baseUri = new Uri(string.Format("{0}://{1}{2}", Request.Scheme, Request.Host, Request.PathBase));

            var displayUrl = UriHelper.GetDisplayUrl(Request);
            var urlBuilder = new UriBuilder(displayUrl)
            {
                Path = null,
                Query = null,
                Fragment = null
            };
            
            return new Uri(urlBuilder.ToString());
        }
    }
}
