﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.Biz;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.Web.Authentication;
using SustApp.EasySNM.WebApi.Components;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.OData.Edm;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.AspNetCore.OData.Deltas;
using Microsoft.AspNetCore.OData.Abstracts;
using Microsoft.AspNetCore.OData.Extensions;
using Microsoft.AspNetCore.OData.Routing;

namespace SustApp.EasySNM.WebApi.Controllers
{
    [Authorize(PolicyNames.Base)]
    public class BaseODataController<T, TKey> : ODataController
        where T : class, IEntity<TKey>
    {
        private readonly IBiz<T, TKey> _biz;

        public BaseODataController(IBiz<T, TKey> biz)
        {
            _biz = biz;
        }

        #region OData

        [HttpGet]
        [EnableQuery]
        public virtual IEnumerable<T> Get()
        {
            return _biz;
        }

        [EnableQuery]
        [HttpGet]
        public virtual IEnumerable<T> Get(TKey key)
        {
            return _biz.Where(i => i.Id.Equals(key));
        }

        [HttpPost]
        public virtual async Task<IActionResult> Post([FromBody] T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _biz.AddAsync(item);

            return Created(item);
        }

        [HttpPatch]
        public virtual async Task<IActionResult> Patch(TKey key, [FromBody]Delta<T> item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            T result;
            if (_biz is MultiBiz<T, TKey> multi)
            {
                result = await multi.GetAsync(item.StructuredType, key);
            }
            else
            {
                result = await _biz.GetAsync(key);
            }

            item.Patch(result);

            await _biz.UpdateAsync(result);

            return Updated(result);
        }

        [HttpPut]
        public virtual async Task<IActionResult> Put(TKey key, [FromBody] T item)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            await _biz.UpdateAsync(item);

            return Updated(item);
        }

        [HttpDelete]
        public virtual async Task<IActionResult> Delete(TKey key)
        {
            if (_biz is MultiBiz<T, TKey> multi)
            {
                IODataFeature oDataFeature = Request.ODataFeature();
                //Type type = Type.GetType($"{oDataFeature.Path.EdmType.FullTypeName()}, {typeof(IEntity).Assembly.FullName}", true);
                Type type = Type.GetType($"{oDataFeature.GetType()}, {typeof(IEntity).Assembly.FullName}", true);
                await multi.DeleteAsync(type, key, null);
            }
            else
            {
                await _biz.DeleteAsync(key);
            }

            return Ok();
        }

        #endregion
    }
}