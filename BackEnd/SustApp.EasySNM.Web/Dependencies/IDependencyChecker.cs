﻿using System.Threading.Tasks;

namespace SustApp.EasySNM.Web.Dependencies
{
    public interface IDependencyChecker
    {
        Task WaitForReady();
    }
}
