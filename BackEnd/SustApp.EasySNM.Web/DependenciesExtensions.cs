﻿using System;
using System.Collections.Generic;
using System.Text;
using SustApp.EasySNM.Web.Dependencies;
using Microsoft.Extensions.Hosting;

namespace Microsoft.AspNetCore.Hosting
{
    /// <summary>
    /// Extensions for IWebHost which allow to apply depedency check before running the host
    /// </summary>
    public static class DependenciesExtensions
    {
        public static IHost CheckDependencies(this IHost host, Action<DependenciesConfiguration> configuration)
        {
            var c = new DependenciesConfiguration(host.Services);
            configuration(c);
            return new DependenciesHost(host, c);
        }
    }
}