﻿using System;
using System.Collections.Generic;
using System.Text;
using Array = System.Array;

namespace SustApp.EasySNM.Web
{
    public class CorsOptions
    {
        private string _origins;
        private string[] _parsedOrigins;

        public string Origins
        {
            get => _origins;
            set
            {
                _origins = value;
                _parsedOrigins = null;
            }
        }

        public string[] ParsedOrigins
        {
            get
            {
                if (_parsedOrigins == null)
                {
                    _parsedOrigins = Origins?.Split(new[] {' ', ',', ';'}, StringSplitOptions.RemoveEmptyEntries) ?? Array.Empty<string>();
                }

                return _parsedOrigins;
            }
        }
    }
}
