﻿namespace SustApp.EasySNM.Web.Authentication
{
    public static class PolicyNames
    {
        public const string Base = "base";
        public const string Admin = "admin";
        public const string BackOffice = "backoffice";
    }
}
