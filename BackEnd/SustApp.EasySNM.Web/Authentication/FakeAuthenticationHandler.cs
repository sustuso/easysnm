﻿using IdentityModel;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Web.Authentication
{
    /// <summary>
    /// TO DO: Da sostituire il codice, nel momento in cui su Blazor, implementeranno l'autenticazione con OAUTH.
    /// </summary>
    public class FakeAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        public FakeAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger, UrlEncoder encoder, ISystemClock clock) : base(options, logger, encoder, clock)
        {
        }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (Context.Request.Path.StartsWithSegments("/Pdfs"))
            {
                return AuthenticateResult.Fail("test");
            }

            // Impersonificazione di un finto utente
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(JwtClaimTypes.Subject, "admin"),
                new Claim(JwtClaimTypes.Role, "admin"),
                new Claim(JwtClaimTypes.Scope, "EasySNM.Api"),
                new Claim("Site", "foligno"),
                new Claim("PcName", "AR-001"),
                new Claim("RoomNumber", "1"),
                new Claim("UserId", "MRBGNN72C22F205Y"),
            }, IdentityServerAuthenticationDefaults.AuthenticationScheme, JwtClaimTypes.Subject, JwtClaimTypes.Role);

            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, identity.AuthenticationType);
            return AuthenticateResult.Success(ticket);
        }
    }
}
