﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Core;
using Serilog.Events;

namespace Microsoft.Extensions.Hosting
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder UseSelProSerilog(this IHostBuilder hostBuilder)
        {
            return hostBuilder.UseSerilog((b, s) =>
            {
                var host = b.Configuration["Serilog:Host"];
                var token = b.Configuration["Serilog:Token"];

                var seqHost = b.Configuration["Serilog:SeqHost"];

                s = s
                    .MinimumLevel.Debug()
                    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
                    .MinimumLevel.Override("IdentityServer", LogEventLevel.Error)
                    .MinimumLevel.Override("IdentityServer4", LogEventLevel.Error)
                    .MinimumLevel.Override("Hangfire", LogEventLevel.Error)
                    .MinimumLevel.Override("SustApp.EasySNM.Web.Dependencies.SqlConnectionCheck", LogEventLevel.Error) //Stringa di connessione di HangFire logata da Serilog..via
                    .MinimumLevel.Override("SustApp.EasySNM.WebApi.SignalR", LogEventLevel.Error) // /api/push NON vanno loggate
                    .Enrich.FromLogContext()
                    .WriteTo.Console(LogEventLevel.Debug);
                //if (!String.IsNullOrWhiteSpace(token) && !String.IsNullOrWhiteSpace(host))
                //    s = s.WriteTo.EventCollector(host, token, restrictedToMinimumLevel: LogEventLevel.Information);
                if (!String.IsNullOrWhiteSpace(seqHost))
                    s = s.WriteTo.Seq(seqHost, restrictedToMinimumLevel: LogEventLevel.Information);
            });
        }
    }
}
