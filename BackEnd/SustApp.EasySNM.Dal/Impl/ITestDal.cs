﻿using SustApp.EasySNM.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Dal
{
    public interface ITestDal : IDataLayer<Test, Guid>
    {
    }
}
