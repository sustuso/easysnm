﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SustApp.EasySNM.Dal
{
    public interface IDataLayer<T, in TKey> : IQueryable<T>
    {
        /// <summary>
        /// Add T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task AddAsync(T entity);

        /// <summary>
        /// Add multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task AddRangeAsync(IEnumerable<T> entities);

        /// <summary>
        /// Update multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        Task UpdateRangeAsync(IEnumerable<T> entities);

        /// <summary>
        /// Update T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task UpdateAsync(T entity);

        /// <summary>
        /// Delete an entity by PK without RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task DeleteAsync(TKey id) => DeleteAsync(id, null);

        /// <summary>
        /// Delete an entity by PK with RowVersion for optimistic concurrency
        /// </summary>
        /// <param name="id"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        Task DeleteAsync(TKey id, byte[] version);

        /// <summary>
        /// Delete an entity without RowVersion
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        Task DeleteAsync(T entity);

        /// <summary>
        /// Get a single enity by PK
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<T> GetAsync(TKey id);
        
        /// <summary>
        /// Patch multiple T entities in database
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchRangeAsync(IEnumerable<T> entities, IEnumerable<Expression<Func<T, object>>> properties);

        /// <summary>
        /// Patch T entity in database
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        Task PatchAsync(T entity, IEnumerable<Expression<Func<T, object>>> properties);
    }
}
