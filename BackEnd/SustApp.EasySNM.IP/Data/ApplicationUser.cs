﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace SustApp.EasySNM.IdentityProvider.Data
{
    public class ApplicationUser : IdentityUser
    {
    }
}
