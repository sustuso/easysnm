using System;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using IdentityModel;
using IdentityServer4.Configuration;
using IdentityServer4.Extensions;
using IdentityServer4.Stores;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using SustApp.EasySNM.IdentityProvider.Components;
using SustApp.EasySNM.IdentityProvider.Data;
using IdentityServer4.AccessTokenValidation;
using Microsoft.AspNetCore.OData;
using SustApp.EasySNM.Web.Authentication;
using SustApp.EasySNM.WebApi.Components;
using SustApp.EasySNM.DomainModel.Json;
using SustApp.EasySNM.DomainModel;
using SustApp.EasySNM.ApiModel;
using Serilog;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.OData.Formatter;

namespace SustApp.EasySNM.IdentityProvider
{
    public class Startup
    {
        private IWebHostEnvironment _environment;
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration, IWebHostEnvironment environment)
        {
            Configuration = configuration;
            _environment = environment;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration.GetConnectionString("Db");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            //TODO Da verificare
            //X509Certificate2 cert = GetCertificate();

            var identityClientOptions = new IdentityClientOptions();
            Configuration.GetSection("Identity").Bind(identityClientOptions);
            services.Configure<IdentityClientOptions>(Configuration.GetSection("Identity"));

            // Lettura utenze da creare da appsettings.json, sezione Creation
            services.Configure<UsersCreationOptions>(Configuration.GetSection("Creation"));
            // Task in background che crea automaticamente gli utenti di startup
            services.AddHostedService<UsersCreationHostedService>();

            // Gestisce il problema di persistenza del cookie introdotto da .NET Core 3.1
            services.AddSameSiteCookiePolicy();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(connectionString));

            services.AddIdentityCore<ApplicationUser>()
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.Configure<Web.CorsOptions>(Configuration.GetSection("Cors"));
            //add to use Swagger
            services.AddMvc(options =>
            {
                options.Filters.Add(new SustAppExceptionFilter());

                foreach (var outputFormatter in options.OutputFormatters.OfType<ODataOutputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    outputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
                foreach (var inputFormatter in options.InputFormatters.OfType<ODataInputFormatter>().Where(_ => _.SupportedMediaTypes.Count == 0))
                {
                    inputFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/prs.odatatestxx-odata"));
                }
            });

            //https://identityserver4.readthedocs.io
            services.AddIdentityServer(o =>
                {
                    //o.PublicOrigin = Configuration["Identity:PublicOrigin"];
                    o.UserInteraction.LoginUrl = "/login";
                    o.UserInteraction.LogoutUrl = "/login";
                })
                //.AddSigningCredential(cert) //TODO da verificare
                .AddInMemoryIdentityResources(IdentityConfig.GetIdentityResources(identityClientOptions))
                .AddInMemoryApiResources(IdentityConfig.GetApiResources(identityClientOptions))
                .AddInMemoryClients(IdentityConfig.GetClients(identityClientOptions))

                //http://docs.identityserver.io/en/latest/quickstarts/7_entity_framework.html
                //Sostituisce "AddInMemoryIdentityResources" e salva i token/refreshToken nella 
                //tabella "PersistedGrants" di SqlServer. Questo � fiondamentale in caso di bilanciamento macchine:
                .AddOperationalStore(options =>
                {
                    options.ConfigureDbContext = b =>
                        b.UseSqlServer(connectionString,
                            sql => sql.MigrationsAssembly(migrationsAssembly));

                    // this enables automatic token cleanup. this is optional.
                    options.EnableTokenCleanup = true;
                    //options.TokenCleanupInterval = 30; // interval in seconds
                });

#if FAKE_AUTH
            // Autenticazione fittizia per avere l'utente impersonificato
            services.AddAuthentication()
                .AddScheme<AuthenticationSchemeOptions, FakeAuthenticationHandler>(IdentityServerAuthenticationDefaults.AuthenticationScheme, null);
            
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo()
                {
                    Version = "v1",
                    Title = "API",
                    Description = "Test API with ASP.NET Core 3.0",
                    Contact = new OpenApiContact()
                    {
                        Name = "EasySNM Detail",
                        Email = "gianni.morbiani@SustApp.it",
                        Url = new Uri("http://www.SustApp.it/")
                    },
                    License = new OpenApiLicense()
                    {
                        Name = "API SustApp",
                        Url = new Uri("http://www.SustApp.it")
                    },
                });

                //Puntamento al file "SSOUsers.xml" per la documentazione dei metodi:
                var xmlFile = $"{System.Reflection.Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = System.IO.Path.Combine(AppContext.BaseDirectory, xmlFile);

                //Aggiunta a Swagger di tali documentazioni:
                //c.IncludeXmlComments(xmlPath);

            });
#else


            services.AddAuthentication()
                .AddIdentityServerAuthentication(options =>
                    {
                        //Url IdentityServer (AuthorizationServer)
                        options.Authority = Configuration.GetValue<string>("IdentityProvider:Uri");
                      
                        options.ApiName = "EasySNM.Api";
                        options.NameClaimType = JwtClaimTypes.Subject;
                        options.SupportedTokens = SupportedTokens.Jwt;
                        options.SaveToken = false;
                        options.JwtValidationClockSkew = TimeSpan.FromSeconds(5);
                        options.RequireHttpsMetadata = false;

                        // Non ci serve
                        options.EnableCaching = false;
                    }
                );
#endif

            services.AddControllersWithViews(o => o.EnableEndpointRouting = false)
                .AddNewtonsoftJson(o => SetJsonSerializer(o.SerializerSettings));
            //services.AddOData();
            services.AddControllers().AddOData(options => options.Select().Filter().OrderBy());
            // Definizione centralizzata delle policy
            services.AddAuthorization(options =>
            {
                options.AddPolicy(PolicyNames.Base,
                    policy => policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme).RequireAuthenticatedUser().RequireClaim("scope", "EasySNM.Api"));

                options.AddPolicy(PolicyNames.BackOffice,
                    policy => policy.RequireRole(GenericRoleNames.BackOffice, GenericRoleNames.Admin));
            });

            // Support injection from NGINX of ip and scheme
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.KnownProxies.Clear();
                options.KnownNetworks.Clear();
                options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
            });

            services.AddHealthChecks();

        }

        private static void SetJsonSerializer(JsonSerializerSettings settings)
        {
            settings.TypeNameHandling = TypeNameHandling.Objects;
            settings.Converters.Add(new PageResultConverter());
            settings.SerializationBinder = new SimpleSerializationBinder(typeof(IEntity), typeof(GetTempResponse));
            settings.ContractResolver = new DeepContractResolver();
        }

        private X509Certificate2 GetCertificate()
        {
            string certificatePassword = Configuration["IdentityProvider:CertificatePassword"];
            if (String.IsNullOrWhiteSpace(certificatePassword))
            {
                throw new InvalidOperationException("No IdentityProvider:CertificatePassword setting specified");
            }

            string certificatePath = Configuration["IdentityProvider:CertificatePath"];
            if (String.IsNullOrWhiteSpace(certificatePath))
            {
                certificatePath = Path.Combine(_environment.ContentRootPath, "CloudKey.pfx");
            }

            //sare una variabile d'ambiente??
            var cert = new X509Certificate2(certificatePath, certificatePassword);
            return cert;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UsePathBase();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseForwardedHeaders();
            app.UseCors(c => c.WithOrigins(app.ApplicationServices.GetRequiredService<IOptions<Web.CorsOptions>>().Value.ParsedOrigins)
                .WithHeaders(HeaderNames.ContentType, HeaderNames.Authorization, "x-requested-with", "request-id").AllowAnyMethod().AllowCredentials());

            ApplyMigrations(app.ApplicationServices);

            // Gestisce il problema di persistenza del cookie introdotto da .NET Core 3.1
            app.UseCookiePolicy();

            app.UseStaticFiles();

            //app.UseSerilogRequestLogging();
            app.UseSerilogRequestLogging(options =>
            {
                // Attach additional properties to the request completion event
                options.EnrichDiagnosticContext = (diagnosticContext, httpContext) =>
                {
                    //Add user logged information from ClaimsPrincipal:
                    diagnosticContext.Set("UserLogged", httpContext.User.Identity.Name);
                };
            });

            app.UseRouting();

            // Middleware che forza il redirect verso la pagina di login
            // in caso di /authorize senza prompt
            app.UseMiddleware<AutoLoginMiddleware>();

            app.UseIdentityServer();
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseMvc(routeBuilder =>
            {
                routeBuilder.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
                
                //routeBuilder.EnableDependencyInjection();
                //routeBuilder.Select().Filter().OrderBy().Count().Expand().MaxTop(100); //Enable OData operations
            });
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                //Il 2° parametro, verra mostrato da Swagger in alto a dx:
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API V1");

                //Mostra Swagger nella pagina che apre il WS:
                //Se si commenta la riga sotto -->https://localhost:54846/swagger   json -->  https://localhost:54846/swagger/v1/swagger.json
                //c.RoutePrefix = string.Empty;
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/hc");

            });
        }

        private void ApplyMigrations(IServiceProvider serviceProvider)
        {
            using (var scope = serviceProvider.CreateScope())
            {
                //http://docs.identityserver.io/en/latest/quickstarts/7_entity_framework.html
                //In fase di migrazione, crea la tabella "PersistedGrants" x la gestione dei Token (la prima volta che parte):
                scope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();
            }
        }
    }
}
