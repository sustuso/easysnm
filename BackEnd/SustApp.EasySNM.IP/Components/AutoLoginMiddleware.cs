﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.WebUtilities;

namespace SustApp.EasySNM.IdentityProvider.Components
{
    public class AutoLoginMiddleware
    {
        private readonly RequestDelegate _next;

        public AutoLoginMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public Task Invoke(HttpContext httpContext)
        {
            // Controllo se è in corso la verifica di una sessione
            if (httpContext.Request.Path.StartsWithSegments("/connect/authorize") &&
                httpContext.Request.Query.TryGetValue("id_token_hint", out var prompt) &&
                // Evito un loop
                !httpContext.Request.Query.ContainsKey("skipLogin"))
            {
                string uri = QueryHelpers.AddQueryString(httpContext.Request.GetEncodedUrl(), "skipLogin", "true");

                // Redirigo alla pagina di login
                httpContext.Response.Redirect(httpContext.Request.PathBase.Add("/login").Add(QueryString.Create("returnUrl", uri)));

                return Task.CompletedTask;
            }

            return _next(httpContext);
        }
    }
}
