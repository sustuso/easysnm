﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.EasySNM.IdentityProvider.Components
{
    public class UsersCreationOptions
    {
        public List<SmartCardUser> Users { get; } = new List<SmartCardUser>();
    }

    public class SmartCardUser
    {
        public string Username { get; set; }

        public string Cmd { get; set; }

        public string Role { get; set; }
    }
}
