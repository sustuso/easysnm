﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SustApp.EasySNM.IdentityProvider.Components
{
    public class IdentityClientOptions
    {
        public Uri JobsClientUri { get; set; }

        public Uri UiClientUri { get; set; }

        public string PublicOrigin { get; set; }

        public Dictionary<string, string> AdditionalData { get; set; } = new Dictionary<string, string>();
    }
}
