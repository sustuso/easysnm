﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace SustApp.EasySNM.IP.Controllers
{
    //[Authorize(PolicyNames.Base)]
    [Route("test/[controller]")]
    [ApiController]
    //[AllowAnonymous]
    public class Test : Controller
    {
        [HttpGet("TestController")] // url + /test/Test/TestController
        public async Task<string> TestController()
        {
            return "Ok";
        }
    }
}
