﻿using IdentityModel;
using IdentityServer4.Services;
using SustApp.EasySNM.IdentityProvider.Components;
using SustApp.EasySNM.IdentityProvider.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using IdentityServer4.Extensions;

namespace SustApp.EasySNM.IdentityProvider.Controllers
{
    public class LoginController : Controller
    {
        private readonly ILogger<UserController> _logger;

        public LoginController(ILogger<UserController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Index(string returnUrl,
            bool? autologin,
            [FromServices] UserManager<ApplicationUser> userManager)
        {
            // Force signout in order to avoid any anti forgery conflict
            if (User?.IsAuthenticated() ?? false)
            {
                await HttpContext.SignOutAsync();
                HttpContext.User = new ClaimsPrincipal();
            }

            var viewModel = new LoginViewModel
            {
                ReturnUrl = returnUrl,
            };
            return View(viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> ValidateThumbprint()
        {
            string thumbprint = await new StreamReader(Request.Body).ReadToEndAsync();
            if (CheckThumbprint(thumbprint))
            {
                return Ok();
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult Cancel(LoginViewModel viewModel)
        {
            return RedirectToReturnUrl(viewModel);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> Index(
            LoginViewModel viewModel,
            [FromServices] IOptions<IdentityClientOptions> identityClientOptions,
            [FromServices] UserManager<ApplicationUser> userManager,
            [FromServices] IIdentityServerInteractionService interaction)
        {
            var context = await interaction.GetAuthorizationContextAsync(viewModel.ReturnUrl);

            _logger.LogInformation($"Tentativo di accesso all'applicazione col ProviderKey Thumbprint = {viewModel.Thumbprint} and ProviderType = {viewModel.ProviderType}");

            if (CheckThumbprint(viewModel.Thumbprint))
            {
                // TODO: gestire il certificato (probabilmente base64) e validarlo
                ApplicationUser user =
                    await userManager.FindByLoginAsync(viewModel.ProviderType, viewModel.Thumbprint);

                if (user != null)
                {
                    //TODO da verificare
                    //SmartCard mode:
                    //if (viewModel.ProviderType == ConstantCommon.SmartCardProvider)
                    //{
                    //    //Password check:
                    //    if (viewModel.Pin != user.SmartCardPin)
                    //    {
                    //        viewModel.Error = "Password errata";
                    //        return View(viewModel);
                    //    }
                    //}

                    //TODO Da verificare
                    if (user != null)
                    {
                        var roles = await userManager.GetRolesAsync(user);
                        var claims = roles
                            .Select(r => new Claim(JwtClaimTypes.Role, r))
                            .ToList();

                        // Add additional data
                        if (!String.IsNullOrWhiteSpace(viewModel.AdditionalData))
                        {
                            // Deserialize from json
                            IDictionary<string, string> additionalData = JsonConvert.DeserializeObject<IDictionary<string, string>>(viewModel.AdditionalData);
                            foreach (var (key, value) in additionalData)
                            {
                                claims.Add(new Claim(key, value));
                            }
                        }
                        // Add fixed additional data
                        foreach (var (key, value) in identityClientOptions.Value.AdditionalData)
                        {
                            if (!String.IsNullOrWhiteSpace(value))
                            {
                                claims.Add(new Claim(key, value));
                            }
                        }

                        var props = new AuthenticationProperties
                        {
                            // Evitiamo che il cookie di sessione si mantenga nel tempo
                            ExpiresUtc = DateTimeOffset.Now.AddSeconds(5)
                        };
                        //TODO da verificare
                        //await HttpContext.SignInAsync(user.UserName, props, claims.ToArray());

                        return RedirectToReturnUrl(viewModel);
                    }
                    else
                    {
                        viewModel.Error = "Utente non attivo";
                    }
                }
                else
                {
                    viewModel.Error = "Utente non riconosciuto";
                }

                if (viewModel.SkipLogin)
                {
                    return RedirectToReturnUrl(viewModel);
                }
            }
            else
            {
                // TODO: personalizzare
                viewModel.Error = "Certificato non valido";
            }

            return View(viewModel);
        }

        private bool CheckThumbprint(string thumbprint)
        {
            // TODO: validazione certificato remota
            return true;
        }

        private IActionResult RedirectToReturnUrl(LoginViewModel viewModel)
        {
            if (String.IsNullOrWhiteSpace(viewModel.ReturnUrl))
            {
                return Redirect("/");
            }
            else
            {
                return Redirect(viewModel.ReturnUrl);
            }
        }
    }
}