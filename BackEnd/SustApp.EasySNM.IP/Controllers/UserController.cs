﻿using SustApp.EasySNM.ApiModel;
using SustApp.EasySNM.IdentityProvider.Data;
using SustApp.EasySNM.Web.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SustApp.EasySNM.WebApi.Components;

namespace SustApp.EasySNM.IdentityProvider.Controllers
{
    [Authorize(PolicyNames.Base)]
    [Route("ipapi/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ILogger<UserController> _logger;

        public UserController(UserManager<ApplicationUser> userManager, ILogger<UserController> logger)
        {
            _userManager = userManager;
            _logger = logger;
        }

        [RestPaging]
        [HttpGet]
        public IQueryable<UserBaseModel> Get()
        {
            //var result = _userManager.Users.Select(u => new UserBaseModel
            //{
            //    Username = u.UserName,
            //    FirstName = u.FirstName,
            //    LastName = u.LastName,
            //    MilitaryRank = u.MilitaryRank,
            //    BirthDate = u.BirthDate,
            //    BirthMunicipality = u.BirthMunicipality,
            //    Province = u.Province,
            //    PhotoUrl = u.PhotoUrl,
            //    Active = u.Active,
            //    SmartCardPin = null,
            //    UriValue = null,
            //    SiteWork = u.SiteWork,
            //    InternalUser = u.InternalUser
            //});
            //TODO da verificare
            return null;
        }

        [HttpGet("{username}")]
        public async Task<IActionResult> Get(string username)
        {
            // Cerco l'utente
            var user = await _userManager.FindByNameAsync(username);
            if (user == null)
            {
                return NotFound();
            }
           
            // Recupero i login dell'utente
            var logins = await _userManager.GetLoginsAsync(user);

            //// Cerco il login dedicato alla smartcard
            //UserLoginInfo smartCardLogin = logins.FirstOrDefault(l => l.LoginProvider == loginProvider);

            // Recupero i ruoli
            var roles = await _userManager.GetRolesAsync(user);

            List<UserLogins> UserLoginsList = new List<UserLogins>();
            logins.ToList().ForEach(e => UserLoginsList.Add(new UserLogins() { Provider = e.LoginProvider, ProviderKey = e.ProviderKey }));
            //TODO Da verificare
            //var result = new UserModel
            //{
            //    Id = user.Id,
            //    Username = username,
            //    //Thumbprint = smartCardLogin?.ProviderKey,
            //    FirstName = user.FirstName,
            //    LastName = user.LastName,
            //    Roles = roles.ToArray(),
            //    MilitaryRank = user.MilitaryRank,
            //    BirthDate = user.BirthDate,
            //    BirthMunicipality = user.BirthMunicipality,
            //    Province = user.Province,
            //    PhotoUrl = user.PhotoUrl,
            //    Active = user.Active,
            //    //CardType = cardType,
            //    SmartCardPin = user.SmartCardPin,
            //    UserLoginsList = UserLoginsList,
            //    UriValue = user.UriValue,
            //    SiteWork = user.SiteWork,
            //    InternalUser = user.InternalUser
            //};
            return Ok(null);
            //return Ok(result);
        }

        [Authorize(Policy = PolicyNames.BackOffice)]
        [HttpDelete("{username}")]
        public async Task<IActionResult> Delete(string username)
        {
            ApplicationUser user = await _userManager.FindByNameAsync(username);
            if (user != null)
            {
                var identityResult = await _userManager.DeleteAsync(user);
                // Controllo il risultato
                if (!CheckIdentityResult(identityResult, username, out var error))
                {
                    return error;
                }
                return Ok();
            }

            return NotFound();
        }

        //[Authorize(Policy = PolicyNames.BackOffice)]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserModel model)
        {
            if(model == null)
                return BadRequest();

            IdentityResult identityResult;
            ApplicationUser user = null;
            if(model.Id != null) //Modify
                user = await _userManager.FindByIdAsync(model.Id);
            else
                user = await _userManager.FindByNameAsync(model.Username);

            using (var scope = Util.CreateTransactionScope(true))
            {
                if (user == null)
                {
                    // Creo l'utente perché non esiste
                    user = new ApplicationUser
                    {
                        UserName = model.Username,
                        //FirstName = model.FirstName,
                        //LastName = model.LastName,
                        //MilitaryRank = model.MilitaryRank,
                        //BirthDate = model.BirthDate,
                        //BirthMunicipality = model.BirthMunicipality,
                        //Province = model.Province,
                        //PhotoUrl = model.PhotoUrl,
                        //Active = model.Active,
                        //SmartCardPin = model.SmartCardPin,
                        //UriValue = model.UriValue,
                        //SiteWork = model.SiteWork,
                        //InternalUser = model.InternalUser
                    };

                    //Add user in db:
                    identityResult = await _userManager.CreateAsync(user);

                    // Controllo il risultato
                    if (!CheckIdentityResult(identityResult, model.Username, out var error))
                    {
                        return error;
                    }
                }
                else
                {
                    // Aggiorno l'utente
                    //user.FirstName = model.FirstName;
                    //user.LastName = model.LastName;
                    //user.MilitaryRank = model.MilitaryRank;
                    //user.BirthDate = model.BirthDate;
                    //user.BirthMunicipality = model.BirthMunicipality;
                    //user.Province = model.Province;
                    //user.PhotoUrl = model.PhotoUrl;
                    //user.Active = model.Active;
                    //user.SmartCardPin = model.SmartCardPin;
                    user.UserName = model.Username;
                    //user.UriValue = model.UriValue;
                    //user.SiteWork = model.SiteWork;
                    //user.InternalUser = model.InternalUser;

                    await _userManager.UpdateAsync(user);
                }

                // Recupero i login dell'utente da AspNetUserLogins
                var logins = await _userManager.GetLoginsAsync(user);

                if (logins != null)
                {
                    //Delete all record in AspNetUserLogins:
                    foreach (var item in logins)
                    {
                        await _userManager.RemoveLoginAsync(user, item.LoginProvider, item.ProviderKey);
                    }
                }

                //Add recod in AspNetUserLogins
                if (model.UserLoginsList != null && model.UserLoginsList.Count > 0)
                {
                    foreach (var item in model.UserLoginsList)
                    {
                        //Check if exist another ProviderKey used from a different user:
                        var userCheck = await _userManager.FindByLoginAsync(item.Provider, item.ProviderKey);
                        if (userCheck != null && user.UserName != userCheck.UserName)
                        {
                            if (model.ForceProviderKey)
                            {
                                //Remove record in AspNetUserLogins table and update AspNetUsers table with null SmartCardPin column:
                                await _userManager.RemoveLoginAsync(userCheck, item.Provider, item.ProviderKey);
                                //userCheck.SmartCardPin = null;
                                await _userManager.UpdateAsync(userCheck);
                            }
                            else
                            {
                                return Ok($"Code01Attenzione: {item.Provider} con valore {item.ProviderKey} risulta già asseganto a.");
                            }
                        }

                        // Aggiungo il login per la smartcard
                        identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(item.Provider, item.ProviderKey, item.Provider));

                        // Controllo il risultato
                        if (!CheckIdentityResult(identityResult, model.Username, out var error))
                        {
                            return error;
                        }
                    }
                }

                if (model.Roles != null)
                {
                    foreach (var role in model.Roles)
                    {
                        identityResult = await _userManager.AddToRoleAsync(user, role);
                        // Controllo il risultato
                        if (!CheckIdentityResult(identityResult, model.Username, out var error))
                        {
                            // Ignoro gli errori dell'utente già nel ruolo
                            if (identityResult.Errors.First().Code != "UserAlreadyInRole")
                            {
                                return error;
                            }
                        }
                    }
                }

                scope.Complete();
            }

            return Ok(null);
        }

        /// <summary>
        /// Check if a spcific user exist, by username
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpGet("ExistUser/{userName}")]
        public async Task<IActionResult> ExistUser(string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);

            if (user != null)
                return Ok(true);
            else
                return Ok(false);
        }

        /// <summary>
        /// Check ApplicationUser in AspNetUserLogins with this PK
        /// </summary>
        /// <param name="loginProvider"></param>
        /// <param name="providerKey"></param>
        /// <returns></returns>
        [HttpGet("{id}/{userName}/ExistUserLogin/{loginProvider}/{providerKey}")]
        public async Task<IActionResult> ExistUserLogin(string id, string userName, string loginProvider, string providerKey)
        {
            ApplicationUser user = null;
            if (id != null) //Modify
                user = await _userManager.FindByIdAsync(id);
            else
                user = await _userManager.FindByNameAsync(userName);

            // Recupero i login dell'utente da AspNetUserLogins
            var logins = await _userManager.GetLoginsAsync(user);

            //Recupero i record in AspNetUserLogins dato lo user:
            var userCheck = await _userManager.FindByLoginAsync(loginProvider, providerKey);

            if (user.UserName != userCheck.UserName)
                return Ok(true);
            else
                return Ok(false);
        }

        private bool CheckIdentityResult(IdentityResult identityResult, string username, out IActionResult result)
        {
            if (!identityResult.Succeeded)
            {
                result = new ConflictObjectResult(identityResult.Errors);
                _logger.LogError("Cannot create user {name}: {errors}", username,
                    String.Join(", ", identityResult.Errors.Select(e => e.Description)));
                return false;
            }

            result = null;

            return true;
        }

        #region Comment

        //[HttpPost("AddOrUpdate")]
        //public async Task<IActionResult> AddOrUpdate([FromBody] UserModel model)
        //{
        //    IdentityResult identityResult;
        //    ApplicationUser user = await _userManager.FindByNameAsync(model.Username);
        //    if (user == null)
        //    {
        //        // Creo l'utente perché non esiste
        //        user = new ApplicationUser
        //        {
        //            UserName = model.Username,
        //            FirstName = model.FirstName,
        //            LastName = model.LastName,
        //            MilitaryRank = model.MilitaryRank,
        //            BirthDate = model.BirthDate,
        //            BirthMunicipality = model.BirthMunicipality,
        //            Province = model.Province,
        //            CF = model.CF,
        //            PhotoUrl = model.PhotoUrl,
        //        };
        //        identityResult = await _userManager.CreateAsync(user);
        //        // Controllo il risultato
        //        if (!CheckIdentityResult(identityResult, model.Username, out var error))
        //        {
        //            return error;
        //        }
        //    }
        //    else
        //    {
        //        // Aggiorno l'utente
        //        user.FirstName = model.FirstName;
        //        user.LastName = model.LastName;
        //        user.MilitaryRank = model.MilitaryRank;
        //        user.BirthDate = model.BirthDate;
        //        user.BirthMunicipality = model.BirthMunicipality;
        //        user.Province = model.Province;
        //        user.CF = model.CF;
        //        user.PhotoUrl = model.PhotoUrl;

        //        await _userManager.UpdateAsync(user);
        //    }

        //    // Recupero i login dell'utente
        //    var logins = await _userManager.GetLoginsAsync(user);
        //    // Cerco il login dedicato alla smartcard
        //    UserLoginInfo smartCardLogin = logins.FirstOrDefault(l => l.LoginProvider == Constants.CmdProvider);
        //    // Se non c'è il login oppure la chiave è diversa
        //    if (smartCardLogin == null || smartCardLogin.ProviderKey != model.Thumbprint)
        //    {
        //        // Rimuovo l'eventuale login esistente
        //        if (smartCardLogin != null)
        //        {
        //            await _userManager.RemoveLoginAsync(user, Constants.CmdProvider, smartCardLogin.ProviderKey);
        //        }
        //        // Aggiungo il login per la smartcard
        //        identityResult = await _userManager.AddLoginAsync(user, new UserLoginInfo(Constants.CmdProvider, model.Thumbprint, Constants.CmdProvider));
        //        // Controllo il risultato
        //        if (!CheckIdentityResult(identityResult, model.Username, out var error))
        //        {
        //            return error;
        //        }
        //    }

        //    // Aggiungo l'utente al ruolo admin
        //    foreach (var role in model.Roles)
        //    {
        //        identityResult = await _userManager.AddToRoleAsync(user, role);
        //        // Controllo il risultato
        //        if (!CheckIdentityResult(identityResult, model.Username, out var error))
        //        {
        //            // Ignoro gli errori dell'utente già nel ruolo
        //            if (identityResult.Errors.First().Code != "UserAlreadyInRole")
        //            {
        //                return error;
        //            }
        //        }
        //    }

        //    return Ok();
        //}

        #endregion
    }
}
