﻿using System;

namespace SustApp.EasySNM.IdentityProvider
{
    public class LoginViewModel
    {
        public string ProviderType { get; set; }
        public string Pin { get; set; }
        public string Thumbprint { get; set; }
        //public string BaseUri { get; set; }

        public string Error { get; set; }

        public string AdditionalData { get; set; }

        public bool HasError => !String.IsNullOrWhiteSpace(Error);

        public string ReturnUrl { get; set; }

        public bool SkipLogin => ReturnUrl?.Contains("skipLogin=", StringComparison.OrdinalIgnoreCase) ?? false;

    }
}
