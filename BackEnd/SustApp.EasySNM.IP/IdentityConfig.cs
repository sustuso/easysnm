﻿using System;
using IdentityServer4.Models;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using SustApp.EasySNM.IdentityProvider.Components;
using Microsoft.Extensions.DependencyInjection;

namespace SustApp.EasySNM.IdentityProvider
{
    public class IdentityConfig
    {
        public static IEnumerable<ApiResource> GetApiResources(IdentityClientOptions options)
        {
            return new List<ApiResource>
            {
                new ApiResource("SustApp.EasySNM.Api", "EasySNM APIs")
                {
                    UserClaims = options.AdditionalData.Keys.Concat(new [] { JwtClaimTypes.Role }).ToArray(),
                }
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources(IdentityClientOptions options)
        {
            return new List<IdentityResource>
            {
                //new IdentityResources.OpenId(),
                new IdentityResource(IdentityServerConstants.StandardScopes.OpenId, options.AdditionalData.Keys.Concat(new [] { JwtClaimTypes.Subject, JwtClaimTypes.Role }).ToArray())
                {
                    Required = true,
                },
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<Client> GetClients(IdentityClientOptions options)
        {
            return new List<Client>
            {
                //Flow: ClientCredentials
                new Client
                {
                    ClientId = "NIpJumYfxD",

                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("W$XGbQ6t_QgZB_f&U|7ZCk?_$ijH<{".Sha256())
                    },

                    // scopes that client has access to
                    AllowedScopes = { "EasySNMApi" }
                },
                new Client
                {
                    ClientId = "SustApp.EasySNM.Jobs",
                    ClientSecrets ={ new Secret("aAJ9gXLIniGdQA12oHLzjMtdFNyYQgAttP8PqsPYfX8".Sha256()) },
                    RequireClientSecret = true,

                    AllowedGrantTypes = GrantTypes.Code,

                    RequirePkce = false,
                    AllowPlainTextPkce = false,

                    AlwaysIncludeUserClaimsInIdToken = true,

                    //If Authenticate, redirect to "SustApp.EasySNM.UI.Server" project
                    RedirectUris = { new Uri(options.JobsClientUri,"signin-oidc").ToString() },
                    //Redirect for logout
                    PostLogoutRedirectUris = { new Uri(options.JobsClientUri, "signout-callback-oidc").ToString()},
                    RequireConsent = false,

                    AccessTokenLifetime = 300,
                    AuthorizationCodeLifetime = 60,

                    // scopes that client has access to
                    AllowedScopes ={ IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile }
                },
                new Client
                {
                    ClientId = "SustApp.EasySNM.UI",
                    // TODO: remove because is not used
                    // ClientSecrets ={ new Secret("secret")},
                    RequireClientSecret = false,

                    AllowedGrantTypes = GrantTypes.Code,

                    RequirePkce = true,
                    AllowPlainTextPkce = false,

                    //If Authenticate, redirect to "SustApp.EasySNM.UI.Server" project
                    RedirectUris = { new Uri(options.UiClientUri, "authentication/login-callback").ToString() },
                    //Redirect for logout
                    PostLogoutRedirectUris = { new Uri(options.UiClientUri, "authentication/logout-callback").ToString() },
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,

                    //AccessTokenLifetime = 60 * 5, // seconds
                    //AuthorizationCodeLifetime = 30, // seconds

                    AccessTokenLifetime = 120 * 60, // seconds
                    AuthorizationCodeLifetime = 15 * 60, // seconds

                    AlwaysSendClientClaims = true,

                    // scopes that client has access to
                    AllowedScopes ={ IdentityServerConstants.StandardScopes.OpenId, IdentityServerConstants.StandardScopes.Profile, "EasySNM.Api" }
                }
            };
        }
    }
}
